# M152 - Multimedia-Inhalte in Webauftritt integrieren

[> **Modulidentifikation** ](https://www.modulbaukasten.ch/module/152/3/de-DE?title=Multimedia-Inhalte-in-Webauftritt-integrieren)

[> **Bewertungsraster** ](./M152_Bewertungsraster.xlsx)

- [Ressourcen](./auftraege/x_gitressourcen)
- [Buecher_Skripts_Praesentationen](./auftraege/x_gitressourcen/Buecher_Skripts_Praesentationen)
- [Buecher_Skripts_Praesentationen/Skript](./auftraege/x_gitressourcen/Buecher_Skripts_Praesentationen/Modul_152_Script_3.pdf)

## Aufträge & Übungen


### Donnerstag Klassen: <br><mark>AP20c</mark> (morgens) und <br><mark>AP20d</mark> (nachmittags)


| Tag  | Datum | Kompet. | Nr.    | Auftrag, Übung, Thema |
| ---- | ----- | ----- | ------ | -------------- |
|   1  | 23.02.| K1    | M152-a | [Webspace buchen und erste Website erstellen](https://gitlab.com/harald.mueller/aktuelle.kurse/-/tree/master/m152/auftraege/a) |
|   2  | 02.03.| K1    | M152-b | [Storytelling, Wireframes, Mockups](https://gitlab.com/harald.mueller/aktuelle.kurse/-/tree/master/m152/auftraege/b) |
|   3  | 09.03.| K1    | M152-c | [Eigener Styleguide definieren/bestimmen und dokumentieren](https://gitlab.com/harald.mueller/aktuelle.kurse/-/tree/master/m152/auftraege/c) |
|   4  | 16.03.| K2    | M152-d | [Urheberrecht](https://gitlab.com/harald.mueller/aktuelle.kurse/-/tree/master/m152/auftraege/d) |
|   4  | 23.03.| K2    | M152-e | [Copyrights und Lizenzen im Internet](https://gitlab.com/harald.mueller/aktuelle.kurse/-/tree/master/m152/auftraege/e) |
|   5  | 30.03.| K2    | M152-f | [DSGVO, Cookie-Warnung, Impressum](https://gitlab.com/harald.mueller/aktuelle.kurse/-/tree/master/m152/auftraege/f) |
|   6  | 06.04.| K3    | M152-g | [Bildformate, Videoformate, Soundformate erstellen und vergleichen](https://gitlab.com/harald.mueller/aktuelle.kurse/-/tree/master/m152/auftraege/g) |
|   7  | 13.04.| K4    | M152-h | [Animationen erstellen (anim-CSS, anim-GIF, SVG, Keyframes, Transitions, Canvas)](https://gitlab.com/harald.mueller/aktuelle.kurse/-/tree/master/m152/auftraege/h) |
|   8  | 20.04.| K5<br>K6 | M152-i<br>M152-j  | [Testen / Automatisierung & autom. Formulare ausfüllen](https://gitlab.com/harald.mueller/aktuelle.kurse/-/tree/master/m152/auftraege/i) <br> [Optimierung, Page speed](https://gitlab.com/harald.mueller/aktuelle.kurse/-/tree/master/m152/auftraege/j)|
|   -  | 27.04.|       |        | *fällt aus, Frühjahrsferien* |
|   -  | 04.05.|       |        | *fällt aus, Frühjahrsferien* |
|   9  | 11.05.|       |        |  **--->  Projektabgaben** |


