# Aufgabe M152-j: Optimierung, Page speed

## Geschwindigkeit und Performanz
- Browser-Entwicklertools ('Inspector' in FireFox, Chrome und Edge --> Ctrl Shift I)
- Google Page Speed Insights (https://developers.google.com/speed/pagespeed/insights/?hl=de)
- Apache JMeter (https://jmeter.apache.org)


## Klickverhalten und Benutzerfokus
- https://www.hotjar.com
- https://crazyegg.com

## Publikums-Test
- https://www.intellizoom.com
