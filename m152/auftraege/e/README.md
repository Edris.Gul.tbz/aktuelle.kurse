# Aufgabe M152-e: Copyrights und Lizenzen im Internet
Zeitbedarf: ca. 45-60 min

Ergänzen Sie Ihre Urheberrechts-oder Ihre Impressum-Seite in Ihrem Internetauftritt mit den Erklärungen über: 
 - was sind und bedeuten Copyrights im Internet und wer hat welche Copyrights auf Ihrem Internetauftritt
 - was sind und bedeuten Lizenzen im Internet
 - was ist "public domain" und "fair use"

Alle Informationen, die Sie hier zusammentragen auf einer/der entsprechenden Seite aufführen und allenfalls mit eigenen Kommentaren oder Feststellungen ergänzen.

[Referenzmaterial](../x_gitressourcen/Urheberrecht) (und weiteres aus dem Internet)
