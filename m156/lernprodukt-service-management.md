# M156

## Lernprodukt Service Management

Sie beschäftigen sich hier intensiv mit diesem Thema in unterschiedlicher Weise und Tiefe.

### Auftrag und Ablauf

<br>Schritt 1.) Beantworten Sie zuerst im Selbststudium folgender Fragen (45 min)
<br>Schritt 2.) Im kleinen Team (2-3 Pers) besprechen Sie die Fragen (30 min)
<br>Schritt 3.) Wir besprechen die Punkte in der Klasse (45 min)
<br>Schritt 4.) Im Team wird ein **"Lernprodukt"** erstellt 

Ein Lernprodukt kann sein:
- Dokument plus Vortrag
- Lehr-Film/Erklär-Video
- Video- oder nur Audio-Podcast 
	- z.B. "Streitgespräch unter Experten" (d.h. es gibt Gegner und Befürworter)
	- z.B. "Streitgespräch unter Konsumenten" (d.h. es gibt Gegner und Befürworter)
	- z.B. "IT-Experte erklärt IT-Lehrlingen das System"
	- z.B. "Verkaufsgespräch Kunde und IT-Dienstleister", 

Aufwand: 12-16 Lektionen

Resultat: variiert zwischen 5 min Film, 20 min Vortrag bis 60 min (Laber-)Podcast


<br>Bewertet wird die 
- thematische/inhaltliche Auseinandersetzung des Themas, 
- die Ausarbeitung und die 
- Präsentation und Wirkung.

### Fragen zu ITIL und Service Management

- Was ist Service Managemennt?
- Was heisst eigentlich "Service"?
- Was heisst eigentlich "Management"?
- Was heisst ITIL (die Abkürzung und der zu erwartende Inhalt)?
- Gibt es noch anderes als ITIL für "Service Management", welche?
- Wie lange gibt es ITIL schon? Gibt es Versionen?
- Wozu ist ITIL "erfunden" worden?
- Was für Bereiche gibt es in ITIL?
- Wo kommt ITIL vor?
- Für wen ist ITIL gemacht?
- Wer kann ITIL bauchen (Zielpublikum)?
- Wird/wurde in meinem Betrieb ITIL eingesetzt, habe ich das schon mal angetroffen?
- Könnte ich mit ITIL ein besseren Job bekommen?
- Könnte ich mit guten ITIL-Kenntnissen (mehr) Geld verdienen?
- Wo und wie könnte ich am Besten ITIL lernen?
- Gibt es professionelle Zertifikate und was könnten die einem nützen?
- Kann ich ITIL gebrauchen? Wo?
- Gibt es Stellen in ITIL, die mir helfen können, 
  <br>das Thema dieses Moduls "Neue Services entwickeln und Einführung planen" 
  <br>in Anwenung zu bringen?
  <br>° Wieviel kann ich übernehmen?
  <br>° Was muss ich noch selber machen?
