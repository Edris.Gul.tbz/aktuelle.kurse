	M114 Tag3 Lernstandsanalyse Zahlensysteme
	-----------------------------------------
	was ist X hoch 0 = ?
	
	16 hoch 1 =   16 (entspricht 2 hoch 4)  |
	16 hoch 2 =  256 (entspricht 2 hoch 8)  | --> was fällt hier auf?
	16 hoch 3 = 4096 (entspricht 2 hoch 12) |
	
	       1 (dez) =      ???? (bin) |      4 (dez) =         ? (hex) 
	      10 (dez) =      ???? (bin) |     15 (dez) =         ? (hex)
	     126 (dez) = ???? ???? (bin) |     32 (dez) =        ?? (hex)
	    1298 (dez) = ???? ???? (bin) |   1298 (dez) =       ??? (hex)
		
	    0001 (bin) =         ? (dez) |  für die Schnelleren
	    1111 (bin) =         ? (dez) |     23'283 (dez) =  ???? (hex)
	 10'1010 (bin) =        ?? (dez) |  1011'1011 (bin) =   ??? (dez)
	101'1001 (bin) =        ?? (dez) |
	
	3 (hex) =  ? (dez) =  ???? (bin) |   5AF3 (hex) =    ??'??? (dez)
	5 (hex) =  ? (dez) =  ???? (bin) |   5AF3 (hex) = ???? ???? (bin)
	A (hex) =  ? (dez) =  ???? (bin) |        --> was fällt hier auf?
	F (hex) =  ? (dez) =  ???? (bin) |
	
	

Lösungen:
<br>15:50 min, D, YouTube [Einfache Einführung zu den Binärzahlen](https://www.youtube.com/watch?v=cKvAUzqDdJI) 
<br>04:35 min, D, YouTube [Das Binärsystem / Dualsystem ft. brainfaqk](https://www.youtube.com/watch?v=6WsI95N0QKU)
<br>06:34 min, D, YouTube [Das Hexadezimalsystem ft. TheSimpleMaths](https://www.youtube.com/watch?v=-6Je-FuAufk)


<br>
<br>
<br>
<br>
<br>
<br>

# Thema B - Komprimieren

Eine Komprimierung und Dekomprimierung "ohne" Verlust 
ist vornehmlich für Texte und einfache Grafiken
notwendig, weil wir dafür kein Verlust von Informationen 
haben können oder wollen. 

Bei Bilder, Videos und bei Musik, 
Sound und Sprache ist es nicht so schlimm, 
wenn "etwas" Verlust Komprimierung und 
der Dekomprimierung entsteht.


[**Einstieg**](https://gitlab.com/ch-tbz-it/Stud/m114/-/tree/main/B.%20Daten%20komprimieren)





## Komprimierung **ohne** Verlust

![komprimierung-ohne-verlust.png](x_ressourcen/komprimierung-ohne-verlust.png)

- [Komprimierung ohne Verlust (VLC/Morse, Huffmann, LZW, Zip)](https://gitlab.com/ch-tbz-it/Stud/m114/-/tree/main/B.%20Daten%20komprimieren/B.1%20Verlustlose%20Komprimierung)


Themen-Bausteine für **Komprimierungs-Techniken** Kodierung:

- Baustein A: <br>Das Prinzip **RLC / RLE**
- Baustein B: <br>Das Prinzip **HUFFMANN**
- Baustein C: <br>Das Prinzip **LZW**
- Baustein D: <br>Die Effizienz der **ZIP-Kompression**
- Baustein E: <br>Was ist und was bringt die **BWT (Burrows-Wheeler-Transformation)**?



**- A -** Studieren Sie die [**Lauflängen-Kodierung**](https://gitlab.com/ch-tbz-it/Stud/m114/-/tree/main/B.%20Daten%20komprimieren/B.1%20Verlustlose%20Komprimierung#rlc-run-length-coding-bzw-rle-run-length-encoding) 
und machen Sie dann **ein eigenes Beispiel auf Karo-Papier** 
um es dann im Detail den anderen Personen erklären zu können. 
Das Beispiel soll nicht nur im SW-Raum ("schwarz, weiss"), sondern 
im RGB-Raum (rot, grün, blau) funktionieren. Zeigen Sie 
auch die Effizienz der Komprimierung auf. Erstellen Sie Anschauungsmaterial
oder angefangene Beispiele oder eine Aufgabenstellung, für eine künftige Prüfungsaufgabe samt deren Lösung.

- [03:56 min, E, YouTube, Lossy and Lossless (RLE) Compression](https://www.youtube.com/watch?v=v1u-vY6NEmM)
- [09:38 min, D, YouTube, Lauflängencodierung - einfach erklärt](https://www.youtube.com/watch?v=EFsnoRGQ-xM)
- [15:43 min, D, YouTube, Lauflängencodierung, in RGB](https://www.youtube.com/watch?v=3QxPpfepv9E)
- [04:48 min, E, YouTube, Run Length Encoding](https://www.youtube.com/watch?v=oLaxHXgR0Xo)
- [04:40 min, E, YouTube, Run-Length Encoding](https://www.youtube.com/watch?v=cAAeSn8_aCU)


<br>


**- B -** Die [**Huffmann-Codierung**](https://gitlab.com/ch-tbz-it/Stud/m114/-/tree/main/B.%20Daten%20komprimieren/B.1%20Verlustlose%20Komprimierung#vlc-variable-length-coding) ist ein Teil der Komprimierung, 
die u.a. auch in **.mp3**, **.mpeg**, **.jpg** Verwendung findet (aber nicht nur).
Schauen Sie sich zuerst zwei oder drei der Videos an und 
entscheiden Sie dann in der Kleingruppe, 
welches Video Sie anderen Schüler:innen zeigen würden.
Machen Sie dann ein **Demo-Beispiel** z.B. für das Wort 
"GREIFENSEE SCHIFFAHRT" auf **Karo-Papier**, und 
zwar so, dass Sie es jemandem erklären könnten. 
Bereiten Sie zudem Hilfen (z.B. Teil-Lösungen, Algotrithmus-Sätze ...) 
vor, damit sich andere Lernende (auch Sie!) daran orientieren können.
(Er-)Stellen Sie weiter eine geeignete Prüfungsaufgabe samt deren Lösung.

- [07:22 min, D, YouTube, Huffman-Codierung - (So geht´s)](https://www.youtube.com/watch?v=eSlpTPXbhYw)
- [06:11 min, D, YouTube, Der Huffman Code](https://www.youtube.com/watch?v=qE4mEwHL62c)
- [11:06 min, E, YouTube, How Huffman Trees Work - Computerphile](https://www.youtube.com/watch?v=umTbivyJoiI)
- [Huffman Coding als Animation](https://people.ok.ubc.ca/ylucet/DS/Huffman.html) --> mit z.B. "GREIFENSEE SCHIFFFAHRT" versuchen.


<br>


**- C -** Studieren Sie das [**Lempel-Ziv-Welch** LZW](https://gitlab.com/ch-tbz-it/Stud/m114/-/tree/main/B.%20Daten%20komprimieren/B.1%20Verlustlose%20Komprimierung#lexikalisches-verfahren-lzw-lempel-ziv-welch-algorithmus)
-Kodierungsverfahren, das in **.zip**, **.7zip** usw. vorkommt 
indem Sie zuerst zwei oder drei der Videos anschauen und 
sich dann in der Kleingruppe entscheiden, 
welches Video die Sache am besten erklärt. 
Machen Sie weiter ein **eigenes Beispiel auf Karo-Papier**. 
Bereiten Sie zudem Hilfen (z.B. Teil-Lösungen, Algotrithmus-Sätze ...) 
vor, damit sich andere Lernende (auch Sie!) daran orientieren können.
(Er-)Stellen Sie weiter eine geeignete Prüfungsaufgabe samt deren Lösung.

- [07:01 min, D, YouTube, Komprimieren - GIF-Dateien - LZW Algorithmus](https://www.youtube.com/watch?v=fowlNsxfe9M)
- [08:16 min, D, YouTube, LZW-Kodierung](https://www.youtube.com/watch?v=dLvvGXwKUGw)
- [06:33 min, E, YouTube, Lempel Ziv Algorithm](https://www.youtube.com/watch?v=hHQgu4qILGs)
- [09:33 min, E, YouTube, LZW Encoding and Decoding Algorithm Explained and Implemented in Java](https://www.youtube.com/watch?v=1KzUikIae6k)
- [09:21 min, D, YouTube, LZW Kodierung](https://www.youtube.com/watch?v=rMLqpncKsk0)
- [10:54 min, D, YouTube, LZW Dekodierung](https://www.youtube.com/watch?v=mxqD315rYnA)

<br>

**- D -** Um eine anschuung zu geben, wie gut die ZIP-Kompression funktioniert
erstellen Sie zuerst einmal 5 Text-Dateien. Benutzen Sie für den Inhalt
einer der Textgeneratoren wie z.B. https://www.loremipsum.de

- eine Datei mit 10 Bytes
<br>eine Datei mit 100 Bytes
<br>eine Datei mit 1000 Bytes
<br>eine Datei mit 10000 Bytes
<br>eine Datei mit 100000 Bytes
<br>Machen Sie eine Statistik im Excel (man kann dort auch Grafiken erstellen)
und begründen Sie das Resultat.

- Erstellen Sie die gleichen 5 Dateien (gleiche Grössen), jedoch nur mit einem
Buchstaben oder Zeichen wie z.B. dem "A" oder dem " " (=Leerschlag, Space). Was
passiert dann mit den ZIP-Files?


- Erstellen Sie ein ZIP aus [Farbkreis-Lo-Res.jpg](./x_ressourcen/Farbkreis-Lo-Res.jpg)
<br>Erstellen Sie ein ZIP aus [Farbkreis-Hi-Res.jpg](./x_ressourcen/Farbkreis-Hi-Res.jpg)
<br>.. und notieren Sie die 4 Dateigrössen. 
<br>**Frage:** *Was* stellen Sie bezüglich der Dateigrössen und der 
*Effizienz/Wirksamkeit* fest und versuchen Sie eine Begründung dazu zu geben.

<br>

**- E -** Analysieren Sie die Funktionsweise der BWT-Transformation, 
also finden Sie heraus, wie das funktioniert. Sie werden sehen, dass 
es selber noch keine Komprimierung macht. 

- https://de.wikipedia.org/wiki/Burrows-Wheeler-Transformation
- 03:51, E, YouTube, [Burrows Wheeler Transformation](https://www.youtube.com/watch?v=Bqdx55Hz20s)
- 04:36, E, YouTube, [Burrows Wheeler Transformation](https://www.youtube.com/watch?v=eBsnfozFqM8)
- **Frage:** * **Was bringt** * diese Technik wenn man sie * **womit** * kombiniert?


<br>
<br>
<br>

**- F -** Die LZ77-Komprimierung

- 08:42, E, YouTube, [LZ77-Komprimierung](https://www.youtube.com/watch?v=goOa3DGezUA)

<br>
<br>


## Komprimierung **mit** Verlust

![komprimierung-mit-verlust1.jpg](x_ressourcen/komprimierung-mit-verlust1.jpg)

- [Komprimierung mit Verlust (Bild, Video, Sound)](https://gitlab.com/ch-tbz-it/Stud/m114/-/tree/main/B.%20Daten%20komprimieren/B.2%20Verlustbehaftete%20Komprimierung)




### Funktionsweise MP3 (Sound-Komprimierung)

https://de.wikipedia.org/wiki/MP3

- 15:41 min, D, [Unterschied zwischen WAV und MP3](https://www.youtube.com/watch?v=JQ8FPVaFQTY)
- 07:35 min, D, [Kannst du MP3s raushören? Und was ist MP3 eigentlich?](https://www.youtube.com/watch?v=OhjO5bosrTU)
- 04:40 min, D, [Meinen Podcast als WAV hochladen? – Alles zu .WAV-Dateien!](https://www.youtube.com/watch?v=7QZ1f4_6O6Q)



### Funktionsweise JPEG (Bild-Komprimierung)

Was ist **DCT** und wie ist das **JPEG**-Format aufgebaut, bzw. wie funktioniert diese Art der Bildkomprimierung?

- Detaillierte Erklärung von **Mike Pound**, University of Nottingham 
	- 07:30 min, E, [Colourspaces (JPEG Pt0)](https://www.youtube.com/watch?v=LFXN9PiOGtY)
	- 07:18 min, E, [JPEG 'files' & Colour (JPEG Pt1)](https://www.youtube.com/watch?v=n_uNPbdenRs)
	- 15:11 min, E, [JPEG **DCT**, Discrete Cosine Transform (JPEG Pt2)](https://www.youtube.com/watch?v=Q2aEzeMDHMA&t=6s)
	- 05:36 min, E, [The Problem with JPEG](https://www.youtube.com/watch?v=yBX8GFqt6GA&t=33s)

- Übersichtliche Erklärung von **Leo Isikdogan** und von **BranchEducation**
	- 06:51 min, E, [JPEG - How Image Compression Works, Leo Isikdogan](https://www.youtube.com/watch?v=Ba89cI9eIg8)
	- 18:46 min, E, [How are Images Compressed? (46MB -> 4MB) JPEG In Depth, BranchEducation](https://www.youtube.com/watch?v=Kv1Hiv3ox8I)

<br>

![JPGFindings1.jpg](x_ressourcen/JPGFindings1.jpg)
![JPGFindings2.jpg](x_ressourcen/JPGFindings2.jpg)
![JPGFindings3.jpg](x_ressourcen/JPGFindings3.jpg)


**kleine Praxisaufgabe** 

Vergleichen Sie die unterschiedlichen Bildformate [Samples.zip](./x_ressourcen/Samples.zip)


### Funktionsweise M4V (Video-Komprimierung)

![](./x_ressourcen/WT_Filmkomprimierung1.jpg)

<br>
<br>
<br>
<br>

![x_ressourcen/openshot-3-1-1-screenshot.png](x_ressourcen/openshot-3-1-1-screenshot.png)

**Praxisaufgabe / selber ausprobieren**

Auftrag im TEAMS "Video erzeugen und komprimieren"

Zeitbedarf: ca 60-70 min

- Erstellen Sie ein 10 Sek. Video mit Ihrem Smartphone oder nehmen eines was Sie früher schon aufgenommen haben aus Ihrem Smartphone heraus und transportieren Sie es in Ihr Notebook oder PC
- Installieren Sie **https://www.openshot.org** (free) oder eine ähnliche Software **https://shotcut.org**. 
- Importieren Sie Ihr kleines Video in OpenShot
- Trennen Sie die Tonspur ab und löschen die Tonspur
- Unterlegen Sie Ihr Video mit einem Sound Ihrer Wahl **https://freemusicarchive.org**, **https://freemp3cloud.com/de** 
- Falls das Video länger als 10 Sek. ist, kürzen Sie es auf 10 Sek.
- Erzeugen Sie mindestens 2 Formate (via Export (roter Button)) wovon eines davon möglichst komprimiert und klein werden soll und das andere so gross und unkomprimiert wie möglich
- Machen Sie ein ScreenShot der Datei-Speichergrössen der beiden Dateien auf dem Dateisystem (Explorer)
- Laden Sie das kleinere der beiden Videos und den ScreenShot in TEAMS hoch

Software:

- https://openshot.org
- https://shotcut.org

<br>
<br>
