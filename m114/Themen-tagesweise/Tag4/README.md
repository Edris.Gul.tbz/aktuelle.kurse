## Tag 4 - Verlustfreie Datenreduktion / Huffmann / RLE / LZW

[M114_Tag4_Fragensammlung_Komplementbildung.txt](_M114_Tag4_Fragensammlung_Komplementbildung.txt)

```
Anderes Wort für invertieren?

Was ist der unterschied zwischen 1er und 2er Komplement?

Wie ist eine Bitmap aufgebaut?

Was muss man beachten wenn man ein 2er Komplement macht?

Was ist die negativste Zahl wenn man eine 4 Bit Zahl hat?

welches Komplement hat das Redundanzproblem behoben?

Was ergibt 1001 - 1111 ?

Was hat das 1-er Komplement für Nachteile? Nenne einen

Was ergibt 0110 + 1101 ?

Was ist in binär -3?

Warum stehet bei -7 in Binär unendlich viele Eines?
 
Wie funktioniert 2-er Komplement?

Warum funktioniert die standart Variante (das erste Bit stellt das Vorzeichen dar) nicht?

Was muss beim 1-er Komplement beim Übertrag gemacht werden?

Was macht man beim 2-er Komplement, falls die positive Zahl mehr als 8 beträgt?

```

00_Entropy in Compression (what is the minimum to compress data)
<br>12:11 min, YouTube, E
<br>Verweis: https://www.youtube.com/watch?v=M5c_RFKVkko
 	 	 	 	 	 	 

- [01_Datenreduktion1.pdf](01_Datenreduktion1.pdf)
 	
### Huffman Algorithmus

01_Huffman-Algorithmus - Vorlesung
<br>Verweis: http://www.ziegenbalg.ph-karlsruhe.de/materialien-homepage-jzbg/cc-interaktiv/huffman/mathinfkom.pdf
 	 	 	 	 	 	 
01_Huffman-Animation
<br>Verweis: https://people.ok.ubc.ca/ylucet/DS/Huffman.html

	
01_Huffman-Codierung - (So geht´s) - deutsch
<br>7:22 min, YouTube
<br>Verweis: https://www.youtube.com/watch?v=eSlpTPXbhYw
 	 	 	 	 	 	 

- [01_HuffmanLSG.ppt](01_HuffmanLSG.ppt)
 

### Datenreduktion durch niedrigere Bildauflösung, RLE
 	 	 	 	 	 	 
- [02_RLE_Datenreduktion2.pdf](02_RLE_Datenreduktion2.pdf)

- [02_RLE_Datenreduktion_Loesung_Bitberechnung.doc](02_RLE_Datenreduktion_Loesung_Bitberechnung.doc)

- 02_RLE_Lauflängencodierung
<br>Verweis: https://de.ccm.net/contents/665-rle-komprimierung-lauflaengencodierung
<br>Verweis: https://de.wikipedia.org/wiki/Laufl%C3%A4ngenkodierung
<br>2:06 min, YouTube, D
<br>Verweis: https://www.youtube.com/watch?v=QQbLs3vZcqQ
 	 	 	 	 	 	 
	
### Burrows-Wheeler Transformation BWT

- [03_BWT_Datenreduktion3.pdf](03_BWT_Datenreduktion3.pdf)

19:03 min, E, YouTube, 27.2.2015, GoogleDeveloppers
<br>Verweis: https://www.youtube.com/watch?v=4WRANhDiSHM
 	 	 	 	 	 	 

### Lempel-Ziv-Welch Algorithmus (LZW, LZ77, ~zip)

- [04_LZW_Datenreduktion4.pdf](04_LZW_Datenreduktion4.pdf)
 	 	 	 	 	 	 
04_LZ77_Compression
<br>8:42 min, YouTube, E
<br>Verweis: https://www.youtube.com/watch?v=goOa3DGezUA
 	 	 	 	 	 	 
04_LZW_Algorithmus
<br>Verweis: https://de.wikipedia.org/wiki/Lempel-Ziv-Welch-Algorithmus
 	 	 	 	 	 	 
 	 	 	
