# Thema A - Daten codieren

## Einstieg in die Themen Kommunikation, Kodierung, Zeichen

[**Einstieg**](https://gitlab.com/ch-tbz-it/Stud/m114/-/tree/main/A.%20Daten%20codieren)


3:27 min, [Zeichensprache (Witz vom Olli)](https://www.youtube.com/watch?v=dW3FGs8oaKg&t=8s)

5:16 min, [Gebärdensprache - eine kleine Einführung](https://www.youtube.com/watch?v=BAB4n84F1og)

[Verschiedene Codes (pdf)](./x_ressourcen/Morse-BCD-ASCII-Gray-Hamming-Codes.pdf)



- [Zahlensysteme, numerische Codes (Unsigned und Signed)](https://gitlab.com/ch-tbz-it/Stud/m114/-/tree/main/A.%20Daten%20codieren/A.1%20Zahlensysteme,%20numerische%20Codes)
- [Alphanumerische Codes ASCII und Unicode](https://gitlab.com/ch-tbz-it/Stud/m114/-/tree/main/A.%20Daten%20codieren/A.2%20Alphanumerische%20Codes%20ASCII%20und%20Unicode)
- [Zusammengesetzte Codierung, Barcodes](https://gitlab.com/ch-tbz-it/Stud/m114/-/tree/main/A.%20Daten%20codieren/A.3%20Zusammengesetzte%20Codierung,%20Barcodes)
- [Bildcodierung / Multimedia](https://gitlab.com/ch-tbz-it/Stud/m114/-/tree/main/A.%20Daten%20codieren/A.4%20Bildcodierung)


### Vorwissen aktivieren zum Thema Codieren

```
- 10 min alleine - Wichtig: schriftlich auf Papier (nicht elektronisch !!) und ohne zu recherchieren
- 10 min zu zweit (Recherchen erlaubt)
- 10 min in der Klasse berichten 
```

```
Frage 1: Was braucht es, um unsere Ideen anderen Leuten mitteilen zu können?

Frage 2: Wo gibt es Codes? Wofür haben wir sowas erfunden?
	
Frage 3: Wie kommunizieren wir ohne elektronische Mittel?  
		 Auf kurze (3 m) und auf längere Distanzen (30 m)?
		
Frage 4: Wie kann man eine Information über eine Sicht-Weite 
		 von etwa 100 Meter übermitteln, wenn Sie keinerlei 
		 elektronische Mittel zur Verfügung haben? 
		
Frage 5: Wie kann man eine Information über eine Sicht-Weite 
		 von etwa 400-1000 Meter übermitteln, wenn Sie keinerlei 
		 elektronische Mittel zur Verfügung haben? 
```		

![lützelsee](x_ressourcen/luetzelsee2.jpg)
<br>
<br>
<br>
<br>
<br>
<br>
<br>

### Themen-Bausteine Sprache und "Alphabete"


[**Alphabete** (im weiteren Sinn)](https://de.wikipedia.org/wiki/Alphabet#Alphabete_im_weiteren_Sinn)

- Baustein A: <br>Morse-Alphabet [2min](https://www.youtube.com/watch?v=_IlZrZ9N4ig)
- Baustein B: <br>Flaggen-Alphabet [2min](https://www.youtube.com/watch?v=nVuKrGOuC2c)
- Baustein C: <br>Semaphoren-Alphabet, Winker-Alphabet [3min](https://www.youtube.com/watch?v=SulHXA4JbE8)
- Baustein D: <br>ASCII-Alphabet (inkl. Unterschied 7-Bit, 8-Bit) [search..](https://www.google.ch/search?q=ascii+utf-8)
- Baustein E: <br>Braille-Schrift [search..](https://www.google.ch/search?q=braille)


| <mark>AP21d</mark> | ExpertGrp A | ExpertGrp B | ExpertGrp C | ExpertGrp D | ExpertGrp E|
|--------------------|----         |----         |----         |----         |----        |
| **StammGrp 1**     | Bajra       | Boulter     | Engeli      | Hamza       | Wagner     |
| **StammGrp 2**     | Hotz        | Jong        | Landa       | Manser      |            |
| **StammGrp 3**     | Monje       | Odermatt    | Paris       | Rasi        |            |
| **StammGrp 4**     | Rieder      | Russ        | Samma       | Schönhaar   | Syla       |


<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>


### Vorwissen aktivieren: Optische Codes

<br>
<br>

```
- 10 min alleine - Wichtig: **schriftlich** auf Papier (nicht elektronisch !!) und **ohne zu recherchieren** 
- 10 min zu zweit (Recherchen erlaubt)
- 10 min in der Klasse berichten 
```

```
Frage 1: Was heisst *OCR*-Code und wo kommen solche im Alltag vor?
Frage 2: Was heisst *Bar*-Code und wo kommen solche im Alltag vor?
Frage 3: Was heisst *QR*-Code und wo kommen solche im Alltag vor?
Frage 4: Wie funktionieren Bar-Codes und kann man die ohne techn. Hilfsmittel selber erkennen?
Frage 5: Wie funktionieren QR-Codes und kann man die ohne techn. Hilfsmittel selber erkennen?
```

<br>
<br>

#### Vorkommen von optischen Codes

<br>[OCR-A](https://de.wikipedia.org/wiki/OCR-A), [Bild](https://commons.wikimedia.org/wiki/Category:OCR-A_typeface_samples?uselang=de)
<br>[OCR-B](https://de.wikipedia.org/wiki/OCR-B), [https://www.ocrb.ch/](https://www.ocrb.ch/), [Bild 'esr'](https://www.ocrb.ch/assets/images/esr-demo.jpg)
<br>[Codes auf Briefen 2](x_ressourcen/M114-Codes2.pdf)
<br>[Codes auf Briefen 3](x_ressourcen/M114-Codes3.pdf)
<br>[Codes auf Briefen 4](x_ressourcen/M114-Codes4.pdf)
<br>[Verpackungscode auf (maschinellen) Rechnungen](x_ressourcen/2022-10-17_Allianz_Kombi-Haushaltsversicherung.pdf)


- Baustein A: <br>Welche Bar-Codes gibt es ([EAN-8 Barcode](x_ressourcen/Barcode.pdf), EAN-13,) und [wie funktionieren](https://www.youtube.com/watch?v=GOYRT3-0IJI) sie? 
- Baustein B: <br>Welche QR-Codes gibt es und [wie funktioniert](https://www.youtube.com/watch?v=yiLjWBfQyF4) der meist-verbreitete?
- Baustein C: <br>Wie funktionieren RFID und [wie funktioniert](https://www.youtube.com/watch?v=YYHwITj7nWw) es technisch?

<br>
<br>[QR-Codes in 100 Sekunden erklärt, 1:42 min, D, 2013](https://www.youtube.com/watch?v=qWgg1StZ2Kw)
<br>
<br>
<br>

| <mark>AP21d</mark> | ExpertGrp A | ExpertGrp B | ExpertGrp C | 
|--------------------|----         |----         |----         |
| **StammGrp 1**     | Bajra       | Engeli      | Hamza       | 
| **StammGrp 2**     | Hotz        | Landa       | Manser      | 
| **StammGrp 3**     | Monje       | Paris       | Rasi        | 
| **StammGrp 4**     | Rieder      | Russ        | Samma       | 
| **StammGrp 5**     | Syla        | Wagner      | Odermatt    |
| **StammGrp 6**     | Schönhaar   | Jong        | Boulter     | 


<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

## Digitalisierung



[![Die Wurzeln der Digitalisierung](x_ressourcen/die-wurzeln-der-digitalisierung.jpg)
](x_ressourcen/DieWurzelnDerDigitalisierung_2019-08-18.pdf)
<br>
<br>
<br>
![](./x_ressourcen/bahnhofsuhr-bahnhof-binaer-stgallen-1030x459.jpg)
<br>[Die **St. Galler Bahnhofsuhr**](https://www.bahnhofsuhrsg.ch)
<br>
<br>[Zahlenanzeige / **Tankstellenanzeige** (*nur für Windows-Systeme, weil in C# geschrieben*)](x_ressourcen/tankstellenanzeige), [.zip](x_ressourcen/Tankstellenanzeige.zip)
<br>Zum selbermachen [Binärsystem (.xlsx)](x_ressourcen/binaer-rechner.xlsx)



<br>
<br>
<br>
<br>
<br>

### Themen-Bausteine "Zahlensysteme"

- Baustein A: Wie ist das Oktalsystem aufgebaut und wie geht die Umrechnung vom Oktalsystem zum Dezimalsystem
- Baustein B: Wie ist das Dualsystem aufgebaut und wie geht die Umrechnung vom Dezimal- ins Dualsystem?
- Baustein C: Wie ist das Hexadezimalsystem aufgebaut und wie geht die Umrechnung vom Hexadezimal- ins Dezimalsystem
- Baustein D: Wie geht die Umrechnung vom Dual- ins Dezimalsystem (mit Komma)


<br>03:57 min, D, [001-Dezimal-ins-Dual-Zahlensystem-umwandeln.mp4](x_ressourcen/001-Dezimal-ins-Dual-Zahlensystem-umwandeln.mp4)
<br>03:59 min, D, [002-DualBinaerzahlen-ins-Dezimalsystem-umwandlen.mp4](x_ressourcen/002-DualBinaerzahlen-ins-Dezimalsystem-umwandlen.mp4)
<br>04:24 min, D, [003-DualBinaerzahlen-ins-Dezimalsystem-umwandeln-mit-Komma.mp4](x_ressourcen/003-DualBinaerzahlen-ins-Dezimalsystem-umwandeln-mit-Komma.mp4)
<br>04:14 min, D, [004-Hexadezimal-ins-Dezimal-umwandeln.mp4](x_ressourcen/004-Hexadezimal-ins-Dezimal-umwandeln.mp4)
<br>

| <mark>AP21d</mark> | ExpertGrp A | ExpertGrp B | ExpertGrp C | ExpertGrp D |
|--------------------|----         |----         |----         |----         |
| **StammGrp 1**     | Bajra       | Boulter     | Engeli      | Hamza       | 
| **StammGrp 2**     | Hotz        | Samma       | Manser      | Landa       |
| **StammGrp 3**     | Monje       | Paris       | Syla        | Rasi, Wagner| 
| **StammGrp 4**     | Rieder      | Russ, Jong  | Odermatt    | Schönhaar   | 


<br>
<br>




### Themen-Bausteine "Kodierungen", "Zeichen" und "Alphabete"

<<<<<<< HEAD
- Baustein A: Unterschied ASCII-Code, ANSI-Code, HTML-Code (beginnen Sie mit der [Asciitable](https://www.asciitable.com) und forschen Sie weiter)
- Baustein B: Unterschied UTF-8, UTF-16, UTF-32 und welche ISO-Code-Tabellen gibt es? Erstellen Sie auch eine Übersicht.
- Baustein C: Unterschied ISO-Codes (z.B. ISO-8859-1) und UTF-8 und Unicode (forschen Sie beginnend mit [UC-Grp](https://www.codetable.net/groups) und [UC-Chars](https://www.codetable.net/unicodecharacters) und mit [Unicode.pdf](x_ressourcen/Unicode.pdf) )

=======
- Baustein A: <br>Unterschied ASCII-Code, ANSI-Code, HTML-Code (beginnen Sie mit der [Asciitable](https://www.asciitable.com) und forschen Sie weiter)
- Baustein B: <br>Unterschied UTF-8, UTF-16, UTF-32 und welche ISO-Code-Tabellen gibt es? Erstellen Sie auch eine Übersicht.
- Baustein C: <br>Unterschied ISO-Codes (z.B. ISO-8859-1) und UTF-8 und Unicode (forschen Sie beginnend mit [UC-Grp](https://www.codetable.net/groups) und [UC-Chars](https://www.codetable.net/unicodecharacters) und mit [Unicode.pdf](x_ressourcen/Unicode.pdf) )
>>>>>>> 78208510b221d9750c6603e7278c15c0510d2ca7

<br>

| <mark>AP21d</mark> | ExpertGrp A | ExpertGrp B | ExpertGrp C |
|--------------------|----         |----         |----         |
| **StammGrp 1**     | Bajra       | Boulter     | Engeli      |
| **StammGrp 2**     | Hotz        | Samma       | Manser      |
| **StammGrp 3**     | Monje       | Paris       | Syla        |
| **StammGrp 4**     | Rieder      | Russ        | Odermatt    |
| **StammGrp 5**     | Wagner      | Jong        | Hamza       |
| **StammGrp 6**     | Schönhaar   | Landa       | Rasi        |



