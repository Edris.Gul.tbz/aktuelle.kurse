# Thema C - Kryptologie


[**Einstieg**](https://gitlab.com/ch-tbz-it/Stud/m114/-/tree/main/C.%20Grundlagen%20Kryptografie)

- [**Kryptologie** (hornetsecurity)](https://www.hornetsecurity.com/de/wissensdatenbank/kryptographie)

## Das [Symmetrische Verschlüsselungskonzept](https://gitlab.com/ch-tbz-it/Stud/m114/-/tree/main/C.%20Grundlagen%20Kryptografie/C.1%20Symmetrische%20Verschl%C3%BCsselung)



Zeigen Sie, wie Sie mit [Steganografie](1-Symmetrisch/steganographie.md) etwas verschlüsseln können. [Weitere Unterlagen](1-Symmetrisch/steganographie.md)<br>


### Aufträge


Erklären Sie den Ihnen zugewiesenen Themen-Baustein.
<br>Machen Sie Aussagen über wichtige Vertreter und/oder wichtige "Vertreter" oder "Erfinder" dieses Bausteins.
<br>Zeigen Sie das System und/oder die Funktionsweise auf. Machen Sie das **in möglichst plastischer, anschaulicher und/oder in physischer Form** (z.B. Papier).
<br>Bereiten Sie ein Vortrag an der Wandtafel oder auf/für ein Demo-Tisch vor.
<br>Bereiten Sie eine Prüfungsfrage und deren Antwort vor, das Sie zuhanden der Klasse in TEAMS (Allg .Chat) abgeben


- Baustein A:<br>Was ist eine [Symetrische Verschlüsselung](1-Symmetrisch/2-TopSecret1.pdf) ? - [Wikipedia: Symmetrisches_Kryptosystem](https://de.wikipedia.org/wiki/Symmetrisches_Kryptosystem), [hornetsecurity.com](https://www.hornetsecurity.com/de/wissensdatenbank/kryptographie/#symetrische-verschluesselung)?<br>

- Baustein B:<br>Zeigen Sie die [Cäsar-Verschlüsselung, 15:42 min, YouTube, D](https://www.youtube.com/watch?v=mn-b36ax4PQ), [Text](https://www.hornetsecurity.com/de/wissensdatenbank/kryptographie/#caesar-verschluesselung) und die Rotationschiffre<br>

- Baustein C:<br>Zeigen Sie die [Vigenère-Verschlüsselung, 11:17 min, YouTube, D](https://www.youtube.com/watch?v=u6i4kKzeOWA)<br>

- Baustein D:<br>Unterschied [Blockchiffre/Stromchiffre](1-Symmetrisch/3-Stromchiffre.ppt)<br>

- Baustein E:<br>Zeigen Sie wie die Blockchiffre [Blowfish](https://de.wikipedia.org/wiki/Blowfish) funktioniert<br>

- Baustein F:<br>Zeigen Sie wie die Verschlüsselungsmethode [Twofish](https://de.wikipedia.org/wiki/Twofish) funktioniert<br>

- Baustein G:<br>Zeigen Sie den [Data Encryption Standard (DES)](https://www.hornetsecurity.com/de/wissensdatenbank/kryptographie/#data-encryption-standard) und das "Triple DES" (3DES)<br>

- Baustein H:<br>Zeigen Sie den [Advnced Encryption Standard (AES)](https://www.hornetsecurity.com/de/wissensdatenbank/kryptographie/#advanced-encryption-standard)<br>

- Baustein J:<br>Zeigen Sie den [International Data Encryption Algorithm (IDEA)](https://de.wikipedia.org/wiki/International_Data_Encryption_Algorithm)<br>

- Baustein K:<br>Zeigen Sie die Stromchiffre, die Rivest Cipher [RC2](https://de.wikipedia.org/wiki/RC2_(Blockchiffre)) und [RC5](https://de.wikipedia.org/wiki/RC5) <br>

<br>

### Schlüsselaustauschverfahren

Das Problem ist, dass ich bei der Übermittlung einer Nachricht eigentlich auch den Schlüssel zur Nachricht mitgeben muss. Es handelt sich hier um eine **symmetrische Verschlüsselung**, das heisst, dass zum verschlüsseln und zum entschlüsseln, also **auf beiden Seiten, der gleiche Schlüssel** verwendet wird.

Aber wenn ich den Schlüssel der Nachricht mitgebe, dann kann jeder auch die verschlüsselte Nachricht entschlüsseln und lesen. Der Schlüssel muss also "geheim" übermittelt werden.

So müsste ich den Schlüssel vor oder nach dem Verschicken der Nachricht dem Empfänger bekanntgeben.

Wenn ich den Schlüssel auf einem anderen Weg dem Empfänger geben muss, dann wird das auch schwierig.

Im Jahr 1976 haben die Forscher [W.Diffie und M.Hellmann und auch R.Mekle (Wikipedia .. )](https://de.wikipedia.org/wiki/Diffie-Hellman-Schl%C3%BCsselaustausch) ein Austauschverfahren vorgestellt, mit der es sicher geworden ist, Nachrichten zu verschlüsseln und zu entschlüsseln ohne dass man den ganzen Schlüssel übertragen musste. Zugrunde liegt ein mathematisches Verfahren.

- Das [Diffie-Hellman-Schlüsselaustauschverfahren, 6:01 min, D](https://www.youtube.com/watch?v=_E0SGl7aN70) anschaulich erklärt auf Whiteboard.

- [Secret Key Exchange, 8:39 min, E](https://www.youtube.com/watch?v=NmM9HA2MQGI) anschaulich erklärt mit Papier und mit Flüssigkeiten und [Diffie-Hellmann-Mathematics, 7.04 min, E](https://www.youtube.com/watch?v=Yjrfm_oRO0w).
<br>
<br>
<br>

##  Das [Asymmetrische Verschlüsselungskonzept](https://gitlab.com/ch-tbz-it/Stud/m114/-/tree/main/C.%20Grundlagen%20Kryptografie/C.2%20Asymmetrische%20Verschl%C3%BCsselung)


### Lernstandsanalyse

	Welche Prinzipien werden bei JPEG-Komprimierung angewendet?

	Welche Prinzipien werden bei der MPEG, MP3, M4A - Komprimierung angewendet?

	Wie geht die mathematische Funktion "modulo"

	Was ergibt 250 mod 16 ?

	Wie geht die Rotations-Chiffre? (Cäsar-Verschlüsselung)

	Was ist das Prinzip der Vigenère Verschlüsselung?
	


### Prä-Instruktion

	Machen Sie eine Primzahlenzerlegung der Zahl 12, der Zahl 100, der Zahl 144.

	Finden Sie die erste 3-stellige Primzahl (die erste Primzahl über 99).

	Finden Sie die erste 4-stellige Primzahl (die erste Primzahl über 999).

	Installieren Sie "PuTTY" auf Ihrem Rechner und
	clicken Sie doppelt auf das Programm "puttygen.exe".
	Erstellen Sie ihr eigenes PublicKey-/PrivateKey-Paar 
	mit der Stärke 4096 und speichern Sie diese bei sich ab.

	Was kann man damit machen?

	Wer bekommt den Public-Key und wer den Private-Key?

	Installieren Sie den "FileZilla Client" und versuchen Sie den 
	Ort zu finden, wo man den Private-Key speichern kann.



- [Grundsatz Moderner Kryptologie](https://www.hornetsecurity.com/de/wissensdatenbank/kryptographie/#grundsatz-moderne-kryptographie)
- [Asymmetrische Verschlüsselung](https://www.hornetsecurity.com/de/wissensdatenbank/kryptographie/#asymetrische-verschluesselung)
- [PGP](https://www.hornetsecurity.com/de/wissensdatenbank/kryptographie/#pgp)
- [s/MIME](https://www.hornetsecurity.com/de/wissensdatenbank/kryptographie/#s-mime)
- [Brute force attacken](https://www.hornetsecurity.com/de/wissensdatenbank/kryptographie/#brute-force-attacken)
- [Man in the middle](https://www.hornetsecurity.com/de/wissensdatenbank/kryptographie/#man-in-the-middle)
- [wie weiter..](https://www.hornetsecurity.com/de/wissensdatenbank/kryptographie/#zukunftsblick)



## Kryptographie [Digital signieren](https://gitlab.com/ch-tbz-it/Stud/m114/-/tree/main/C.%20Grundlagen%20Kryptografie/C.3%20Digital%20signieren)

Richten Sie Ihr EMail-Programm die digital signierte EMail-Verschlüsselung ein 
und machen Sie die entsprechenden Tests.

Wie geht das, was muss man machen? Erstellen Sie eine Kurzanleitung 
und geben Sie sie im TEAMS-Auftrag ab.

<br>
<br>

## Das RSA-Kryptosystem

	
Das RSA-Kryptosystem
18:57 min, D, YouTube, Weiz, 2018
Verweis: https://www.youtube.com/watch?v=mDRMzBlI3U4


RSA - Verschlüsselung
<br>47 min, 5 Videos in YouTube Playlist, Vorlesung Prof. Chr. Spannagel
<br>Verweis: https://www.youtube.com/playlist?list=PLdVk34QLniSBox_E5IFU9S4zSszFE2RsJ&disable_polymer=true
 	 	 	 	 
	
RSA Verschlüsselung mit Schlüsselgenerierung und Modelldurchlauf- it-archiv.net
<br>8:50 min, D, YouTube, https://www.youtube.com/watch?v=gFd_SZZF63I
 	 	 	 	 
	

<br>

## The Imitation Game - Hausaufgabe bis 10.7.2023

Aus diesem Film wird eine Prüfungsfrage gestellt

[![x_ressourcen/the-imitation-game.jpg](x_ressourcen/the-imitation-game.jpg)](https://nanoo.tv/link/v/FPqRbmAT)
<br> 
(NANOO.TV, abrufbar bis ende 2023)

<br>

- [Enigma Code & Touring.Machine](https://www.hornetsecurity.com/de/wissensdatenbank/kryptographie/#enigma-turing)


<br>

Wie ein Mathegenie Hitler knackte. ARTE Doku
<br>1:13:06, D, YouTube, 2017, https://www.youtube.com/watch?v=ttRDu4wuVTA
 	
<br>

Alan Turing - Wer war er eigentlich
<br>5:17 min, D, YouTube, https://www.youtube.com/watch?v=hGsmzgY_QVo

<br>



| AP21a     | ExpGrp A | ExpGrp B | ExpGrp C | ExpGrp D | ExpGrp E |
|----       |-----     |----      |----      |----      |----      |
| StammGrp 1| | | | | |
| StammGrp 2| | | | | |
| StammGrp 3| | | | | |
| StammGrp 4| | | | | |
| StammGrp 5| | | | | |



| AP21d     | ExpGrp A | ExpGrp B | ExpGrp C | ExpGrp D | ExpGrp E |
|----       |-----     |----      |----      |----      |----      |
| StammGrp 1| | | | | |
| StammGrp 2| | | | | |
| StammGrp 3| | | | | |
| StammGrp 4| | | | | |
| StammGrp 5| | | | | |
