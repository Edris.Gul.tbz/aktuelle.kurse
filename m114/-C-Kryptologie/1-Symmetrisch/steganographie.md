# Kryptologie

## Steganigraphie

[Steganographie, hornetsecurity.com](https://www.hornetsecurity.com/de/wissensdatenbank/kryptographie/#steganographie)

- Steganografie 1, [3 min, D, YouTube, Erklärvideo Bilderverschlüsselung (Steganographie)](https://www.youtube.com/watch?v=k4W52FCWtvI)
- Steganografie 2, [3 min, D, YouTube, Kryptologie-Steganographie](https://www.youtube.com/watch?v=SBTi1CkmHUQ)
- Steganografie 3, [4 min, D, YouTube, Steganographie Theorie](https://www.youtube.com/watch?v=EzKbsfGhC4w)
- Steganografie 4, [7 min, D, YouTube, Hacking mit Python - Text in Bildern und MP3s verstecken](https://www.youtube.com/watch?v=PK3L62Vkux4)
- Steganografie 5, [7 min, D, YouTube, Nachricht in Bild verstecken - Tutorial](https://www.youtube.com/watch?v=XZDvwBQ6hXg)
- Steganografie 6, [7 min, D, YouTube, Verstecke Nachrichten/Dateien in Bildern](https://www.youtube.com/watch?v=XcX3JY30tqU)
- Steganografie 7, [13:13 min, E, YouTube, Secrets Hidden in Images (Steganography, Mike Pound)](https://www.youtube.com/watch?v=TWEXCYQKyDc)
