[TOC]

# M114 - Codierungs-, Kompressions- und Verschlüsselungsverfahren einsetzen

[**Modulidentifikation** ICT CH ](https://www.modulbaukasten.ch/module/114/4](https://www.modulbaukasten.ch/module/114/4) 

[**Kompetenzmatrix** - Kt. ZH](./0-Modulidentifikation-Kompetenzraster-Leistungsbeurteilungen/kompetenzraster_2023-05.pdf)

[**Umsetzung** - TBZ https://gitlab.com/ch-tbz-it/Stud/m114](https://gitlab.com/ch-tbz-it/Stud/m114)


## Bewertung:

Der ganze "Kurs" wird vermehrt in der [**Gruppen-Puzzle**](Gruppen-Puzzle)-Technik durchgeführt. Es braucht für die Lernenden ein gewisses erhöhtes Engagement. Ich hoffe, es wird viel Spass macht und der Lerneffekt entsprechend hoch sein wird.

Für jeden Abschnitt wird von den Gruppen/der Klasse die entsprechenden
Fragen formuliert. Am Schluss des "Kurses" gibt es eine Prüfung,
(oder 2 oder 3 Teil-Prüfungen?), soll jede:r einzelne alleine "alles" beantworten.

## Ablaufplan 2023-Q4

|Tag  |AP21a | AP21d |Thema, Auftrag, Übung |
|---- |----  |----   |----                  |
| 1.) | Di 15.05.| fällt aus  | [A - Kodieren](./-A-Kodieren) |
| 2.) | Di 23.05.| Do 25.05.  |  |
| 3.) | Di 30.05.| Do 31.05.  | [B - Komprimieren](./-B-Komprimieren) |
| 4.) | Di 06.06.| Do 08.06.  |  |
| 5.) | Di 13.06.| Do 15.06.  | [C - Kryptologie](./-C-Kryptologie) |
| 6.) | Di 20.06.| Do 22.06.  |  |
| 7.) | Di 27.06.| Do 29.06.  | [D - Sichere Kommunikation](./-D-sichereKommunikation) |
| 8.) | Di 04.07.| Do 06.07.  |  |
| 9.) | Di 11.07.| Do 13.07.  | Prüfung <br> anschliessend Reserve-Themen |



## Unterlagen TBZ:

Neuere Aufarbeitung des Modules:

https://gitlab.com/ch-tbz-it/Stud/m114

- A. [Daten codieren](https://gitlab.com/ch-tbz-it/Stud/m114/-/tree/main/A.%20Daten%20codieren)
- B. [Daten komprimieren](https://gitlab.com/ch-tbz-it/Stud/m114/-/tree/main/B.%20Daten%20komprimieren)
- C. [Grundlagen der Kryptologie](https://gitlab.com/ch-tbz-it/Stud/m114/-/tree/main/C.%20Grundlagen%20Kryptografie)
- D. [Gesicherte Datenübertragung](https://gitlab.com/ch-tbz-it/Stud/m114/-/tree/main/D.%20Gesicherte%20Daten%C3%BCbertragung)


Auf der (früheren) Website von Jürg Arnold ist zu finden:

- [ARJ 01-Codesysteme](https://www.juergarnold.ch/codesysteme.html)
- [ARJ 02-Verlustlose Datenkompression](https://www.juergarnold.ch/kompression.html)
- [ARJ 03-Datenkompression mit Verlust & Multimedia](https://www.juergarnold.ch/videotechnik.html)
- [ARJ 04-Kryptographie](https://www.juergarnold.ch/kryptografie.html)



## Einstieg:


### Zeichensprache

3:27 min, WvO https://www.youtube.com/watch?v=dW3FGs8oaKg&t=8s


### Zahlen, Zählen, Zahlensysteme

	Auf wieviel kann man mit Händen und Fingern zählen?

Hilfe: [Video, 4 min](https://gitlab.com/harald.mueller/aktuelle.kurse/-/blob/master/m114/Thema_A_kodieren/x_ressourcen/000-Die-Sendung-mit-der-Maus--Bis-1023-zaehlen.mp4)
	

**Binary Count**
<br>4:42 min, YouTube, [Sendung mit der Maus - Von 0 bis 1023 zählen](https://www.youtube.com/watch?v=B_06pe8Bt90&t=29s)
<br>1:33 min, YouTube, [Binary Count](https://www.youtube.com/watch?v=zELAfmp3fXY)
<br>0:44 min, Youtube, [Binary Counter Twos Complement](https://www.youtube.com/watch?v=pYgWKKvsdOk)
 	 

**Binärsystem - Dualsystem - ganz einfach erklärt**
<br>13:20 min, D, 2018, YouTube [Dualsystem - ganz einfach erklärt](https://www.youtube.com/watch?v=I3-cmqbVF0Y)


Und noch etwas **zum Ausprobieren**
<br>So ähnlich sah bei einer **Tankestelle** früher die **Anzeige** aus.
(Die Anzahl der Zahlen auf der **Rolle** kann gewählt werden)
<br>[Tankstellenanzeige.zip](./Thema_A_kodieren/x_ressourcen/Tankstellenanzeige.zip)



