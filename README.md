# Aktuelle Kurse 
Harald Müller, 079 700 14 14, harald.mueller@tbz.ch

## Notenskala

[**AllgNotenberechnung**.xlsx](x-ressources/AllgNotenberechnung.xlsx)

[**BenotungAufgrundKompetenzmatrix**.xlsx](x-ressources/BenotungAufgrundKompetenzmatrix.xlsx)

<br>


![schulnoten-der-schweiz-wikipedia.jpg](x-ressources/schulnoten-der-schweiz-wikipedia.jpg)

[Wikipedia Schulnoten Schweiz](https://de.wikipedia.org/wiki/Schulnote)

<br>
<br>
(Volksschule)

| Note | Attribut | Erklärung |
|------|----------|-----------|
| 6.0  | sehr gut | Alle Lernziele sind erreicht, erweiterte Anforderungen werden klar übertroffen. Es wurde sehr selbständig gearbeitet. Zusätzlich werden regelmässig ausserordentliche Leistungen erbracht. |
| 5.5  |          | Alle Lernziele sind erreicht, erweiterte Anforderungen werden übertroffen. Es wurde selbständig gearbeitet. |
| 5.0  | gut      | Alle Lernziele sind erreicht, die erweiterten Anforderungen werden erfüllt. Es wurde weitgehend selbständig gearbeitet. |
| 4.5  |          | Alle Lernziele sind weitgehend erreicht, die erweiterten Anforderungen werden zum Teil erfüllt. Es wurde teilweise selbständig gearbeitet. |
| 4.0  | genügend | Die wesentlichen Lernziele sind erreicht, die Grundanforderungen werden erfüllt. Es ist häufig Unterstützung nötig. |
| 3.5  |          | Einige wesentliche Lernziele sind erreicht, die Grundanforderungen werden zum Teil erfüllt. Es ist intensive Unterstützung nötig. |
| 3.0  | ungenügend | Die wesentlichen Lernziele sind nur teilweise erreicht. Trotz Unterstützung werden die Grundanforderungen nicht erfüllt. |
| 2.0  | schwach  | Die wesentlichen Lernziele sind nicht oder nur zu einem sehr geringen Teil erreicht, die Grundanforderungen werden nicht erfüllt. |
| 1.0  | sehr schwach<br>nichtabgabe | Die wesentlichen Lernziele sind in keinem Teil erreicht. Die Grundanforderungen werden klar nicht erfüllt. |


gemäss, adaptiert aus  [Notenskala Schweiz](https://notenberechner.ch/notenskala-schweiz/)

<br>
<br>
<hr>
<br>
<br>
<br>
![notenscala-berufsbildung.ch.jpg](x-ressources/notenscala-berufsbildung.ch.jpg)
<br>siehe 
[https://www.berufsbildung.ch "Das Portal zur Berufsbildung"](https://www.berufsbildung.ch/dyn/1836.aspx)
<br>