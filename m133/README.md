# M133 Web-Applikation mit Session-Handling realisieren

[> **Modulidentifikation** ](https://www.modulbaukasten.ch/module/133/3/de-DE?title=Web-Applikation-mit-Session-Handling-realisieren)

- 1. Vorgabe analysieren, Funktionalität entwerfen und Realisierungskonzept festlegen.
- 2. Spezifische Funktionalität einer Web-Applikation mit Session-Handling, Authentifizierung und Formularüberprüfungen realisieren.
- 3. Web-Applikation mit einer Programmiersprache unter Berücksichtigung sicherheitsrelevanter Anforderungen programmieren.
- 4. Web-Applikation gemäss Testplan auf Funktionalität und Sicherheit überprüfen, Testergebnisse festhalten und allenfalls erforderliche Korrekturen vornehmen.

<hr>

## Leistungsbeurteilung

- LB1 (35%) Theorie- und Praxisprüfung (Ecolm 30 min + 60 min Programmieren)
<br>(am Tag 5) 

- LB2 (65%) Projektabgabe
<br>[M133_LB2_Projekt_Vorgabe_Bewertung](./1_Beurteilungskriterien-und-Projektauftrag/M133_LB2_Projekt_Vorgabe_Bewertung_V21.pdf)


## Ablaufplan 2024-Q4

### Klasse <mark>BI21a</mark> am Dienstag

Morgenmodul [--> M239](https://gitlab.com/harald.mueller/aktuelle.kurse/-/blob/master/m239/README.md#klasse-bi21a-am-dienstag)

|Tag |Datum|Thema, Auftrag, Übung |
|----|-----|--------------------- |
|  1 | Di 14.05.24 | Einführung & Installationen, Input PHP & HTML <br>Beginn Projekt |
|  2 | Di 21.05.24 | Input Formulare und Werteübermittlung <br> Weiterarbeit am Projekt |
|  3 | Di 28.05.24 | Input nach Bedarf <br> Weiterarbeit am Projekt  |
|  4 | Di 04.06.24 | Weiterarbeit am Projekt  |
|  5 | Di 11.06.24 | Theorie- und Praxisprüfung 15:10-16:30 |
|  6 | Di 18.06.24 | Weiterarbeit am Projekt  |
|  7 | Di 25.06.24 | Weiterarbeit am Projekt  |
|  8 | Di 02.07.24 | Weiterarbeit am Projekt  |
|  9 | Di 09.07.24 | Projektabgaben ab 09:00 |



## Aufträge

- Tag 1: 
[Auftrag_Tag1_M133_ApplikationsentwicklerInnen.pdf](./1_Beurteilungskriterien-und-Projektauftrag/Auftrag_Tag1_M133_ApplikationsentwicklerInnen.pdf)


## Clone

```
git clone https://gitlab.com/harald.mueller/m133demo.git

```