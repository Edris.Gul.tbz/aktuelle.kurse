<div id="footer">
	<table align="center" class="footer">
		<tr>
			<th colspan="2">Öffnungszeiten</th>
			<th class="middle">Youngster Library</th>
		</tr>
		<tr>
			<td>Montag</td>
			<td>geschlossen</td>
			<td class="middle">Tel. 041 835 21 21</td>
		</tr>
		<tr>
			<td>Dienstag-Freitag</td>
			<td>08:00-18:00</td>
			<td class="middle">Ausstellungsstrasse 60</td>
		</tr>
		<tr>
			<td>Samstag-Sonntag</td>
			<td>10:00-17:00</td>
			<td class="middle">8005 Zürich</td>
			<td class="madeby">Made by Maxwell International</td>
		</tr>
		<tr>
			<th colspan="4" class="middle"><a href="lageplan.php" class="footer">Lageplan</a></th>
		</tr>
	</table>
</div>