<?php
	/*Program: Home site from Youngster Library */
	header('Content-Type: text/html;charset=utf-8;');
	include 'general/session_start.php';
?>

<!DOCTYPE HTML>

<html>
	<head>
		<?php include 'general/head.php'; ?>
		<title>Youngster Library - Home</title>

	</head>

	<body>
		<div id="content">
			<?php include 'general/header.php'; ?>
			
			<div id="main">
				<h1>Aktuelle Bücher</h1>
				<div id="wrap">
					<div id="showcase" class="noselect"> 
						<?php 
							
							define ( 'MYSQL_HOST', 'localhost:3306' );
							define ( 'MYSQL_BENUTZER', 'root' );
							define ( 'MYSQL_KENNWORT', '1234' );
							define ( 'MYSQL_DATENBANK', 'youngster_library' );
							
							$db_link = @mysqli_connect (
														MYSQL_HOST,
														MYSQL_BENUTZER,
														MYSQL_KENNWORT,
														MYSQL_DATENBANK);
							 
							if ( ! $db_link ){
								echo 'keine Verbindung zur Zeit mÃ¶glich - spÃ¤ter probieren ';
							}
							
							mysqli_set_charset($db_link, 'utf8');
							
							$sql = " select * 
									from buch 
									order by buch_id desc 
									limit 6
									";
							
							$db_erg = mysqli_query( $db_link, $sql );
							
							$i = 0;
							
							while($daten = mysqli_fetch_array( $db_erg, MYSQL_ASSOC)){
								$bild[$i] = $daten['bild'];
								$id[$i] = $daten['buch_id'];
								$i++;
							}
							
							
							
						
						for($i=0;$i<6;$i++){
							echo '<a href="buch_details.php?bild=' . $id[$i] . '"><img class="cloud9-item" src="' . $bild[$i] . '" alt="New ' . $i . '"></a>';
						}
						?>
					</div>
					<p id="item-title">&nbsp;</p>
					<div class="nav" class="noselect">
						<button class="left">
							<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
						</button>
						<button class="right">
							<span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
						</button>
					</div>
				</div>
				<script src="js/jquery.reflection.js"></script> 
				<script src="js/jquery.cloud9carousel.js"></script>
				<script>
					$(function() {
						var showcase = $("#showcase")

						showcase.Cloud9Carousel( {
							yPos: 42,
							yRadius: 48,
							mirrorOptions: {
								gap: 12,
								height: 0.2
							},
							buttonLeft: $(".nav > .left"),
							buttonRight: $(".nav > .right"),
							autoPlay: true,
							bringToFront: true,
							onRendered: showcaseUpdated,
							onLoaded: function() {
								showcase.css( 'visibility', 'visible' )
								showcase.css( 'display', 'none' )
								showcase.fadeIn( 1500 )
							}
						} )

						function showcaseUpdated( showcase ) {
								var title = $('#item-title').html(
								$(showcase.nearestItem()).attr( 'alt' )
							)

							var c = Math.cos((showcase.floatIndex() % 1) * 2 * Math.PI)
								title.css('opacity', 0.5 + (0.5 * c))
						}

						// Simulate physical button click effect
						$('.nav > button').click( function( e ) {
							var b = $(e.target).addClass( 'down' )
							setTimeout( function() { b.removeClass( 'down' ) }, 80 )
						} )

						$(document).keydown( function( e ) {
							switch( e.keyCode ) {
								/* left arrow */
								case 37:
								$('.nav > .left').click()
								break

								/* right arrow */
								case 39:
								$('.nav > .right').click()
							}
						} )
					})
				</script> 
				<script type="text/javascript">

				var _gaq = _gaq || [];
				_gaq.push(['_setAccount', 'UA-36251023-1']);
				_gaq.push(['_setDomainName', 'jqueryscript.net']);
				_gaq.push(['_trackPageview']);

				(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
				})();

				</script>
			</div>
		<?php include 'general/footer.php'; ?>
		</div>
	</body>
</html>