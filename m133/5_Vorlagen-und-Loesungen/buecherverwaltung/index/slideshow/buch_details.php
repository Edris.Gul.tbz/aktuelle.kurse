<?php
	/*Program: Book Details site from Youngster Library */
	header('Content-Type: text/html;charset=utf-8;');
	include 'general/session_start.php';
?>

<!DOCTYPE HTML>

<html>
<head>
	<?php include 'general/head.php'; ?>
	<title>Youngster Library - Buch Details</title>
</head>

<body>
	<div id="content">
		<?php include 'general/header.php'; ?>
		
		<div id="main">
			<?php
			/*--------------------- WICHTIG ---------------------------*/
				if (isset($_POST['bearbeiten']) || isset($_GET['bild'])) {
					define ( 'MYSQL_HOST', 'localhost:3306' );
					define ( 'MYSQL_BENUTZER', 'root' );
					define ( 'MYSQL_KENNWORT', '1234' );
					define ( 'MYSQL_DATENBANK', 'youngster_library' );
					
					$db_link = @mysqli_connect (
												MYSQL_HOST,
												MYSQL_BENUTZER,
												MYSQL_KENNWORT,
												MYSQL_DATENBANK);
					 
					if ( ! $db_link ){
						echo 'keine Verbindung zur Zeit möglich - später probieren ';
					}
					
					mysqli_set_charset($db_link, 'utf8');
					
					/*--------------------- WICHTIG ---------------------------*/
					
					if(isset($_POST['bearbeiten'])){
						$id = key($_POST['bearbeiten']);
					}else if(isset($_GET['bild'])){
						$id = $_GET['bild'];
					}
					
					$sql = "
								SELECT *
								FROM buch
								WHERE buch_id = '$id'
							";
					
					$db_erg = mysqli_query( $db_link, $sql );
					
					while ($daten = mysqli_fetch_array( $db_erg, MYSQL_ASSOC)){
						if($daten['reihe'] == NULL){
							$reihe = "Keine Reihe";
						}else{
							$reihe = $daten['reihe'];
						}
						
						
						echo '<table class="details">';
							echo '<tr>';
								echo '<th rowspan="7"><label for="bildbutton[' . $daten['buch_id'] . ']"><img src="' . $daten['bild'] . '" alt="' . $daten['titel'] . '" class="bookDetails"/></label></th>';
								echo '<th colspan="2" class="bezeichnung"><h1>' . $daten['titel'] . '</h1></th>';
							echo '</tr>';
							echo '<tr>';
								echo '<td class="bezeichnung">Verfasser:</td>';
								echo '<td>' . $daten['verfasser'] . '</td>';
							echo '</tr>';
							echo '<tr>';
								echo '<td class="bezeichnung">Jahr:</td>';
								echo '<td>' . $daten['jahr'] . '</td>';
							echo '</tr>';
							echo '<tr>';
								echo '<td class="bezeichnung">Verlag:</td>';
								echo '<td>' . $daten['verlag'] . '</td>';
							echo '</tr>';
							echo '<tr>';
								echo '<td class="bezeichnung">Reihe:</td>';
								echo '<td>' . $reihe . '</td>';
							echo '</tr>';
							echo '<tr>';
								echo '<td class="bezeichnung">Interessenkreis:</td>';
								echo '<td>';
								$sql = "
											SELECT name
											FROM interessenkreis
											WHERE interessenkreis_id = " . $daten['interessenkreis_idfs'] . "
										";
								
								$db_erg = mysqli_query( $db_link, $sql );
								
								$interessenkreis = mysqli_fetch_array( $db_erg, MYSQL_ASSOC);
								echo $interessenkreis['name'];
								echo '</td>';
							echo '</tr>';
							echo '<tr>';
								echo '<td class="fill">Verfügbar:</td>';
								echo '<td>';
								if ($daten['verfuegbar'] == 'nein') {
									echo '<p class="verfNein">Nein</p>';
								}
								else {
									echo '<p class="verfJa">Ja</p>';
								}
								echo '</td>';
							echo '</tr>';
						echo '</table>';
						echo '<br><br>';
						?>
							<div id="beschreibung">
								<h1>Beschreibung</h1>
								<p><?php echo $daten['beschreibung']; ?></p>
							</div>
						<?php					
					}
				}
				
				else {
					echo '<p class="pflicht">Es ist kein Buch ausgewählt! Wählen Sie eines unter <a href="buecher.php" class="link">Bücher</a> aus.<p>';
				}
			?>
		</div>
	</div>
	
	<?php include 'general/footer.php'; ?>
</body>
</html>