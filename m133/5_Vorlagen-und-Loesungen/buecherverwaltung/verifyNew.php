<?php
	/*Program: verifies the input for a new Login */
	include 'general/session_start.php';
?>

<!DOCTYPE HTML>

<html>
<head>
	<?php include 'general/head.php'; ?>
	<title>Youngster Library - Registrieren...</title>
</head>

<body>
	<div id="content">
		<?php
			include 'general/header.php';
			if (isset($_POST['backlink'])){
				$backlink = $_POST['backlink'];
			}
			else if (isset($_GET['backlink'])) {
				$backlink = $_GET['backlink'];
			}
		?>
		
		<div id="main">
			<?php 
				$conn = mysqli_connect("localhost:3306", "root", "", "youngster_library") OR
				die("Error " . mysqli_error($conn));
				mysqli_set_charset($conn, 'utf8');
				
				$vorname = strip_tags($_POST["vorname"]);
				$nachname = strip_tags($_POST["nachname"]);
				$gebdat = strip_tags($_POST["gebdat"]);
				$tel = strip_tags($_POST["tel"]);
				$user = strip_tags($_POST["newUser"]); 
				$passwort = strip_tags($_POST["newPassword"]); 
				$passwort2 = strip_tags($_POST["newPassword2"]); 

				if($vorname == '' OR $nachname == '' OR $tel == '' OR strlen($tel) < 10 OR $user == '' OR !preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $user) OR $passwort == '' OR $passwort2 == '' OR $passwort != $passwort2) { 
					$errors = array();
					
					if ($vorname == '') {
						array_push($errors, 'Vorname fehlt');
					}
					
					if ($nachname == '') {
						array_push($errors, 'Nachname fehlt');
					}
					
					if ($tel == '') {
						array_push($errors, 'Telefonnummer fehlt');
					}
					else if (strlen($tel) < 10) {
						array_push($errors, 'Telefonnummer ist zu kurz');
					}
					
					if ($user == '') {
						array_push($errors, 'E-Mail fehlt');
					}
					else if (!preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $user)) {
						array_push($errors, 'E-Mail ist fehlerhaft');
					}
					
					if ($passwort == '' OR $passwort2 == '') {
						array_push($errors, 'Passwort fehlt');
					}
					else if (strlen($passwort) < 6 OR strlen($passwort2) < 6) {
						array_push($errors, 'Passwort erfordert min. 6 Zeichen');
					}
					
					if($passwort != $passwort2) {
						array_push($errors, 'Passwörter stimmen nicht überein');
					}
					
					?>
					
					<h1 class="login">Registrieren</h1>
			
					<div class="loginfield">
						<p class="pflicht"><?php echo implode(', ', $errors); ?></p>
						<form action="verifyNew.php" method="POST">
							<h2>Allgemein</h2>
							<input type="text" name="vorname" placeholder="Vorname *" value="<?php echo $vorname; ?>" class="login" autocomplete="off" required autofocus/>
							<input type="text" name="nachname" placeholder="Nachname *" value="<?php echo $nachname; ?>" class="login" autocomplete="off" required/>
							<input type="date" name="gebdat" placeholder="Geburtsdatum *" value="<?php echo $gebdat; ?>" class="login"/>
							<input type="text" name="tel" placeholder="Telefonnummer *" value="<?php echo $tel; ?>" class="login" autocomplete="off" required/>
							<h2>Zugangsdaten</h2>
							<input type="text" name="newUser" placeholder="E-Mail *" value="<?php echo $user; ?>" class="login" autocomplete="off" required/>
							<input type="password" name="newPassword" placeholder="Passwort *" class="login" required/>
							<input type="password" name="newPassword2" placeholder="Passwort wiederholen *" class="login" required/>
							<p class="pflicht">*) Pflichtfelder</p>
							<?php
								if (isset($backlink)){
									echo '<input type="hidden" name="backlink" value="'.$backlink.'"/>';
								}
							?>
							<input type="submit" value="REGISTRIEREN" class="login"/>
						</form>
					</div>
					
					<?php
				} 
				
				else {
					$passwort = md5($passwort); 

					$result = mysqli_query($conn, "SELECT konto_id FROM konto WHERE email LIKE '$user'"); 
					$menge = mysqli_num_rows($result); 

					if($menge == 0) 
						{ 
						if ($gebdat == '') {
							$eintrag = "INSERT INTO konto (name, vorname, email, tel_nummer, passwort) VALUES ('$nachname', '$vorname', '$user', '$tel', '$passwort')"; 
						}
						else {
							$eintrag = "INSERT INTO konto (name, vorname, geb_datum, email, tel_nummer, passwort) VALUES ('$nachname', '$vorname', '$gebdat', '$user', '$tel', '$passwort')"; 
						}
						
						$eintragen = mysqli_query($conn, $eintrag); 

						if($eintragen == true) 
						{ 
							$_SESSION["user"] = $user; 
							$_SESSION["timestamp"] = time();
							mysqli_query($conn, "UPDATE konto SET angemeldet = '1' WHERE email = '$user'");
							
							if (isset($backlink)){
								header("Location: $backlink");
							}
							else {
								header("Location: index.php");
							}	
						} 
						else 
						{ 
							echo 'Fehler beim Daten schreiben. Bitte benachrichtigen Sie den Webadministrator.';
						} 
					} 

					else 
					{ 
						?>
							<h1 class="login">Registrieren</h1>
							
							<div class="loginfield">
								<p class="pflicht">Es existiert bereits ein Login mit dieser E-Mail-Adresse</p>
								<form action="verifyNew.php" method="POST">
									<h2>Allgemein</h2>
									<input type="text" name="vorname" placeholder="Vorname *" value="<?php echo $vorname; ?>" class="login" autocomplete="off" required autofocus/>
									<input type="text" name="nachname" placeholder="Nachname *" value="<?php echo $nachname; ?>" class="login" autocomplete="off" required/>
									<input type="date" name="gebdat" placeholder="Geburtsdatum *" value="<?php echo $gebdat; ?>" class="login"/>
									<input type="text" name="tel" placeholder="Telefonnummer *" value="<?php echo $tel; ?>" class="login" autocomplete="off" required/>
									<h2>Zugangsdaten</h2>
									<input type="text" name="newUser" placeholder="E-Mail *" value="<?php echo $user; ?>" class="login" autocomplete="off" required/>
									<input type="password" name="newPassword" placeholder="Passwort *" class="login" required/>
									<input type="password" name="newPassword2" placeholder="Passwort wiederholen *" class="login" required/>
									<p class="pflicht">*) Pflichtfelder</p>
									<?php
										if (isset($backlink)){
											echo '<input type="hidden" name="backlink" value="'.$backlink.'"/>';
										}
									?>
									<input type="submit" value="REGISTRIEREN" class="login"/>
								</form>
							</div>
						<?php
					}
				}
			?>
		</div>
	</div>
	
	<?php include 'general/footer.php'; ?>
</body>
</html>