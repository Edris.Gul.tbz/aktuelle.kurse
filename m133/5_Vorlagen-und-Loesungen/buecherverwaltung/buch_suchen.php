<?php

	header('Content.Type: text/html; charset=utf-8');
	include 'general/session_start.php';
?>
<!DOCTYPE HTML>
<html>
	<head>
		<?php include 'general/head.php';?>
		<title>Youngster Library - Bücher suchen</title>
	</head>
	<body>
		<div id="content">
			<?php include 'general/header.php';?>
			<div id="mainnav">
				<?php
					//Muss noch eingefügt werden
				?>
			</div>
			<div id="main">
				<h1>Buch suchen</h1>
				<?php
					$stichwort = "";
					$titel = "";
					$titelkurz = "";
					$verfasser = "";
					$verlag = "";
					$isbn = "";
					$jahrvon = "";
					$jahrbis = "";
					$mediengruppe = "";
					$interessenkreis = "";
					$verfuegbar = false;
					
					define ( 'MYSQL_HOST', 'localhost:3306' );
					define ( 'MYSQL_BENUTZER', 'root' );
					define ( 'MYSQL_KENNWORT', '' );
					define ( 'MYSQL_DATENBANK', 'youngster_library' );
					
					$db_link = @mysqli_connect (
												MYSQL_HOST,
												MYSQL_BENUTZER,
												MYSQL_KENNWORT,
												MYSQL_DATENBANK);
					 
					if ( ! $db_link ){
						echo 'keine Verbindung zur Zeit möglich - später probieren ';
					}
		
					if (isset($_SESSION['user'])){
						if ($_SESSION['user'] == 'admin') {
							$bookDetails = 'buch_bearbeiten.php';
						}
						else {
							$bookDetails = 'buch_details.php';
						}
					}
					else {
						$bookDetails = 'buch_details.php';
					}
					
					mysqli_set_charset($db_link, 'utf8');
					
					//
					//SQL zum Resultate anzeigen erstellen
					//
					
					if(isset($_POST['anzeigen'])){
						$sql = "
									SELECT * 
									FROM buch 
									WHERE ";
						
						if($_POST['stichwort'] != ""){
							$stichwort = $_POST['stichwort'];
							
							$sql .= "
										beschreibung LIKE \"%" . $_POST['stichwort'] . "%\" 
										AND ";
						}
						
						if($_POST['titel'] != ""){
							$titel = $_POST['titel'];
							
							$sql .= "
										titel LIKE \"%" . $_POST['titel'] . "%\" 
										AND ";
						}
						
						if($_POST['verfasser'] != ""){
							$verfasser = $_POST['verfasser'];
							
							$sql .= "
										verfasser LIKE \"%" . $_POST['verfasser'] . "%\" 
										AND ";
						}
						
						if($_POST['verlag'] != ""){
							$verlag = $_POST['verlag'];
							
							$sql .= "
										verlag LIKE \"%" . $_POST['verlag'] . "%\" 
										AND ";
						}
						
						if($_POST['isbn'] != ""){
							$isbn = $_POST['isbn'];
							
							$sql .= "
										isbn LIKE \"%" . $_POST['isbn'] . "%\" 
										AND ";
						}
						
						if($_POST['jahrvon'] != "" || $_POST['jahrbis'] != ""){
							$jahrvon = $_POST['jahrvon'];
							$jahrbis = $_POST['jahrbis'];
							
							if($_POST['jahrvon'] != ""){
								$von = $_POST['jahrvon'];
							}else{
								$von = 0;
							}
							
							if($_POST['jahrbis'] != ""){
								$bis = $_POST['jahrbis'];
							}else{
								$bis = 1000000;
							}
							
							$sql .= "
										jahr BETWEEN " . $von . " AND " . $bis . " 
										AND ";
						}
						
						$mediengruppe = $_POST['mediengruppe'];
						if($_POST['mediengruppe'] != "Alle"){
							
							$sql .= "
										mediengruppe LIKE \"%" . $_POST['mediengruppe'] . "%\" 
										AND ";
						}
						
						$interessenkreis = $_POST['interessenkreis'];
						if($_POST['interessenkreis'] != "alle"){
							
							$sql .= "
										interessenkreis_idfs = " . $_POST['interessenkreis'] . " 
										AND ";
						}
						
						if(isset($_POST['verfuegbar'])){
							$verfuegbar = true;
							$sql .= "
										verfuegbar = 1 
										AND ";
						}else{
							$verfuegbar = false;
						}
						
						$sql .= "
									buch_id > 0 
									ORDER BY jahr 
								";
						
						$db_erg = mysqli_query( $db_link, $sql );
					}
					
					//
					//Sucheingabe
					//
					echo '<form name="anzeigen" action="' . $_SERVER['PHP_SELF'] . '" method="POST" accept-charset="utf-8" enctype="multipart/form-data">';
						echo '<fieldset>';
							echo '<table>';
								echo '<tr>';
									echo '<th colspan="4">Sucheingabe</th>';
								echo '</tr>';
								echo '<tr>';
									echo '<td colspan="2">Stichwort:</td>';
									echo '<td colspan="2"><input autocomplete="off" type="text" name="stichwort" value="' . $stichwort . '"/></td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td colspan="2">Titel:</td>';
									echo '<td colspan="2"><input autocomplete="off" type="text" name="titel" value="' . $titel . '"/></td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td colspan="2">Verfasser:</td>';
									echo '<td colspan="2"><input autocomplete="off" type="text" name="verfasser" value="' . $verfasser . '"/></td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td colspan="2">Verlag:</td>';
									echo '<td colspan="2"><input autocomplete="off" type="text" name="verlag" value="' . $verlag . '"/></td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td colspan="2">ISBN:</td>';
									echo '<td colspan="2"><input autocomplete="off" type="text" name="isbn" value="' . $isbn . '"/></td>';
								echo '</tr>';
								echo '<tr>';
									echo '<th colspan="4">Weitere Suchkriterien</th>';	//Weitere Suchkriterien
								echo '</tr>';
								echo '<tr>';
									echo '<td colspan="2">Jahr von ... bis:</td>';
									echo '<td><input class="jahr" autocomplete="off" type="number" name="jahrvon" value="' . $jahrvon . '"/></td>';
									echo '<td><input class="jahr" autocomplete="off" type="number" name="jahrbis" value="' . $jahrbis . '"/></td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td colspan="2">Mediengruppe:</td>';
									echo '<td colspan="2"><select name="mediengruppe" class="editDropdown">';
														$sql = "
																	SELECT DISTINCT mediengruppe
																	FROM buch
																	ORDER BY mediengruppe
																";
														
														$db_erg2 = mysqli_query( $db_link, $sql );
														
														while ($daten = mysqli_fetch_array( $db_erg2, MYSQL_ASSOC)){
															echo '<option ';
															if($mediengruppe == $daten['mediengruppe']){
																echo 'selected ';
															}
															echo 'value="' . $daten['mediengruppe'] . '">' . $daten['mediengruppe'] . ' ';
														}
													echo '</select>';
								echo '</tr>';
								echo '<tr>';
									echo '<td colspan="2">Interessenkreis:</td>';
									echo '<td colspan="2"><select name="interessenkreis" class="editDropdown">';
														$sql = "
																	SELECT *
																	FROM interessenkreis
																	ORDER BY name
																";
														
														$db_erg2 = mysqli_query( $db_link, $sql );
														
														echo '<option selected value="alle">Alle ';
														
														while ($daten = mysqli_fetch_array( $db_erg2, MYSQL_ASSOC)){
															echo '<option ';
															if($interessenkreis == $daten['interessenkreis_id']){
																echo 'selected ';
															}
															echo 'value="' . $daten['interessenkreis_id'] . '">' . $daten['name'] . ' ';
														}
													echo '</select>';
								echo '</tr>';
								echo '<tr>';
									echo '<td colspan="2">Nur verfügbare:</td>';
									echo '<td colspan="2"><input autocomplete="off" type="checkbox" name="verfuegbar" ';
									if($verfuegbar){
										echo 'checked';
									}
									echo '/></td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td><input type="submit" name="anzeigen" value="Suchen" class="filterbutton"/></td>';
									echo '<td><input type="reset" class="filterbutton"/></td>';
								echo '</tr>';
							echo '</table>';
						echo '</fieldset>';
					echo '</form><br/>';
					
					//
					//Resultate anzeigen
					//
					
					if(isset($_POST['anzeigen'])){
						while ($daten = mysqli_fetch_array( $db_erg, MYSQL_ASSOC)){
							
							$sql = "
										SELECT COUNT(*) as positiv 
										FROM bewertung 
										WHERE buch_idfs = " . $daten['buch_id'] . " 
										AND bewertung = 'Positiv' 
									";
							
							$db_ergpos = mysqli_query( $db_link, $sql );
							$positiv = mysqli_fetch_array($db_ergpos, MYSQL_ASSOC);
							
							$sql = "
										SELECT COUNT(*) as negativ 
										FROM bewertung 
										WHERE buch_idfs = " . $daten['buch_id'] . " 
										AND bewertung = 'Negativ' 
									";
							
							$db_ergneg = mysqli_query( $db_link, $sql );
							$negativ = mysqli_fetch_array($db_ergneg, MYSQL_ASSOC);
							
							if($daten['reihe'] == NULL){
								$reihe = "Keine Reihe";
							}else{
								$reihe = $daten['reihe'];
							}
				
							if(strlen($daten['titel']) > 40){
								$kurz = substr($daten['titel'], 0, 40);
								$kurz = trim($kurz);
								if(strlen($kurz) - strrpos($kurz, ' ') < 3){
									$titelkurz = substr($kurz, 0, strrpos($kurz, ' '));
								}else{
									$titelkurz = substr($kurz, 0, strrpos($kurz, ' '));
								}
								$titelkurz .= "...";
							}else{
								$titelkurz = $daten['titel'];
							}
							
							echo '<form name="bearbeiten" action="' . $bookDetails . '" method="POST" accept-charset="utf-8" enctype="multipart/form-data">';
								echo '<table>';
									echo '<tr>';
										echo '<th rowspan="5" colspan="4"><label for="bildbutton[' . $daten['buch_id'] . ']"><img src="' . $daten['bild'] . '" alt="' . $daten['titel'] . '" class="listEditBooks"/></label></th>';
										echo '<th colspan="2"><input type="submit" id="bildbutton[' . $daten['buch_id'] . ']" name="bearbeiten[' . $daten['buch_id'] . ']" value="' . $titelkurz . '" class="bildbutton"></th>';
									echo '</tr>';
									echo '<tr>';
										echo '<td class="bezeichnung">Verfasser:</td>';
										echo '<td>' . $daten['verfasser'] . '</td>';
									echo '</tr>';
									echo '<tr>';
										echo '<td class="bezeichnung">Jahr:</td>';
										echo '<td>' . $daten['jahr'] . '</td>';
									echo '</tr>';
									echo '<tr>';
										echo '<td class="bezeichnung">Verlag:</td>';
										echo '<td>' . $daten['verlag'] . '</td>';
									echo '</tr>';
									echo '<tr>';
										echo '<td class="bezeichnung">Reihe:</td>';
										echo '<td>' . $reihe . '</td>';
									echo '</tr>';
									echo '<tr>';
										echo '<td><img class="daumen" src="images/up.png" alt="Daumen rauf"/></td>';
										echo '<td>' . $positiv['positiv'] . '</td>';
										echo '<td><img class="daumen" src="images/down.png" alt="Daumen runter"/></td>';
										echo '<td>' . $negativ['negativ'] . '</td>';
										if($daten['verfuegbar'] == "ja"){
											echo '<td class="bezeichnungrechts"><img src="images/haekchen.png" alt="Häkchen"/></td>';
											echo '<td>Verfügbar</td>';
										}else{
											echo '<td class="bezeichnungrechts"><img src="images/kreuz.png" alt="Kreuz"/></td>';
											echo '<td>Nicht verfügbar</td>';
										}
									echo '</tr>';
								echo '</table>';
							echo '</form><br><br>';
						}
					}
				?>
			</div>
			<?php include 'general/footer.php';?>
		</div>
	</body>
	<?php include 'general/scroll_up.php'; ?>
</html>