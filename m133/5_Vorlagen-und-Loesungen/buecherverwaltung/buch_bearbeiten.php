<?php

header('Content.Type: text/html; charset=utf-8');
include 'general/session_start_loggedin.php';
?>
<!DOCTYPE HTML>
<html>
	<head>
		<?php include 'general/head.php';?>
		<title>Youngster Library - Admin Buch bearbeiten</title>
	</head>
	<body>
		<div id="content">
			<?php include 'general/header.php';?>
			
			<div id="main">
				<?php
					$titelerror = "";
					$verfassererror = "";
					$jahrerror = "";
					$verlagerror = "";
					$isbnerror = "";
					$isbn2error = "";
					$beschreibungerror = "";
					$mediengruppeerror = "";
					
					$titel = "";
					$verfasser = "";
					$jahr = "";
					$verlag = "";
					$isbn = "";
					$isbn2 = "";
					$interessenkreis2 = "";
					$reihe = "";
					$beschreibung = "";
					$mediengruppe = "";
					$error = false;
					
					define ( 'MYSQL_HOST', 'localhost:3306' );
					define ( 'MYSQL_BENUTZER', 'root' );
					define ( 'MYSQL_KENNWORT', '' );
					define ( 'MYSQL_DATENBANK', 'youngster_library' );
					
					$db_link = @mysqli_connect (
												MYSQL_HOST,
												MYSQL_BENUTZER,
												MYSQL_KENNWORT,
												MYSQL_DATENBANK);
					 
					if ( ! $db_link ){
						echo 'keine Verbindung zur Zeit möglich - später probieren ';
					}
					
					mysqli_set_charset($db_link, 'utf8');
					
					//
					//Änderung vornehmen
					//
					if(isset($_POST['aendern']) && $_POST['titel'] != "" && $_POST['verfasser'] != "" && $_POST['jahr'] != "" && $_POST['verlag'] != "" && $_POST['isbn'] != "" && $_POST['beschreibung'] != "" && $_POST['mediengruppe'] != "" && preg_match("/^[\d][\d\-]{8,}[\d]$/", $_POST['isbn']) && (preg_match("/^[\d][\d\-]{8,}[\d]$/", $_POST['isbn2']) || $_POST['isbn2'] == "" || $_POST['isbn2'] == "Keine")){
						$id = key($_POST['aendern']);
						
						function eingabebereinugung(&$value, $key){
							
							if(!is_array($value)){
								$value = strip_tags($value);
								
								$value = htmlspecialchars($value, ENT_QUOTES);
								
								$value = trim($value);
							}
						}
						
						array_walk ($_POST, 'eingabebereinugung');
						array_walk ($_GET, 'eingabebereinugung');
						array_walk ($_REQUEST, 'eingabebereinugung');
						
						$null = NULL;
						
						$sql = "
									UPDATE buch 
									SET titel = '" . $_POST['titel'] . "', 
									verfasser = '" . $_POST['verfasser'] . "', 
									jahr = '" . $_POST['jahr'] . "', 
									verlag = '" . $_POST['verlag'] . "', 
									interessenkreis_idfs = '" . $_POST['interessenkreis'] . "', 
									isbn = '" . $_POST['isbn'] . "', 
									";
						if($_POST['isbn2'] != "" && $_POST['isbn2'] != "Keine"){
							$sql .= "
										isbn_2 = '" . $_POST['isbn2'] . "', 
										";
						}else{
							$sql .= "
										isbn_2 = '" . $null . "', 
										";
						}
						$sql .= "
									beschreibung = '" . $_POST['beschreibung'] . "', 
									";
						if($_POST['reihe'] != "" && $_POST['reihe'] != "Keine Reihe"){
							$sql .= "
										reihe = '" . $_POST['reihe'] . "', 
										";
						}else{
							$sql .= "
										reihe = '" . $null . "', 
										";
						}
						if($_POST['verfuegbar'] == "ja"){
							$sql .= "
										verfuegbar = 'ja', 
										";
						}else{
							$sql .= "
										verfuegbar = 'nein', 
										";
						}
						$sql .= "
									mediengruppe = '" . $_POST['mediengruppe'] . "' 
									WHERE buch_id = '" . $id . "' ";
						
						$db_erg = mysqli_query($db_link, $sql);
						
						$error = false;
						
					}else if(isset($_POST['aendern'])){
						
						if($_POST['titel'] == ""){
							$titelerror = "Geben Sie den Titel ein!";
							$titel = "";
						}else{
							$titel = $_POST['titel'];
						}
						
						if($_POST['verfasser'] == ""){
							$verfassererror = "Geben Sie den Verfasser an!";
							$verfasser = "";
						}else{
							$verfasser = $_POST['verfasser'];
						}
						
						if($_POST['jahr'] == ""){
							$jahrerror = "Geben Sie das Jahr an!";
							$jahr = "";
						}else{
							$jahr = $_POST['jahr'];
						}
						
						if($_POST['verlag'] == ""){
							$verlagerror = "Geben Sie den Verlag an!";
							$verlag = "";
						}else{
							$verlag = $_POST['verlag'];
						}
						
						if($_POST['isbn'] == ""){
							$isbnerror = "Geben Sie den ISBN an!";
							$isbn = "";
						}else if(!preg_match("/^[\d][\d\-]{8,}[\d]$/", $_POST['isbn'])){
							$isbnerror = "Geben Sie eine gültige ISBN an!";
							$isbn = "";
						}else{
							$isbn = $_POST['isbn'];
						}
						
						if(!preg_match("/^[\d][\d\-]{8,}[\d]$/", $_POST['isbn2']) && $_POST['isbn2'] != "Keine" && $_POST['isbn2'] != ""){
							$isbn2error = "Geben Sie eine gültige ISBN an!";
							$isbn2 = $_POST['isbn2'];
						}else if($_POST['isbn'] == ""){
							$isbn2 = "Keine";
						}else{
							$isbn2 = $_POST['isbn2'];
						}
						
						if($_POST['beschreibung'] == ""){
							$beschreibungerror = "Schreiben Sie eine kurze Beschreibung zum Buch!";
							$beschreibung = "";
						}else{
							$beschreibung = $_POST['beschreibung'];
						}
						
						if($_POST['mediengruppe'] == ""){
							$mediengruppeerror = "Geben Sie die Mediengruppe an!";
							$mediengruppe = "";
						}else{
							$mediengruppe = $_POST['mediengruppe'];
						}
						
						if($_POST['reihe'] == ""){
							$reihe = "Keine Reihe";
						}else{
							$reihe = $_POST['reihe'];
						}
						
						$interessenkreis2 = $_POST['interessenkreis'];
						
						$error = true;
						$id = key($_POST['aendern']);
					}
					
					if(isset($_POST['loeschen'])){
						$id = key($_POST['loeschen']);
						
						$sql = "
									DELETE FROM buch 
									WHERE buch_id = '" . $id . "' ";
						
						$db_erg = mysqli_query($db_link, $sql);
					}
					
					//
					//Formular zum ändern
					//
					if(isset($_POST['bearbeiten']) && is_array($_POST['bearbeiten']) || $error){
						
						if($error == false){
							$id = key($_POST['bearbeiten']);
						}else{
							
						}
						
						$sql = '
									SELECT *
									FROM buch
									WHERE buch_id = ' . $id . '
								';
						
						$db_erg = mysqli_query( $db_link, $sql );
						
						$daten = mysqli_fetch_array( $db_erg, MYSQL_ASSOC);
						
						$sql = '
									SELECT *
									FROM interessenkreis
									WHERE interessenkreis_id = ' . $daten['interessenkreis_idfs'] . '
								';
						
						$db_erg = mysqli_query( $db_link, $sql );
						
						$interessenkreis = mysqli_fetch_array( $db_erg, MYSQL_ASSOC);
						
						if($daten['reihe'] == NULL){
							$reihe = "Keine Reihe";
						}else{
							$reihe = $daten['reihe'];
						}
						
						if($daten['isbn_2'] == NULL){
							$isbn2 = "Keine";
						}else{
							$isbn2 = $daten['isbn_2'];
						}
						
						?>
						
						<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">
							<table>
								<tr>
									<th class="bildtabelle" rowspan="11"><img src="<?php echo $daten['bild']; ?>" alt="<?php echo $daten['titel'] ?>" class="editDetails"/></th>
									<th class="titel" colspan="2"><h1><?php echo $daten['titel']; ?></h1></th>
								</tr>
								<tr>
									<td>Titel:</td>
									<td><input autocomplete="off" type="text" name="titel" value="<?php if($titel == ""){ echo $daten['titel'];}else{ echo $titel;}?>"/></td>
									<td class="error"><?php echo $titelerror; ?></td>
								</tr>
								<tr>
									<td>Reihe:</td>
									<td><input autocomplete="off" type="text" name="reihe" value="<?php if($reihe == ""){ if($daten['reihe'] != NULL){ echo $daten['reihe'];}else{ echo 'Keine Reihe';}}else{ echo $reihe;}?>"/></td>
								</tr>
								<tr>
									<td>Verfasser:</td>
									<td><input autocomplete="off" type="text" name="verfasser" value="<?php if($verfasser == ""){ echo $daten['verfasser'];}else{ echo $verfasser;}?>"/></td>
									<td class="error"><?php echo $verfassererror; ?></td>
								</tr>
								<tr>
									<td>Interessenkreis:</td>
									<td>
										<select name="interessenkreis" class="editDropdown">
											<?php
												$sql = "
															SELECT *
															FROM interessenkreis
															ORDER BY name
														";
												
												$db_erg = mysqli_query( $db_link, $sql );
												
												while ($interessenkreis = mysqli_fetch_array( $db_erg, MYSQL_ASSOC)){
													echo '<option ';
													if($interessenkreis2 == ""){
														if($daten['interessenkreis_idfs'] == $interessenkreis['interessenkreis_id']){
															echo 'selected ';
														}
													}else if($interessenkreis2 == $interessenkreis['interessenkreis_id']){
														echo 'selected';
													}
													echo 'value="' . $interessenkreis['interessenkreis_id'] . '">' . $interessenkreis['name'] . ' ';
												}
											?>
										</select>
									</td>
								</tr>
								<tr>
									<td>Mediengruppe:</td>
									<td><input autocomplete="off" type="text" name="mediengruppe" value="<?php if($mediengruppe == ""){ echo $daten['mediengruppe'];}else{ echo $mediengruppe;}?>"/></td>
									<td class="error"><?php echo $mediengruppeerror; ?></td>
								</tr>
								<tr>
									<td>Jahr:</td>
									<td><input autocomplete="off" type="text" name="jahr" value="<?php if($jahr == ""){ echo $daten['jahr'];}else{ echo $jahr;}?>"/></td>
									<td class="error"><?php echo $jahrerror; ?></td>
								</tr>
								<tr>
									<td>Verlag:</td>
									<td><input autocomplete="off" type="text" name="verlag" value="<?php if($verlag == ""){ echo $daten['verlag'];}else{ echo $verlag;}?>"/></td>
									<td class="error"><?php echo $verlagerror; ?></td>
								</tr>
								<tr>
									<td>ISBN:</td>
									<td><input autocomplete="off" type="text" name="isbn" value="<?php if($isbn == ""){ echo $daten['isbn'];}else{ echo $isbn;}?>"/></td>
									<td class="error"><?php echo $isbnerror; ?></td>
								</tr>
								<tr>
									<td>2. ISBN:</td>
									<td><input autocomplete="off" type="text" name="isbn2" value="<?php if($isbn2 == ""){ if($daten['isbn2'] != NULL){ echo $daten['isbn2'];}else{ echo 'Keine';}}else{ echo $isbn2;}?>"/></td>
									<td class="error"><?php echo $isbn2error; ?></td>
								</tr>
								<tr>
								<?php
									if($daten['verfuegbar'] == "ja"){
										echo '<td class="bezeichnungrechts"><img src="images/haekchen.png" alt="Häkchen"/></td>';
										echo '<td><select name="verfuegbar" class="editDropdown">';
											echo '<option selected value="ja">Verfügbar';
											echo '<option value="nein">Nicht verfügbar';
										echo '</td>';
									}else{
										echo '<td class="bezeichnungrechts"><img src="images/kreuz.png" alt="Kreuz"/></td>';
										echo '<td><select name="verfuegbar" class="editDropdown">';
											echo '<option value="ja">Verfügbar';
											echo '<option selected value="nein">Nicht verfügbar';
										echo '</td>';
									}
								?>
								</tr>
							</table>
							
							<textarea name="beschreibung" class="editDetails"><?php if($beschreibung == ""){echo $daten['beschreibung'];}else {echo $beschreibung;}?></textarea>
							
							<?php echo '<input type="submit" name="aendern['.$id.']" value="Speichern" class="save">'; ?>
							<?php echo '<input type="submit" onClick="return confirm(\'Wollen dieses Buch wirklich löschen?!\')" name="loeschen['.$id.']" value="Löschen" class="save">'; ?>
						</form>
						
						<?php
					}
					
					else {
						header("Location: buecher.php");
					}
				?>
			</div>
			<?php include 'general/footer.php';?>
		</div>
	</body>
	<?php include 'general/scroll_up.php'; ?>
</html>