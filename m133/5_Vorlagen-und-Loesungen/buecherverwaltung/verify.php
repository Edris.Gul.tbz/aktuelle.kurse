<?php
	/*Program: verifies the Login data and directs to the according page */
	include 'general/session_start.php';
?>

<!DOCTYPE HTML>

<html>
<head>
	<?php include 'general/head.php'; ?>
	<title>Youngster Library - Login überprüfen...</title>
</head>

<body>
	<div id="content">
		<?php include 'general/header.php'; ?>
		
		<div id="main">
			<?php
				define ( 'MYSQL_HOST', 'localhost:3306' );
				define ( 'MYSQL_BENUTZER', 'root' );
				define ( 'MYSQL_KENNWORT', '' );
				define ( 'MYSQL_DATENBANK', 'youngster_library' );
				
				$conn = mysqli_connect (MYSQL_HOST, MYSQL_BENUTZER, MYSQL_KENNWORT, MYSQL_DATENBANK);
				if ( ! $conn )
				{
					// hier sollte dann später dem Programmierer eine
					// E-Mail mit dem Problem zukommen gelassen werden
					// die Fehlermeldung für den Programmierer sollte
					// das Problem ausgeben mit: mysql_error()
					die('keine Verbindung zur Zeit möglich - später probieren ');
				}
				
				mysqli_set_charset($conn, 'utf8');
				
				$username = strip_tags($_POST["user"]); 
				$passwort = strip_tags($_POST["password"]); 
				$passwort = md5($passwort);

				$abfrage = "SELECT email, passwort FROM konto WHERE email = '$username' LIMIT 1"; 
				$ergebnis = mysqli_query($conn, $abfrage); 
				$row = mysqli_fetch_object($ergebnis); 

				if(isset($row) AND $row->passwort == $passwort) { 
					$_SESSION["user"] = $username; 
					$_SESSION["timestamp"] = time();
					mysqli_query($conn, "UPDATE konto SET angemeldet = '1' WHERE email = '$username'");
					if (isset($_POST['backlink'])) {
						header("Location: ".$_POST['backlink']);
					}
					else {
						header("Location: index.php");
					}
				} 
				
				else { ?>
					<h1 class="login">Login</h1>
				
					<div class="loginfield">
						<p class="pflicht">E-Mail und/oder Passwort waren falsch</p>
						
						<form action="verify.php" method="POST">
							<input type="text" name="user" placeholder="E-Mail *" value="<?php if (isset($_POST['user'])){echo $_POST['user']; }?>" class="login" autocomplete="off" required autofocus/>
							<input type="password" name="password" placeholder="Passwort *" class="login" required/><br>
							<?php
								if (isset($_POST['backlink'])){
									$backlink = $_POST['backlink'];?>
									<input type="hidden" name="backlink" value="<?php echo $_POST['backlink']; ?>"/><?php
								}
							?>
							<p class="pflicht">*) Pflichtfelder</p>
							<input type="submit" value="LOGIN" class="login"/>
						</form>
					</div>
					
					<?php
				} 
			?>
			
			<div class="loginfield">
				<button onclick="location.href='registrieren.php'" class="registrieren">Jetzt Registrieren!</button>
			</div>
		</div>
	</div>
	
	<?php include 'general/footer.php'; ?>
</body>
</html>

