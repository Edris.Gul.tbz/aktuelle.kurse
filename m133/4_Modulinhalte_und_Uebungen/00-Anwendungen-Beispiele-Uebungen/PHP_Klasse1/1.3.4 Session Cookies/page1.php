Es werden 3 Variabeln wie folgt initialisiert:<br>	
-variable1 mit <b>"eins"</b><br>			
-variable2 mit <b>"zwei"</b><br>			
-variable3 mit <b>"drei"</b><br>			

<?
session_start();				 
							

$variable1 = "eins";			
$variable2 = "zwei";			
$variable3 = "drei";			

session_register("variable1");				//Hier wird die Variable1 ins Cookie geschrieben
session_register("variable2");				//Das Selbe mit variable2
session_register("variable3");				//Und noch mit variable3

?>
<br><br>
<a href="page2.php">LINK</a>