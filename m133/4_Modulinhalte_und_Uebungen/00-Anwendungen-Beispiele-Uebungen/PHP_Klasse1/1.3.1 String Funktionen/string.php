<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Stringfunktionen</title>
<style type="text/css">
<!--
body {
font-family:"Courier New", Courier, mono;
font-size:12px
}
.red {color: #FF5500;font-weight:bold;}
-->
</style>
</head>

<body>
<table width="100%" border="1" cellpadding="3" bordercolor="#FF9900">
  <tr>
    <td>
	<span class="red">string mit Vars verknuepfen</span><br>
	<?	
	//string mit Vars verkn�pfen;
	$Nachname = "Probst";
	$Vorname = "Rene";
	$fach = "Java";
	echo"Heute hatten wir eine ".$fach."-Pr�fung bei Herrn ".$Nachname.", der mit vornamen ".$Vorname." heisst. oda?";
?>
      <br>
      <br>
      <span class="red">original code:</span><br>
      <br>
      &lt;? <br>
$Nachname = &quot;Probst&quot;;<br>
$Vorname = &quot;Rene&quot;;<br>
$fach = &quot;Java&quot;;<br>
echo&quot;Heute hatten wir eine &quot;.$fach.&quot;-Pr&uuml;fung bei Herrn &quot;.$Nachname.&quot;, der mit vornamen &quot;.$Vorname.&quot;heisst. oda?&quot;;<br>
?&gt;<br></td>
  </tr>
  <tr>
    <td>
	<span class="red">string verschluesseln</span><br>
	<?	
	//string verschl�sseln;
	$Wort = "geheimwort";
	echo "Normal: ".$Wort;
	?>
      <br>
      <?
	echo "Verschl�sselt: ".md5($Wort);
?>
      <br>
      <br>
      <span class="red">original code:</span><br>
      <br>
&lt;? <br>
$Wort = &quot;geheimwort&quot;;<br>
echo &quot;Normal: &quot;.$Wort;<br>
?&gt;&lt;br&gt;&lt;?<br>
echo &quot;Verschl&uuml;sselt: &quot;.md5($Wort);<br>
?&gt; </td>
  </tr>
  <tr>
    <td>
	<span class="red">stringlaenge</span><br>
	<?	//stringlaenge;
	$String = "Probst";
	echo "Wort: ".$String."<br>";
	echo "L�nge: ".strlen($String);
?>
      <br>
      <br>
      <span class="red">original code:</span><br>
      <br>
      &lt;?<br>
$String = &quot;Probst&quot;;<br>
echo &quot;Wort: &quot;.$String.&quot;&lt;br&gt;&quot;;<br>
echo &quot;L&auml;nge: &quot;.strlen($String);<br>
?&gt; </td>
  </tr>
  <tr>
    <td>
	<span class="red">striptags</span><br>
	<?	//striptags;
	$String = "this is wayne<img src='wayne.jpg' border'0'><br>";
	echo ($String);
	echo "Striped: ".strip_tags($String);
?>
      <br>
      <br>
      <span class="red">original code:</span><br>
      <br>
&lt;?<br>
$String = &quot;this is wayne&lt;img src='wayne.jpg' border'0'&gt;&lt;br&gt;&quot;;<br>
echo ($String);<br>
echo &quot;Striped: &quot;.strip_tags($String);<br>
?&gt; </td>
  </tr>
  <tr>
    <td>
	<span class="red">suche</span><br>
      <?
	//suche nach string und R�ckgabe des Pos.
	$finde   = "see";
	$string1 = "oder oder und oder";
	$string2 = "BZZ Zuerichsee";

	$pos1 = stripos($string1, $finde);
	$pos2 = stripos($string2, $finde);
	
	if ($pos1 == false){
	echo "'$finde' wurde in '$string1' nicht gefunden.<br>";
	}
   
	if ($pos1 != false){
	echo "'$finde' wurde in '$string1' auf position '$pos1' gefunden<br>";
	}
	
	if ($pos2 == false){
	echo "'$find' wurde in '$string2' nicht gefunden.<br>";
	}
   
	if ($pos2 != false){
	echo "'$finde' wurde in '$string2' auf position '$pos2' gefunden<br>";
	}
?>
      <br><br>
      <span class="red">original code:</span><br>
      <br>
      &lt;?<br>
$finde = &quot;see&quot;;<br>
$string1 = &quot;oder oder und oder&quot;;<br>
$string2 = &quot;BZZ Zuerichsee&quot;;
<p> $pos1 = stripos($string1, $finde);<br>
  $pos2 = stripos($string2, $finde);<br>
  <br>
  if ($pos1 == false){<br>
  echo &quot;'$finde' wurde in '$string1' nicht gefunden.&lt;br&gt;&quot;;<br>
  }<br>
  <br>
  if ($pos1 != false){<br>
  echo &quot;'$finde' wurde in '$string1' auf position '$pos1' gefunden&lt;br&gt;&quot;;<br>
  }<br>
  <br>
  if ($pos2 == false){<br>
  echo &quot;'$find' wurde in '$string2' nicht gefunden.&lt;br&gt;&quot;;<br>
  }<br>
  <br>
  if ($pos2 != false){<br>
  echo &quot;'$finde' wurde in '$string2' auf position '$pos2' gefunden&lt;br&gt;&quot;;<br>
  }<br>
  ?&gt;</p></td>
  </tr>
    <tr>
    <td>
    <div class="red">Zeichen oder Strings ersetzen:</div><br />
	$string = 'EditPlus ist toll. Irgendwie mag ich EditPlus. Ich bleibe bei EditPlus<br /><br />
	<span class="red">Vorher:</span> EditPlus ist toll. Irgendwie mag ich EditPlus. Ich bleibe bei EditPlus<br /><br />
	$string=str_replace("EditPlus","Eclipse",$string)<br />
	&nbsp;&nbsp;&nbsp;&nbsp;/* Ersetzt "EditPlus" im String $string durch "Eclipse" */<br /><br />
	<span class="red">Nacher</span>: Eclipse ist toll. Irgendwie mag ich Eclipse. Ich bleibe bei Eclipse<br /><br />

	</td>
  </tr>
</table>
</body>
</html>
