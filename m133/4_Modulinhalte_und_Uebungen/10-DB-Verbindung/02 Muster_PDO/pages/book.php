<?php

// Read data from database
$query = '
SELECT
	DATE_FORMAT(b.datum, "%d.%m.%Y") as datum, sk.kontonr as sollkonto, hk.kontonr as habenkonto, b.text, b.betrag
FROM
	buchung b, konto sk, konto hk
WHERE
	sk.id=b.sollkonto AND hk.id=b.habenkonto
ORDER BY
	b.datum, b.id ASC';
$result = $dbconnection->query($query);

?>
<h1>Hauptbuch</h1>
<table cellpadding="0" cellspacing="0">
	<tr><th>Datum</th><th>Sollkonto</th><th>Habenkonto</th><th>Text</th><th align="right">Betrag</th></tr>
<?php while($row = $result->fetch()) { ?>
	<tr><td><?= $row['datum'] ?></td><td><?= $row['sollkonto'] ?></td><td><?= $row['habenkonto'] ?></td><td><?= $row['text'] ?></td><td align="right"><?= number_format($row['betrag'], 2) ?></td></tr>
<?php } ?>
</table>