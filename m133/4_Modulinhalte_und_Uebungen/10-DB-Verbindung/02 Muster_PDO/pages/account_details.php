<?php

if(!isset($_GET['konto'])) return;

// Get account information
$kontoid = $_GET['konto'];
$query = '
SELECT
	bezeichnung, kontotyp, kontonr
FROM
	konto
WHERE
	id='.$kontoid;
$result = $dbconnection->query($query);
$konto = $result->fetch();

// Dunno what's going on here
$result->fetchAll();

?>

<h1>Konto <?= $konto['kontonr'] ?>: <?= $konto['bezeichnung'] ?></h1>

<table cellpadding="0" cellspacing="0">
	<tr><th>Datum</th><th>Gegenkonto</th><th>Text</th><th>Soll</th><th>Haben</th></tr>

<?php

// Read bookings
$query = '
SELECT
	DATE_FORMAT(b.datum, "%d.%m.%Y") as datum, sk.kontonr as sollkonto, hk.kontonr as habenkonto, b.text, b.betrag
FROM
	buchung b, konto sk, konto hk
WHERE
	sk.id=b.sollkonto AND hk.id=b.habenkonto AND (sk.id='.$kontoid.' OR hk.id='.$kontoid.')
ORDER BY
	b.datum, b.id ASC';
$result1 = $dbconnection->query($query);

while($row = $result1->fetch()) { ?>
	
	<tr><td><?= $row['datum'] ?></td>
	<td><?= ($row['sollkonto'] == $konto['kontonr']) ? $row['habenkonto'] : $row['sollkonto'] ?></td>
	<td><?= $row['text'] ?></td>
	
	<?php if($row['sollkonto'] == $konto['kontonr']) { ?>
		<td align="right"><?= number_format($row['betrag'], 2) ?></td><td>&nbsp;</td></tr>
	<?php } else { 	?>
		<td align="right">&nbsp;</td><td><?= number_format($row['betrag'], 2) ?></td></tr>
	<?php } 	?>

<?php } ?>
</table>
