-- phpMyAdmin SQL Dump
-- version 2.8.0.3
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Erstellungszeit: 21. Oktober 2008 um 10:30
-- Server Version: 5.0.20
-- PHP-Version: 5.1.2
-- 
-- Datenbank: `airline`
-- 

-- --------------------------------------------------------

-- 
-- Tabellenstruktur für Tabelle `buchung`
-- 

CREATE TABLE `buchung` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `datum` date NOT NULL,
  `sollkonto` int(11) NOT NULL,
  `habenkonto` int(11) NOT NULL,
  `text` varchar(50) collate latin1_general_ci NOT NULL,
  `betrag` double NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=35 ;

-- 
-- Daten für Tabelle `buchung`
-- 

INSERT INTO `buchung` VALUES (1, '2004-01-01', 1, 3, 'Bareinlage', 1000);
INSERT INTO `buchung` VALUES (2, '2004-01-01', 5, 1, 'Otto, u/Kauf, 80 Uhren', 800);
INSERT INTO `buchung` VALUES (3, '2004-01-08', 1, 4, 'Strasse, u/Verkauf, 20 Uhren', 400);
INSERT INTO `buchung` VALUES (4, '2004-01-15', 1, 4, 'Strasse, u/Verkauf, 15 Uhren', 300);
INSERT INTO `buchung` VALUES (5, '2004-01-22', 1, 4, 'Strasse, u/Verkauf, 15 Uhren', 440);
INSERT INTO `buchung` VALUES (6, '2004-01-31', 2, 5, 'Inventur, 23 Uhren', 230);


-- --------------------------------------------------------

-- 
-- Tabellenstruktur für Tabelle `konto`
-- 

CREATE TABLE `konto` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `kontostand` double NOT NULL,
  `bezeichnung` varchar(64) collate latin1_general_ci NOT NULL,
  `kontotyp` int(11) NOT NULL,
  `kontonr` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=6 ;

-- 
-- Daten für Tabelle `konto`
-- 

INSERT INTO `konto` VALUES (1, 1340, 'Kasse', 1, 1000);
INSERT INTO `konto` VALUES (2, 230, 'Warenbestand', 1, 1050);
INSERT INTO `konto` VALUES (3, 1000, 'Eigenkapital', 2, 2100);
INSERT INTO `konto` VALUES (4, 1140, 'Warenertrag', 3, 3000);
INSERT INTO `konto` VALUES (5, 1030, 'Warenaufwand', 4, 4000);

-- --------------------------------------------------------

-- 
-- Tabellenstruktur für Tabelle `kontotyp`
-- 

CREATE TABLE `kontotyp` (
  `id` int(10) unsigned NOT NULL,
  `bezeichnung` varchar(50) collate latin1_general_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- 
-- Daten für Tabelle `kontotyp`
-- 

INSERT INTO `kontotyp` VALUES (1, 'Aktiv');
INSERT INTO `kontotyp` VALUES (2, 'Passiv');
INSERT INTO `kontotyp` VALUES (3, 'Ertrag');
INSERT INTO `kontotyp` VALUES (4, 'Aufwand');
