<?php

if(isset($page_title))
{
	$page_title = ' - '.$page_title;
} else {
	$page_title = '';
}

?>

<!DOCTYPE HTML
		PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
		"http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
	<title><?= $config_airline_name ?><?= $page_title ?></title>
	<link rel="stylesheet" type="text/css" href="media/style.css">
</head>

<body>
