<?php
/*************************************************/
/** M133 jQuery PHP simple call			        **/
/** Author: 	M. von Orelli			        **/
/** Datum:	    20.3.17			                **/
/** Version:	1.0				                **/
/** Applikation:jQuery PHP Function Call JSON   **/
/**                    return value             **/
/*************************************************/

/*************************************************/
/* Datum   ¦ Aenderung                          **/
/*         ¦                                    **/
/*         ¦                                    **/
/*         ¦                                    **/
/*         ¦                                    **/
/*         ¦                                    **/
/*************************************************/
  
 

   $b = "Return value from PHP JSON format";
   print json_encode($b);
  
?>