<?php
/*************************************************/
/** M133 jQuery PHP simple call			        **/
/** Author: 	M. von Orelli			        **/
/** Datum:	    20.3.17			                **/
/** Version:	1.0				                **/
/** Applikation:jQuery PHP Function Call JSON   **/
/**                    return value             **/
/*************************************************/

/*************************************************/
/* Datum   ¦ Aenderung                          **/
/*         ¦                                    **/
/*         ¦                                    **/
/*         ¦                                    **/
/*         ¦                                    **/
/*         ¦                                    **/
/*************************************************/
  
   $var1 = "Array Wert1";
   $var2 = "Array Wert2";   
   $var3 = "Array Wert3";
   
  $return = array('key1' => $var1, 'key2' => $var2, 'key3' => $var3);
  echo json_encode($return);

  
?>