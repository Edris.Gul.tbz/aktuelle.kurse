<?php
// Session starten
session_start();

// TODO: Weiterleiten auf index.php wenn Session nich NICHT vorhanden

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Session gestartet</title>
  </head>
  <body>
    <h1>Hallo <?php echo $_SESSION["User"]?>, Herzlich Willkommen im Portal!</h1>

      <?php

        // TODO: Link zum Logout

        echo "<h3>Der Inhalt deiner Session:</h3>";
        echo "<pre>";
        print_r($_SESSION); // DUMP $_SESSION
        echo "</pre>";
        echo "<h3>Weitere Informationen über die Session: </h3>";
        echo "Session Name: " . session_name() . "<br />"; // Ausgabe Session Name
        echo "Session ID: " . session_id() . "<br />"; // Ausgabe Session ID
        echo "Session Pfad : " . session_save_path() . "<br />"; // Pfad zu Session File
        echo "Session Cache: " . session_cache_expire() . "<br />"; // Session Cache in Minuten
      ?>
  </body>
</html>
