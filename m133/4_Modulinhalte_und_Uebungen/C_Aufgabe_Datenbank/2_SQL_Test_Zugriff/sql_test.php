<form action="" method="post">
   Anfrage:
   <br/>
   <textarea name="query" cols="30" rows="5">
        <?php echo htmlspecialchars(filter_input(INPUT_POST,"query"));?>
    </textarea>
   <br />
   Datenbank:
   <br/>
   <input type="text" name="db" value="<?php echo filter_input(INPUT_POST,"db");?>"/>
   <br/>
   <input type="reset" name="reset" value="Feld leeren"/>
   <input type="submit" name="submit" value="Abschicken"/>
</form>
<hr />

<?php
if(isset($_POST['query']) && !empty($_POST['query']))
{

   //Verbindung herstellen
   $link = mysqli_connect('localhost',
                          'root',
                          '',
                          $_POST["db"]);
   if (!$link)
   {
      echo 'Verbindungsfehler: '.mysqli_connect_error();
      die();
   }
   else
   {
      //Abfrage absenden
      $result = mysqli_query($link,$_POST['query']);

      //Ergebnisverarbeitung
      if(is_object($result))
      {
         //SELECT verarbeiten
         table_output($result);
         echo 'Zeilen im Ergebnis: '.
               mysqli_num_rows($result);
      }
      elseif($result === true && mysqli_affected_rows($link) != -1)
      {
         //INSERT, UPDATE und DELETE verarbeiten
         echo 'Zeilen ver�ndert: '.
               mysqli_affected_rows($link);
      }
      else
      {
         echo 'Fehler: '.mysqli_error($link);
      }
      //Verbindung beenden
      mysqli_close($link);
    }
}
else
{
   echo '<h3>Es wurde keine SQL-Abfrage definiert!</h3>';
}

function table_output($result)
{
   $head = true;
   echo '<table border="1">';
   while($row = mysqli_fetch_assoc($result))
   {
      if($head == true)
      {
         echo '<tr>';
         foreach($row as $key=>$val)
         {
            echo '<th>'.$key.'</th>';
         }
         echo '</tr>';
         reset($row);
         $head = false;
      }   
      echo '<tr>';
      foreach($row as $val)
      {
         echo '<td>'.$val.'</td>';
      }
      echo '</tr>';
   }
   echo '</table>';
}
?>