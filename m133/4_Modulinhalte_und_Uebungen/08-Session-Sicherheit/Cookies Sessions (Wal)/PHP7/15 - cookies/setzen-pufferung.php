<?php
  ob_start();
?>
<html>
<head>
  <title>Cookies</title>
</head>
<body>
<?php
  setcookie("Programmiersprache", "PHP", 
    time() + 60*60*12, "/");
  setcookie("Sprachversion", "7", 
    mktime(0, 0, 0, 12, 24, 2016), "/");
  setcookie("Session", "abc123", null, "/");
?>
<p>Cookies wurden gesetzt!</p>
</body>
</html>
<?php
  ob_end_flush();
?>
