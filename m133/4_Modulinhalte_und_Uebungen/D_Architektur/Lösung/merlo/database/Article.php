<?php

class Article
{
    private $number;
    private $title;
    private $articleNumber;
    private $price;
    private $content;
    private $category;

    /**
     * Article constructor.
     * @param $number
     * @param $title
     * @param $articleNumber
     * @param $price
     * @param $content
     * @param $category
     */
    public function __construct($number, $title, $articleNumber, $price, $content, $category)
    {
        $this->number = $number;
        $this->title = $title;
        $this->articleNumber = $articleNumber;
        $this->price = $price;
        $this->content = $content;
        $this->category = $category;
    }


    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param mixed $number
     */
    public function setNumber($number): void
    {
        $this->number = $number;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getArticleNumber()
    {
        return $this->articleNumber;
    }

    /**
     * @param mixed $articleNumber
     */
    public function setArticleNumber($articleNumber): void
    {
        $this->articleNumber = $articleNumber;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content): void
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category): void
    {
        $this->category = $category;
    }
}