<?php

class ArticleDao
{
    private $connection;

    public function __construct()
    {
        $this->connection = new Connection();
    }
    
    public function selectAll(): array
    {
        $link = $this->connection->getConnection();
        $result = mysqli_query($link, 'select * from ' . $this->connection->getTable());
        $articles = array();
        if ($num = mysqli_num_rows($result)) {
            for ($i = 0; $i < $num; $i++) {
                $nr = $this->mysql_result($result, $i, 'nr');
                $artnr = $this->mysql_result($result, $i, 'artnr');
                $price = $this->mysql_result($result, $i, 'preis');
                $title = $this->mysql_result($result, $i, 'titel');
                $content = $this->mysql_result($result, $i, 'inhalt');
                //added because provided solution did not include category field
                $DUMMY_CAT = 0;

                $articles[] = new Article($nr, $title, $artnr, $price, $content, $DUMMY_CAT);
            }
        }
//		print_r($articles);
		
        $this->connection->closeConnection();
        return $articles;
    }

    public function deleteByNumber($number)
    {
        $link = $this->connection->getConnection();
        $result = mysqli_query($link, 'delete from ' . $this->connection->getTable() . " where nr = '$number'");
        $this->connection->closeConnection();

        return $result;
    }

    public function updateArticleByNumber($artnr, $title, $price, $content, $nr)
    {
        $link = $this->connection->getConnection();
        $result = mysqli_query($link, 'update ' . $this->connection->getTable() . " set artnr = $artnr, titel = '$title', preis = '$price', inhalt = '$content' where nr = '$nr'");
        $this->connection->closeConnection();

        return $result;
    }

    public function selectArticleByNumber($nr)
    {
        $link = $this->connection->getConnection();
        $result = mysqli_query($link, 'select * from ' . $this->connection->getTable() . " where nr =  '" . $nr . "'");

        $article = null;

        if ($num = mysqli_num_rows($result)) {
            for ($i = 0; $i < $num; $i++) {
                $nr = $this->mysql_result($result, $i, 'nr');
                $artnr = $this->mysql_result($result, $i, 'artnr');
                $price = $this->mysql_result($result, $i, 'preis');
                $title = $this->mysql_result($result, $i, 'titel');
                $content = $this->mysql_result($result, $i, 'inhalt');
                //added because provided solution did not include category field
                $DUMMY_CAT = 0;

                $article = new Article($nr, $title, $artnr, $price, $content, $DUMMY_CAT);
            }
        }
        $this->connection->closeConnection();

        return $article;
    }

    public function insertArticle($title, $artnr, $price, $content)
    {
        $link = $this->connection->getConnection();
        $result = mysqli_query($link, 'insert into ' . $this->connection->getTable() . " (titel, artnr, preis, inhalt) VALUES('$title', '$artnr', '$price', '$content')");
        $this->connection->closeConnection();

        return $result;
    }

//  overwrite mysql_result because the function is deprecated
    private function mysql_result($result, $number, $field = 0)
    {
        mysqli_data_seek($result, $number);
        $row = mysqli_fetch_array($result);
        return $row[$field];
    }
}