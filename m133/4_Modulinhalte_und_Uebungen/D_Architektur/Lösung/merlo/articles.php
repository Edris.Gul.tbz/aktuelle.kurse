﻿<?php

require_once 'database/ArticleDao.php';
require_once 'database/Article.php';
require_once 'database/Connection.php';

$message = 'Optionen<P>';
//$con = new Connection();
//$link = $con->getConnection();
$articleDao = new ArticleDao();
$bgColor = '#ffffff';
// Variables from the form
$nr = $_REQUEST['nr'] ?? null;
$action = $_REQUEST['action'] ?? '';
$artnr = $_REQUEST['artnr'] ?? null;
$title = $_REQUEST['titel'] ?? null;
$price = $_REQUEST['preis'] ?? null;
$content = $_REQUEST['inhalt'] ?? null;

// Action that has been selected
switch ($action) {
    case 'loeschen':
        $deletionWorked = $articleDao->deleteByNumber($nr);
        if ($deletionWorked) {
            $message = 'Der Artikel wurde geloescht.';
        } else {
            $message = 'Löschvorgang schlug fehl.';
        }
        break;
    case 'save':
        $updateWorked = $articleDao->updateArticleByNumber($artnr, $title, $price, $content, $nr);
        if ($updateWorked) {
            $message = 'Der Artikel wurde upgedated.';
        } else {
            $message = 'Updatevorgang schlug fehl.';
        }
        break;
    case 'neu':
        $insertResult = $articleDao->insertArticle($title, $artnr, $price, $content);
        if ($insertResult) {
            $message = 'Der Artikel wurde hinzugefuegt.';
        } else {
            $message = 'Insert schlug fehl';
        }
        break;
    case 'update':
        $article = $articleDao->selectArticleByNumber($nr);
        echo '<table>';
        echo '<form action="' . $_SERVER['PHP_SELF'] . '" method="post">
            <input type="hidden" name="action" value="save">
            <input type="hidden" name="nr" value=' . $nr . '>
            <tr>
            <td>Art.-Nr.</td>
            <td><input type="text" name="artnr" value="' . $article->getArticleNumber() . '"></td>
            </tr><tr>
            <td>Titel</td>
            <td><input type=text name="titel" value=' . $article->getTitle() . '></td>
            </tr><tr>
            <td>Preis</td>
            <td><input type=text name="preis" value="' . $article->getPrice() . '"></td>
            </tr><tr>
            <td>Text</td>
            <td><textarea name="inhalt">' . $article->getContent() . '</textarea><td>
            </tr><tr>
            </tr> </td>
            <td><input type=submit value="Artikel Updaten"></form></td>
            </tr>
            </table><p>';
        break;
    case 'formneu':
        echo '<table>';
        echo '<form action"' . $_SERVER['PHP_SELF'] . '" method="post">
           <input type="hidden" name="action" value="neu">
           <tr>
           <td>Art.-Nr.</td>
           <td><input type=text name="artnr"></td>
           </tr><tr>
           <td>Titel</td>
           <td><input type=text name="titel"></td>
           </tr><tr>
           <td>Preis</td>
           <td><input type=text name="preis"></td>     
           </tr><tr>
           <td>Text</td>
           <td><textarea name="inhalt"></textarea></td>
           </tr><tr>
           <td> </td>
           <td><input type=submit value="Neuen Artikel hinzufuegen"></form></td>
           </tr>
           </table><p>';
        break;
    default:
        echo '<ol><b>Alle Artikel in der Übersicht:</b>' .
            '<br>' .
            '<table border= "l" width="700">' .
            '<tr bgcolor="#00cc00"><td width="100"><b>Art.-Nr.<b></td>' .
            '<td width="100"><b>Artikel</b></td>' .
            '<td width="100"><b>Preis</b></td>' .
            '<td width="300"><b>inhalt</b></td>' .
            '<td width="50" ><b>Update</b></td>' .
            '<td width="50"><b>Loeschen</b></td></tr>';
        $result = $articleDao->selectAll();

        if (!empty($result)) {
            foreach ($result as $article) {
				$bgColor = $bgColor === '#ffffff' ? '#ededed' : '#ffffff';
                
                echo '<tr bgColor ="' . $bgColor . '">' .
                    '<td>' . $article->getArticleNumber() . '</td>' .
                    '<td>' . $article->getTitle() . '</td>' .
                    '<td>' . $article->getPrice() . ' Fr. -</td>' .
                    '<td>' . $article->getContent() . '</td>' .
                    '<td><a href="articles.php?nr=' . $article->getNumber() . '&action=update">Update</a></td>' .
                    '<td><a href="articles.php?nr=' . $article->getNumber() . '&action=loeschen">Loeschen</a></td>';
            }
            echo '</tr>';
        } else {
            echo "<tr><td colspan='6' width='100%'>kein Artikel vorhanden!</td></tr>";
        }
        echo '</table>';
        echo '</ol>';
}
// Print message and nav links
echo "$message" .
    '<p><a href=articles.php?action=start>Zur Startseite</a>' .
    ' - <a href=articles.php?action=formneu>Neuen Artikel einfuegen?</a>' .
    '</ol>';
