<?php

// Read kontis
$query = '
SELECT
	id, bezeichnung, kontonr
FROM
	konto
ORDER BY
	kontonr ASC';
$result = $dbconnection->query($query);
$konto_list = $result->fetchAll();

// Check booking
$datum = '';
$sollkonto = '';
$habenkonto = '';
$text = '';
$betrag = '';
if(isset($_POST['book']) && $_POST['book'] == 'true')
{
	// Read data
	$datum = $_POST['datum'];
	$sollkonto = $_POST['sollkonto'];
	$habenkonto = $_POST['habenkonto'];
	$text = $_POST['text'];
	$betrag = $_POST['betrag'];
	
	// Do booking
	$success = true;
	$error = '';
	$dbconnection->exec('BEGIN');
	
	// Haben
	$result = $dbconnection->query("SELECT kontotyp FROM konto WHERE id=$habenkonto");
	$operator_haben = '-';
	$row = $result->fetch();
	$result->fetchAll(); // Clear
	if($row['kontotyp'] == 2 || $row['kontotyp'] == 3)
	{
		$operator_haben = '+';
	}
	$query = "UPDATE konto SET kontostand=kontostand$operator_haben$betrag WHERE id=$habenkonto";
	if($dbconnection->exec($query) === false)
	{
		$success = false;
		$error = 'Fehler bei Buchung auf Habenkonto';
	}
	
	// Soll
	$result = $dbconnection->query("SELECT kontotyp FROM konto WHERE id=$sollkonto");
	$operator_soll = '+';
	$row = $result->fetch();
	$result->fetchAll(); // Clear
	if($row['kontotyp'] == 2 || $row['kontotyp'] == 3)
	{
		$operator_soll = '-';
	}
	$query = "UPDATE konto SET kontostand=kontostand$operator_soll$betrag WHERE id=$sollkonto";
	if($dbconnection->exec($query) === false)
	{
		$success = false;
		$error = 'Fehler bei Buchung auf Sollkonto';
	}
	
	// Booking text
	$query = "INSERT INTO buchung (datum,sollkonto,habenkonto,text,betrag) VALUES ('$datum','$sollkonto','$habenkonto','$text','$betrag')";
	if($dbconnection->exec($query) === false)
	{
		$success = false;
		$error = 'Fehler bei Eintrag der Buchung';
	}
	
	// Check kontostand
	$result = $dbconnection->query("SELECT kontostand FROM konto WHERE id=$habenkonto");
	$row = $result->fetch();
	$result->fetchAll(); // Clear
	if(floatval($row['kontostand']) < 0.0)
	{
		// Rollback, transaction not possible
		$dbconnection->exec('ROLLBACK');
		$stand = ($operator_haben == '+') ? $row['kontostand']-$betrag : $row['kontostand']+$betrag;
		echo("Transaktion nicht m&ouml;glich. Zu kleiner Betrag auf Habenkonto ($stand)");
	} else if($success == false)
	{
		// Rollback, transaction not possible
		$dbconnection->exec('ROLLBACK');
		echo($error);
	} else {
		// Commit, all is fine
		$dbconnection->exec('COMMIT');
		echo('Transaktion erfolgreich ausgef&uuml;hrt.');
	}
}
?>

<h1>Buchung</h1>

<form enctype="multipart/form-data" method="post" action="index.php?page=booking">
<input type="hidden" name="book" value="true">
<table>
	<tr>
		<th>Datum</th>
		<th>Sollkonto</th>
		<th>Habenkonto</th>
		<th>Text</th>
		<th>Betrag</th>
		<th>&nbsp;</th>
	<tr>
	<tr>
		<td><input type="text" name="datum" value="<?= $datum ?>"></td>
		<td>
			<select	name="sollkonto">
			<?php
			foreach($konto_list as $konto) {
				$selected = '';
				if($konto['id'] == $sollkonto) $selected = ' selected="selected"';
			?>
				<option value="<?= $konto['id'] ?>"<?=$selected ?>><?= $konto['kontonr'] ?> - <?= $konto['bezeichnung'] ?></option>
			<?php } ?>
			</select>
		</td>
		<td>
			<select	name="habenkonto">
			<?php foreach($konto_list as $konto) {
				$selected = '';
				if($konto['id'] == $habenkonto) $selected = ' selected="selected"';
			?>
				<option value="<?= $konto['id'] ?>"<?=$selected ?>><?= $konto['kontonr'] ?> - <?= $konto['bezeichnung'] ?></option>
			<?php } ?>
			</select>
		</td>
		<td><input type="text" name="text" value="<?= $text ?>"></td>
		<td><input type="text" name="betrag" value="<?= $betrag ?>"></td>
		<td><input type="submit" value="Buchen"></td>
	<tr>
</table>
</form>
