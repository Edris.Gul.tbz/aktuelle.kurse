<?php
/**
 * Created by PhpStorm.
 * User: mvorelli
 * Date: 15.11.2017
 * Time: 12:13
 */

// http://localhost:8080/pdo/pdo.php
 
 
echo "PDO Demo File";

  $user="root";
  $pw = "";

try 
{
    $dbh = new PDO('mysql:host=localhost;dbname=personen_orte', $user, $pw);
    foreach ($dbh->query('SELECT * from tbl_orte') as $row) {
        print_r($row);
    }
	// close DB connection
    $dbh = null;
} 
catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
    die();
}

?>