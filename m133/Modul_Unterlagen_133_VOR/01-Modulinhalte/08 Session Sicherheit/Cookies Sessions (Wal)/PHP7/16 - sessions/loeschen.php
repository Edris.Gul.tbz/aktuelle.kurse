<?php
  session_start();
?>
<html>
<head>
  <title>Sessions</title>
</head>
<body>
<?php
  session_unset();
  session_destroy();
  setcookie(session_name(), "weg damit", 0, "/");
?>
<p>Alles gel&ouml;scht!</p>
</body>
</html>
