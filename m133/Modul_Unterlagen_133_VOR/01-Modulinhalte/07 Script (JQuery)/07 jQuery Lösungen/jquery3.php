<?php
/*************************************************/
/** M133 jQuery PHP simple call			        **/
/** Author: 	M. von Orelli			        **/
/** Datum:	    20.3.17			                **/
/** Version:	1.0				                **/
/** Applikation:jQuery call PHP  		        **/
/*************************************************/

/*************************************************/
/* Datum   ¦ Aenderung                          **/
/*         ¦                                    **/
/*         ¦                                    **/
/*         ¦                                    **/
/*         ¦                                    **/
/*         ¦                                    **/
/*************************************************/
  $CRLF = chr(13). chr(10);
  $myfile = fopen("jquery3.log", "a+");
  fwrite($myfile, "Call from jQuery". $CRLF );
  fclose($myfile);

?>