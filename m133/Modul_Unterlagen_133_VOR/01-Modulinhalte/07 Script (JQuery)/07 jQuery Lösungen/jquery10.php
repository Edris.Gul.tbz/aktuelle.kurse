<?php
/*************************************************/
/** M133 jQuery PHP simple call			        **/
/** Author: 	M. von Orelli			        **/
/** Datum:	    20.3.17			                **/
/** Version:	1.0				                **/
/** Applikation:jQuery PHP Addition             **/
/**                    return value result      **/
/*************************************************/

/*************************************************/
/* Datum   ¦ Aenderung                          **/
/*         ¦                                    **/
/*         ¦                                    **/
/*         ¦                                    **/
/*         ¦                                    **/
/*         ¦                                    **/
/*************************************************/
  
  $zahl1 = floatval($_REQUEST['zahl1']);
  $zahl2 = floatval($_REQUEST['zahl2']);
  $result = $zahl1 + $zahl2;
  echo $result;

  
?>