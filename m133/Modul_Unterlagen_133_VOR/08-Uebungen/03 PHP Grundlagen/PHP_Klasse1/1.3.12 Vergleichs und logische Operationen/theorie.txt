logische Operatoren
====================

and  = beide true
or   = eines oder beide true
xor  = eines der beiden true (entweder oder)
nor  = nicht eines oder beide true (or umgekehrt)
nand = nicht beide gleich (and umgekehrt)
xnor = gleichheit

    and     or    xor    nxor    nand    nor
--------------------------------------------
00   0      0	   0      1       1	  1
01   0      1      1      0       1       0
10   0      1      1      0       1       0
11   1      1      0      1       0       0

in PHP funktionieren nxor, nand und nor nicht!!!

Vergleichsoperatoren
====================

gleich:

10 == 10
"PHP" == "PHP"


ungleich:

"PHP" != "CGI"
"10 != 10


gr�sser / kleiner als:

15 > 10
15 < 15


gr�sser / kleiner gleich:

15 >= 15
15 <= 10
