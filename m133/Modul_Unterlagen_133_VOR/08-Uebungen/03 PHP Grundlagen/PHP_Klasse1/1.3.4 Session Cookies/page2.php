<?
session_start();					//Dieser Befehl wieder am Anfang

echo "Variable1: ", $variable1, "<br>";			//nun kann 'Variable1' ganz normal verwendet werden
echo "Variable2: ", $variable2, "<br>";			//Ausgabe der drei Variablen
echo "Variable3: ", $variable3, "<br>";			//

/*Weitere Befehle im Zusammenhang mit Sessions*/

//session_name('name');					//Mit diesem Befehl kann der Session ein neuer Name gegeben werden.<br>
//session_destroy();					//Damit wird die Session vernichtet. Alle Variablen gehen dabei verloren.

?>