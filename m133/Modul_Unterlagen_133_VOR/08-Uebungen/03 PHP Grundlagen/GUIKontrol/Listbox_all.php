Listbox

 

 

http://de.selfhtml.org/html/formulare/auswahl.htm
<http://de.selfhtml.org/html/formulare/auswahl.htm> 

 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Strict//EN"
       "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>Text des Titels</title>
</head>
<body>
 
<h1>W&auml;hlen Sie Ihren Favoriten!</h1>
 
<form action="select.htm">
<p>
<select name="top5" size="3">
<option>Heino</option>
<option>Michael Jackson</option>
<option>Tom Waits</option>
<option>Nina Hagen</option>
<option>Marianne Rosenberg</option>
</select>
</p>
</form>
 
</body>
</html>

 

Mehrfach Listen

http://de.selfhtml.org/html/formulare/auswahl.htm#listen_mehrfach
<http://de.selfhtml.org/html/formulare/auswahl.htm#listen_mehrfach> 

 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Strict//EN"

       "http://www.w3.org/TR/html4/strict.dtd">

<html>

<head>

<title>Text des Titels</title>

</head>

<body>

 

<h1>W&auml;hlen Sie so viele Favoriten wie Sie wollen!</h1>

 

<form action="select.htm">

<p>

<select name="top5" size="5" multiple>

<option>Heino</option>

<option>Michael Jackson</option>

<option>Tom Waits</option>

<option>Nina Hagen</option>

<option>Marianne Rosenberg</option>

</select>

</p>

</form>

 

</body>

</html>

 

Vorselektierte Listbox

http://de.selfhtml.org/html/formulare/auswahl.htm#listen_vorselektiert

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Strict//EN"

       "http://www.w3.org/TR/html4/strict.dtd">

<html>

<head>

<title>Text des Titels</title>

</head>

<body>

 

<h1>Sie k&ouml;nnen auch einen anderen Favoriten w&auml;hlen!</h1>

 

<form action="select.htm">

<p>

<select name="top5" size="5">

<option>Heino</option>

<option>Michael Jackson</option>

<option selected>Tom Waits</option>

<option>Nina Hagen</option>

<option>Marianne Rosenberg</option>

</select>

</p>

</form>

 

</body>

</html>

 

http://de.selfhtml.org/html/formulare/auswahl.htm#radiobuttons
<http://de.selfhtml.org/html/formulare/auswahl.htm#radiobuttons> 

RadioButtons

 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Strict//EN"

       "http://www.w3.org/TR/html4/strict.dtd">

<html>

<head>

<title>Text des Titels</title>

</head>

<body>

 

<h1>Hier wird abkassiert!</h1>

 

<form action="input_radio.htm">

<p>Geben Sie Ihre Zahlungsweise an:</p>

<p>

<input type="radio" name="Zahlmethode" value="Mastercard"> Mastercard<br>

<input type="radio" name="Zahlmethode" value="Visa"> Visa<br>

<input type="radio" name="Zahlmethode" value="AmericanExpress"> American Express

</p>

</form>

 

</body>

</html>

 

http://de.selfhtml.org/html/formulare/auswahl.htm#checkboxen

CheckBox

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Strict//EN"

       "http://www.w3.org/TR/html4/strict.dtd">

<html>

<head>

<title>Text des Titels</title>

</head>

<body>

 

<h1>Pizzabelag nach Wahl!</h1>

 

<form action="input_checkbox.htm">

<p>Kreuzen Sie die gew&uuml;nschten Zutaten an:</p>

<p>

<input type="checkbox" name="zutat" value="salami"> Salami<br>

<input type="checkbox" name="zutat" value="pilze"> Pilze<br>

<input type="checkbox" name="zutat" value="sardellen"> Sardellen

</p>

</form>

 

</body>

</html>

 
