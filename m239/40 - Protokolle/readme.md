# Protokolle
 
## DNS
- 10 - Wie DNS arbeitet (interaktiv)
<br>Etwas Versign-lastig, aber anschaulich erklärt
<br>Verweis: https://www.verisign.com/de_DE/domain-names/how-dns-works/index.xhtml
 	 	 	 	 	 	 
- 11 - DNS, rekursive und iterative Namensaufloesung
<br>Verweis: https://de.wikipedia.org/wiki/Rekursive_und_iterative_Namensaufl%C3%B6sung
 	 	 	 	 	 	 
- 50 - Angaben zum MX-Record (Wikipedia)
<br>Verweis: https://de.wikipedia.org/wiki/MX_Resource_Record
 	 	 	 	 	 	 
- 80 - DynDNS - Ermöglicht Webserver ohne fixe IP-Adresse
<br>Verweis: https://de.wikipedia.org/wiki/Dynamisches_DNS
 	 	 	 	 	 	 
- 80 - NsLookup (Abfragen nach unterschiedlichen Ressource Record-Typen A,NS,MX, etc. möglich)
<br>Tool, um die Mailserver, Nameserver, etc. zu eruieren
<br>Verweis: https://bscw.tbz.ch/bscw/bscw.cgi/d23029528/NsLookup
 	 	 	 	 	 	 
- PowerCert - How a DNS Server (Domain Name System) works
<br>6:04 min, E, YouTube, 26.05.2016
<br>Verweis: https://www.youtube.com/watch?v=mpQZVYPuDGU


## HTTPs

- 04 - Fiddler - Tool zur Analyse von HTTP-Nachrichten
<br>Verweis: http://www.telerik.com/fiddler
	
- 05 - HTTP Hands on. (Anleitungen zum ausprobieren)
<br>**Skript** CAL, SUK, KAE ca. 2019
<br>[M239_hands-on_HTTP_v1.6.pdf/](vx-ressources/13%20-%20M239_hands-on_HTTP_v1.6.pdf)
	
- 07 - Wie funktioniert HTTPS (Video, englisch)
<br>11 min, E, 11.03.2011
<br>Verweis: https://www.youtube.com/watch?v=JCvPnwpWVUQ

- 10 - Wie funktioniert das HTTP-Protokoll
<br>Gute Einführung in das Protokoll
<br>Verweis: https://www.mediaevent.de/tutorial/http-request.html

- 12 - How the web works
<br>Sehr guter Artikel über den Ablauf von HTTP. Wichtig: Text super, aber erste Grafik sehr missverständlich!
<br>Verweis: https://academind.com/tutorials/how-the-web-works/

- 17 - Content Negotiation
<br>Beschreibt Content Negotiation grundsätzlich und im Zusammenhang mit dem Apache-Server
<br>Verweis: https://httpd.apache.org/docs/current/content-negotiation.html

- 20 - HTTP mit Putty durchführen (Video)
<br>Das Tool "Putty" kann anstelle von Telnet verwendet werden
<br>Verweis: https://www.youtube.com/watch?v=ptJYNY7UbQU

- 30 - Wie funktioniert HTTPS
<br>Verweis: http://www.softed.de/blog/wie-funktioniert-https/?h=redir

- 31 - How does HTTPS work - Whats a CA - What's a self-signed Certificate
<br>11 min, E, 2.4.2018
<br>Verweis: https://www.youtube.com/watch?v=T4Df5_cojAs
	
- 31 - HTTPS vs HTTP vs SSL-TLS
<br>7 min, E, Youtube
<br>Verweis: https://www.youtube.com/watch?v=hExRDVZHhig&t=7s

- 50 - HTTP-Statuscodes
<br>Mit einem Statuscode gibt der Server in der Antwort Auskunft über den Verlauf der Anfrage.
<br>Verweis: https://de.wikipedia.org/wiki/HTTP-Statuscode

- 80 - HTTP2
<br>Alle Details zum neuen Standard HTTP2 (auf englisch)
<br>Verweis: https://http2.github.io/
 	 	 	 	 	 	 
						 
- PowerCert - Port Forwarding explained
<br>9:03 min, E, YouTube, 1.11.2016
<br>Verweis: https://www.youtube.com/watch?v=2G1ueMDgwxw


### Aufgaben/Arbeitsblätter zu HTTP und URL


- Arbeitsblatt mit 17 Aufgaben zum selber (im Team zu 2 Pers) bearbeiten. Dauer ca. 100 min.
<br>[x-ressources/16%20-%20m239_URL_Spez.pdf](x-ressources/16%20-%20m239_URL_Spez.pdf)

- Arbeitsblatt "HTTP Protokoll"
<br>Ablauf, Client-Anfrage, GET-Methode, POST-Methode, Laborübung: Network Analyzer / Packet Sniffer 
<br>[x-ressources/15%20-%20HTTP-Protokoll%20mit%20Labor.pdf](x-ressources/15%20-%20HTTP-Protokoll%20mit%20Labor.pdf)




## WebDAV

- Finden Sie heraus, was WebDAV ist und warum man 
das gut im Windows-Betriebssystem brauchen kann.

- bitKinex
<br>Verweis: http://www.bitkinex.com/?v=3.2.3&u=1
<br>[x-ressources/bitKinex-ohnePW.ds.txt](x-ressources/bitKinex-ohnePW.ds.txt)



## SMTP, IMAP, POP3 (E-Mail)
	
- 01 - Der Weg einer E-Mail
<br/>13 min, D, 2011
<br>Verweis: https://www.kabeleins.ch/tv/abenteuer-leben/videos/der-weg-der-e-mail-clip
 	 	 	 	 	 	 
- 05 - E-Mail und die Protokolle
<br/>Guter Überblick über die Mail-Protokolle
<br>Verweis: http://www.elektronik-kompendium.de/sites/net/0902261.htm
 	 	 	 	 	 	 
- SMTP, senden und empfangen von Mailserver
<br>**Skript** 22 Seiten, K. Suter, 2010
<br>[M239_mail_tecchannel_V0.3.pdf](x-ressources/02%20-%20M239_mail_tecchannel_V0.3.pdf) 

- 10 - What Is SMTP
<br/>Kleine Einführung in das SMTP-Protokoll (englisch)
<br>Verweis: https://www.youtube.com/watch?v=tmE9OqjdK7s
 	 	 	 	 	 	 
	
- 13 - Das SMTP-Protokoll im Detail betrachtet
<br/>Gute Anleitung für die Erkundung des Protokolls
<br>Verweis: http://www.phpgangsta.de/das-smtp-protokoll-im-detail-betrachtet

	
- 14 - TelnetSMTP-TestTool.exe
 <br/>Verweis: [TelnetSMTP-TestTool.exe](x-ressources/14%20-%20TelnetSMTP-TestTool.exe) 	 	 
	
- 15 - E-Mail-Nachricht per Telnet versenden
<br/>Verweis: https://entwicklergate.de/t/e-mail-nachricht-per-telnet-versenden/118

	
- 16 - How to test an SMTP server - Send email from command prompt
<br/>4 min, E, 2018
<br>Verweis: https://www.youtube.com/watch?v=lfYtz3uRPYc


- 17 - Send an email using telnet
<br/>2 min, E, 2008
<br>Verweis: https://www.youtube.com/watch?v=wOxj_HvnLmE


- 19 - Spezialzeichen im Mail-Betreff
<br/>Zeigt, wie man Spezialzeichen (Flugzeug, Autos, etc.) im Betreff verwenden kann.
<br>Verweis: https://www.evernote.com/shard/s147/sh/b6a99e6a-1bac-454e-8c8d-a3352f0fb298/6bca843933716b1785299dc23694de0f

	
- 40 - Das POP3-Protokoll im Detail betrachtet
<br/>Gute Anweisung für die Erkundung des Protokolls
<br>Verweis: http://www.phpgangsta.de/das-pop3-protokoll-im-detail-betrachtet


- 45 - PowerCert - POP3 vs IMAP - What's the difference
<br/>7:49 min, E, YouTube, 27.3.2018
<br>Verweis: https://www.youtube.com/watch?v=SBaARws0hy4

	
- 50 - Das IMAP-Protokoll im Detail betrachtet
<br/>Gute Anleitung zur Erkundung des Protokolls
<br>Verweis: http://www.phpgangsta.de/das-imap-protokoll-im-detail-betrachtet
 	 	 	 	 	 	 
	
- 51 - Gute Erklärungen zum IMAP-Protokoll
<br/>Die Beschreibung ist zwar schon älter (2007), aber sehr gut geschrieben und immer noch aktuell.
<br>Verweis: http://www.linux-magazin.de/Ausgaben/2007/06/Facharbeiten-mit-Verstand


	
- 52 - Mail-Header
<br/>Gute Beschreibung der möglichen Mail-Header (bspw. Return-Path, Message-ID, CC, BCC)
<br>Verweis: https://gaijin.at/manmailhead.php
 	 	 	 	 	 	 
	
- 53 - Header-FAQ
<br/>Zweck dieser FAQ ist es, grundlegende Informationen über den Aufbau einer Internet-E-Mail und die Bedeutung der einzelnen Headerzeilen zu vermitteln, um insbesondere den Weg einer E-Mail zurückzuverfolgen, den Absender bzw. die beteiligten Mailserver herauszufinden und sich bei unerwünschter Massen-E-Mail (Bulkmail oder UBE/UCE) oder anderen unerwünschten Zusendungen gezielt an den richtigen Stellen beschweren zu können.
<br>Verweis: https://th-h.de/net/usenet/faqs/headerfaq/
