# 70 - Tests


- Testing Tools Guide
<br>Übersicht über eine grosse Anzahl (>800) Test-Tools für verschiedene Arten von Tests
<br>Verweis: http://www.testingtoolsguide.net

	
- Dokumentation verschiedener Testarten (die Mind-Map wurde aus diesem Text erstellt)
<br>Verweis: [../02 - Unterlagen Theorie/M239 Compendio B 7 - Systemtest und Dokumentation vorbereiten.pdf](../02%20-%20Unterlagen%20Theorie/M239%20Compendio%20B%207%20-%20Systemtest%20und%20Dokumentation%20vorbereiten.pdf)	

![M239_Internetserver_Tests_Mindmap.png](M239_Internetserver_Tests_Mindmap.png)

