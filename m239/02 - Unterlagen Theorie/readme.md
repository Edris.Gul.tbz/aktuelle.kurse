# Anforderungsanalyse

M239 Lastenheft oder Pflichtenheft, wozu braucht man das
12:49 min, YouTube, D
Verweis: https://www.youtube.com/watch?v=-4Tq83wZFw8

PowerCert - DHCP Explained - Dynamic Host Configuration Protocol
10:09 min, E, YouTube, 1.5.2017
Verweis: https://www.youtube.com/watch?v=e6-TaH5bkjo


PowerCert - FTP (File Transfer Protocol), SFTP, TFTP Explained
7:54 min, E, YouTube, 6.11.2018
Verweis: https://www.youtube.com/watch?v=tOj8MSEIbfA

	
PowerCert - How a DNS Server (Domain Name System) works
6:04 min, E, YouTube, 26.05.2016
Verweis: https://www.youtube.com/watch?v=mpQZVYPuDGU

PowerCert - HTTP, HTTPS, SSL and TLS Explained
6:30 min, E, YouTube, 12.12.2018
Verweis: https://www.youtube.com/watch?v=hExRDVZHhig&t=7s

PowerCert - POP3 vs IMAP - What's the difference
7:49 min, E, YouTube, 27.3.2018
Verweis: https://www.youtube.com/watch?v=SBaARws0hy4

	
PowerCert - Port Forwarding Exlained
9:03 min, E, YouTube, 1.11.2016
Verweis: https://www.youtube.com/watch?v=2G1ueMDgwxw

	
PowerCert - What is a DMZ (Demilitarized Zone)
6:13 min, E, YouTube, 17.9.2018
Verweis: https://www.youtube.com/watch?v=dqlzQXo1wqo