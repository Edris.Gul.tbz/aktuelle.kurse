# M231 Datenschutz und Datensicherheit anwenden

Inhaltsverzeichnis

[TOC]


[**Modulidentifikation** ICT CH](https://www.modulbaukasten.ch/module/231/1/de-DE?title=Datenschutz-und-Datensicherheit-anwenden)

[TBZ Unterlagen -> https://gitlab.com/ch-tbz-it/Stud/m231](https://gitlab.com/ch-tbz-it/Stud/m231)


[Kompetenzmatrix](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/00_kompetenzband)

Buch zum Unterricht mit TBZ-Mailadresse: [Bücher vom HERDT-Verlag](https://herdt-campus.com/apply-token/grh_7xQuPjCbc_ty-WIlBuhAFC5Y_Ca5zi5ienS5pmdZc58h) zum Download<br>[Informationstechnologie Grundlagen (Stand 2021)](https://herdt-campus.com/product/ITECH_2021)
<br>Lokal: [Informationstechnologie Grundlagen (Stand 2021)](ITECH_2021.pdf)

## Leistungsbeurteilungen (Prüfungen)

[Leistungsbeurteilungen (2 Prüfungen & 1 Dossier/Lernjournal)..](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/99_Leistungsbeurteilung)


- Inhalt der Leistungsbeurteilung 1 (30%)"**LB1**":<br>
(Der Gebrauch des Dossiers/Lernjournals und des Internets ist erlaubt)

	- Datenschutz und Datensicherheit, Datenschutzgesetz DSG (Grundsätze)
	- Git-Konzept (grobe Funktionsweise lokal und in der Cloud), Git-(Grund-)Befehle
	- Datensicherheit


- Inhalt der Leistungsbeurteilung 2 (30%)"**LB2**":<br>
<sup>_(Der Gebrauch des Dossiers/Lernjournals und des Internets kann erlaubt werden)_</sup>
	- Backup-Konzepte und Backup-Strategien
	- Passwörter, Autorisierung
	- Verschlüsselungen

- Dossier/Lernjournal (40%)

[Tipps für das Dossier/Lernjournal..](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/06_Tipps?ref_type=heads#lernjournal)


## Ablaufplan 2023/24-Q2 (Fr nachmittags)

|Tag  |AP23d     |Thema, Auftrag, Übung |
|---- |----      |----                  |
| 1.) | Fr 17.11.| Begrüssung ... <br>[Ablagesysteme](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/04_Ablagesysteme), -> [01 Wo speichere ich meine Daten](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/04_Ablagesysteme/01_Wo%20speichere%20ich%20meine%20Daten%20ab.md)|
| 2.) | Fr 24.11.| [Ablagesysteme](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/04_Ablagesysteme) -> [02 Eigenes Ablagekonzept](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/04_Ablagesysteme/02_Eigenes%20Ablagekonzept.md) ,<br>[Git und Markdown](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/10_Git#einf%C3%BChrung-in-git-und-markdown) - [Eigene GIT Umgebung](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/10_Git/03_Eigene%20GIT%20Umgebung.md) - [Markdown-Guide](https://www.markdownguide.org/basic-syntax/)  |
|  -  |-fällt aus-| - Klausurtagung -                                                              |                           |
| 3.) | Fr 08.12.|  *"Datenschutz"* [Schutzwürdigkeit von Daten im Internet](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/01_Datenschutz),<br> Teams-Auftrag M231-3a:<br> - [Datenschutzrecht seit 1.9.2023](https://www.admin.ch/gov/de/start/dokumentation/medienmitteilungen.msg-id-90134.html),<br> - [Datenschutzgesetz DSG](https://www.fedlex.admin.ch/eli/fga/2020/1998/de) und [Datenschutzverordnung DVO](https://www.newsd.admin.ch/newsd/message/attachments/75620.pdf),<br> -- [SRF-Video zum Thema Datenschutz (14 min)](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/01_Datenschutz/09_Video%20SRF.md) <br> -- [Cyber-Game](./x_ressources/cyber-game.jpg), ([.pdf](./x_ressources/Cyber-Game-Spielanleitung.pdf))  |
| 4.) | Fr 15.12.| *"Datensicherheit der eigenen Infrastruktur"*<br>[Backup](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/05_Backup)     |
| 5.) | Fr 22.12.| **LB1**<br>Weiterarbeit am "Backup" <br>Konzept erstellen (fürs Smartphone, für den Notebook)<br>Bereitstellung und Durchführung des Backups für den Notebook/Laptop |
| - - | Ferien   | - - |
| 6.) | Fr 12.01.| *"_Schutzwürdigkeit von Daten im Internet_"*<br>Zugangsarten/-systeme und [Passwörter](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/03_Passw%C3%B6rter),<br>[Warum Passwörter](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/03_Passw%C3%B6rter/02_Diskussion%20Passw%C3%B6rter.md), [Authentifizierung und Autorisierung](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/03_Passw%C3%B6rter/03_Authentifizierung%20und%20Autorisierung.md), [Was ist Multi-Faktor-Authentisierung](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/03_Passw%C3%B6rter/05_MFA.md), <br>[Passwörterverwaltung, Passwortmanager](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/03_Passw%C3%B6rter/06_Passwortmanager.md) einrichten   |
| 7.) | Fr 19.01.| Einstieg in die Datenverschlüsselung<br>[Challange - Eigene Verschlüsselung](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/02_Verschl%C3%BCsselung/00_Challenge.md),<br> [FIDO - Anmeldeverfahren ohne Passwörter](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/03_Passw%C3%B6rter/fido-anmeldeverfahren)<br> [Datenverschlüsselung](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/02_Verschl%C3%BCsselung) (-> [Cäsar/Vigenère-Verschlüsselung / CrypTool](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/02_Verschl%C3%BCsselung/01_Caesar.md))   |
| 8.) | Fr 26.01.| **LB2** <br>*"Konsequenzen von Fehlern im Datenschutz und bei der Datensicherheit"*<br>[Problematik der Datenloeschung](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/06_BackupProbleme-und-Juristisches/problematik-datenloeschung.md?ref_type=heads) sowie [Impressum, Discaimer, AGB](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/06_BackupProbleme-und-Juristisches/impressum-disclaimer-agb.md?ref_type=heads) |
| 9.) | Fr 02.02.| [Lizenzmodelle](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/07_Lizenzmodelle?ref_type=heads)<br>Anwendungen auf Datenschutzkonformität überprüfen <br>Abgabe **LB3** |



## Empfehlung

Abonnieren und hören Sie ab jetzt! den wöchentlichen 
Podcast (Audio und/oder Video) von "c't uplink", 
denn da werden Sie immer mit den neuesten Trends 
und Empfehlungen zu Informatik-Themen professionell 
und in deutscher Sprache bedient<br>
https://www.heise.de/thema/ct-uplink


@ Harald Müller, August 2023

