# Retrospektive


![MindMap Retrospektive](./M426_MindMap_Sprint Retrospektive.jpg)
<hr>

![Cartoon_015_retrospektive1](./M426_projektcartoon_015_retrospektive1.jpg)
<hr>

![Retrospektive im Scrum](./Retrospektive im Scrum.jpg)
<hr>

![Starfish-Retrospektive 1](./Starfish-Retrospektive 1.jpg)
<hr>

![Starfish-Retrospektive 2](./Starfish-Retrospektive 2.jpg)
<hr>

![SprintRetrospektive_Postkarte](./Scrum_SprintRetrospektive_Postkarte.jpg)

## Tools

- [Retromat - Ablauf einer Retrospektive zufällig zusammenstellen](https://retromat.org/de/)
- [Retrotool.io](https://retrotool.io)
