### Aufgabe/Task: Nr. 13

Thema: Trees & Graphs

Aufgabenbeschreibung:
Versuchen Sie, z.B. im Zweierteam, die Navi-, bzw. Routenplaner-Aufgabe zu lösen. 

Ihr Programm soll wie ein Navi funkionieren. Es soll folgende Fragen beantworten können:
- Wie lange dauert eine Fahrt von Genf (GE) nach Konstanz (KO) und wo führt sie entlang?
- Wenn jetzt zwischen Bern (BE) und Zürich (ZH) ein Stau aufgrund eines Unfalles herrscht und diese Verbindung anstelle von "2" jetzt "7" dauert. Wo führt der Weg jetzt entlang und wie lange dauert nun die Fahrt zwischen Genf (GE) und Konstanz (KO)

Bewertung: Keine.


Sie haben die Daten: [csv](./schweizerkarte_staedteverbindungen.csv), [txt](./schweizerkarte_staedteverbindungen.txt), [xlsx](./schweizerkarte_staedteverbindungen.xlsx)

und diese Karte

![Karte](./schweizerkarte_staedtenamen.jpg)

```
GE	NE	2
GE	TH	2
GE	LG	5
NE	CF	1
NE	BI	1
NE	BE	1
CF	NE	1
CF	BI	1
BI	CF	1
BI	BE	1
BI	BS	2
BI	ZH	2
BE	BI	1
BE	NE	1
BE	TH	1
BE	ZH	2
TH	GE	2
TH	LU	2
LU	ZH	1
LU	LG	3
LU	TH	2
LG	LU	3
LG	GE	5
LG	CH	3
CH	LG	3
CH	LU	3
CH	VD	1
CH	ZH	2
VD	CH	1
VD	SG	1
SG	VD	1
SG	KO	1
SG	WI	1
KO	SG	1
KO	SH	1
KO	WI	1
SH	KO	1
SH	WI	1
SH	BS	2
BS	SH	2
BS	ZH	2
BS	BI	2
ZH	BS	2
ZH	WI	1
ZH	CH	2
ZH	LU	1
ZH	BE	2
ZH	BI	2
TH	BE	1
TH	GE	2
TH	LU	2
WI	SH	1
WI	KO	1
WI	ZH	1
```