### Aufgabe/Task: Nr. 03

Thema: Erste Schritte

Geschätzter Zeitbedarf: 60-120min

#### Aufgabenbeschreibung:

Beginnen Sie heute noch mit der Installation und den Übungen 1 bis 6 im
verlinkten Dokument.

 - [script1_1_Daten_einlesenUndAusgeben.pdf](./script1_1_Daten_einlesenUndAusgeben.pdf)

Minimal-Leistung: Zu Beginn des Fachunterrichts das nächste Mal, zeigen Sie der
LP auf Ihrem Rechner die Übung 1, 2 und 3.

Bewertung: Keine. Jedoch muss die Minimalleistung der LP gezeigt werden.
