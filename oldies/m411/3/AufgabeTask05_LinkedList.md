### Aufgabe/Task: Nr. 05

Thema: Linked List / Verkettete Liste

Geschätzter Zeitbedarf: 30-70min pro Aufgabe

#### Aufgabenbeschreibung:

Zur Unterstützung und weitere Anleitung, schauen Sie sich dieses Video an.

<https://www.youtube.com/watch?v=i2v_Ve9PUCw>

Bauen Sie gemäss dieser Anleitung eine (einfache) Verlinkte Liste.

- [script3_2_dynamicStructures_linkedList.pdf](./script3_2_dynamicStructures_linkedList.pdf)
- [script3_2_dynamischeStrukturen_verkListe.pdf](./script3_2_dynamischeStrukturen_verkListe.pdf)

- [Java LinkedList – Wie du eine verkettete Liste implementierst. Der Workshop!](http://www.codeadventurer.de/?p=1844)

**Anleitung mit Bildern und Code**
Geeks for Geeks (C, C++, C#, Java, JavaScript, Python. --> Code auf "Java" stellen)
- [1 Introduction](https://www.geeksforgeeks.org/linked-list-set-1-introduction)
- [2 Insert node](https://www.geeksforgeeks.org/linked-list-set-2-inserting-a-node)
- [3 Delete node](https://www.geeksforgeeks.org/linked-list-set-3-deleting-node)


Bewertung: Keine, ist aber prüfungsrelevant
