package com.doerzbach;

public enum Geschlecht {
    maennlich("Herr"),weiblich("Frau");
    private final String anrede;
    Geschlecht(String anrede) {
        this.anrede=anrede;
    }

    @Override
    public String toString() {
        return anrede;
    }
}
