# Java 9
Grundlagen, Programmierun

Elmar Fuchs, 1. Ausgabe, Oktober 2017 , ISBN 978-3-86249-645-7

 - [JAV9.pdf](./JAV9.pdf)
 
## Wissenstest
 - [JAVA 9 Grundlagen 1 - Einführung, Programmentwicklung und grundlegende Sprachelemente](https://shop.herdt.com/quiz/jav9/java9e28093grundlagen1/quiz/#/quiz)
 - [JAVA 9 Grundlagen 2 - Kontrollstrukturen, Klassen, Attribute, Methoden](https://shop.herdt.com/quiz/jav9/java9e28093grundlagen2/quiz/#/quiz) 
 