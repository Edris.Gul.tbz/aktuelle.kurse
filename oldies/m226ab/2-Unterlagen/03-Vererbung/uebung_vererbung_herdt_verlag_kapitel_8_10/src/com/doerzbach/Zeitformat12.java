package com.doerzbach;

public class Zeitformat12 extends Zeitformat{

    public Zeitformat12(int hours,int minutes){
        setHours(hours);
        setMinutes(minutes);
    }
    @Override
    public void zeitAusgabe() {
        String pmam;
        int hours;
        if(getHours()>12) {
            pmam = "PM";
            hours=getHours()-12;
        } else {
            pmam = "AM";
            hours=getHours();

        }
        System.out.printf("%02d:%02d %s\n",hours,getMinutes(),pmam);

    }

}
