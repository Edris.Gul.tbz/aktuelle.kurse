package com.doerzbach;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

	// write your code here
        Zeitformat zeit1=new Zeitformat12(22,10);
        Zeitformat zeit2=new Zeitformat24(22, 10);
        Zeitformat zeit3=new Zeitformat12(17,10);
        Zeitformat zeit4=new Zeitformat24(17, 10);
        Zeitformat zeit5=new Zeitformat12(12,0);
        Zeitformat zeit6=new Zeitformat24(12,00);
        /*zeit1.zeitAusgabe();
        zeit2.zeitAusgabe();
        zeit3.zeitAusgabe();
        zeit4.zeitAusgabe();
        zeit5.zeitAusgabe();
        zeit6.zeitAusgabe();*/
        ArrayList<Zeitformat> zeiten=new ArrayList<>();
        zeiten.add(zeit1);
        zeiten.add(zeit2);
        zeiten.add(zeit3);
        zeiten.add(zeit4);
        zeiten.add(zeit5);
        zeiten.add(zeit6);
        for (Zeitformat zeit: zeiten){
            zeit.zeitAusgabe();
        }
        Zeit zeit1clone=zeit1.clone();
        if(zeit1.equals(zeit1clone)){
            System.out.println("zeit1 und zeit2 sind gleich");
        } else {
            System.out.println("zeit1 und zeit2 sind nicht gleich");
        }
    }
}
