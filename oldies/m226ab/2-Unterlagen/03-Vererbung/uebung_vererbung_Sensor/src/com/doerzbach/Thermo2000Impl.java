package com.doerzbach;

public class Thermo2000Impl extends TemperaturSensor{

    @Override
    public String getName() {
        return "Thermo2000";
    }

    @Override
    public void doMeasurement() {
        measurementValue=-50+Math.random()*300;

    }
}
