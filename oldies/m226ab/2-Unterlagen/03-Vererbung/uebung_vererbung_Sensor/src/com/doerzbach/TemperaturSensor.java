package com.doerzbach;

public abstract class TemperaturSensor extends Sensor{
    private String unit="Grad Celsius";
    protected double measurementValue;
    public double getValue(){
        return measurementValue;
    }
    public String getUnit(){
        return unit;
    }
}
