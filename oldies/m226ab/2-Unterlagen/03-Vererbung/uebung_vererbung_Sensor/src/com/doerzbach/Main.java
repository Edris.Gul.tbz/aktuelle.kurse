package com.doerzbach;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        Scanner scanner = new Scanner(System.in);
        int choice;
        do {
            Sensor s = null;
            System.out.println("Choose your Sensor");
            System.out.println("0 EXIT");
            System.out.println("1 Aqualung2021ProDivePressureSensor");
            System.out.println("2 Barometric1000PressureSensor");
            System.out.println("3 TemperaturInBaar");
            System.out.println("4 Thermo2000");
            System.out.printf("Make your choice:");
            choice = scanner.nextInt();

            switch (choice) {
                case 0:
                    break;
                case 1:
                    s = new Aqualung2021ProDivePressureSensorImpl();
                    break;
                case 2:
                    s = new Barometric1000PressureSensorImpl();
                    break;
                case 3:
                    s = new TemperaturInBaarImpl();
                    break;
                case 4:
                    s = new Thermo2000Impl();
                    break;
                default:
                    System.out.println("No valid choice");
            }
            if (s != null) {
                System.out.print("Enter Interval in ms:");
                long interval=scanner.nextLong();
                System.out.print("Enter number of Measurments:");
                int numberOfMeasurements=scanner.nextInt();
                System.out.printf("Create a writer with filename text.csv, interval of %d ms and maximal number of measurements of %d using Sensor %s%n",interval,numberOfMeasurements,s.getClass());
                CsvWriter w = new CsvWriter(interval, "test.csv", s, numberOfMeasurements);
                System.out.println("let it run");
                w.run();
            }
        } while (choice != 0);

        // Now comes the multithreaded version, which is not part of the exercise.
        // Read it if you are interested.
        System.out.println("Create two CsvWriters w1 and w2 with 2 different sensors writing both to the same file test.csv but at different intervals");
        Sensor s1 = new Aqualung2021ProDivePressureSensorImpl();
        CsvWriter w1 = new CsvWriter(30000, "test.csv", s1);
        Sensor s2 = new Thermo2000Impl();
        CsvWriter w2 = new CsvWriter(500, "test.csv", s2);
        System.out.println("Create two threads t1 and t2 if the writes w1 and w2");
        Thread t1 = new Thread(w1); // w1 and w2 implement the Runnable interface which is required for a thread.
        Thread t2 = new Thread(w2);
        System.out.println("Start Threads t1 and t2");
        t1.start(); // This starts the Thread t2 and the Thread is executing run() method of w1
        t2.start(); // This starts the Thread t2 and the Thread is executing run() method of w2
        System.out.println("Wait for 10 seconds now");
        Thread.sleep(10000); // Lets wait for 10 seconds before we continue
        System.out.println("Set the running flag to false using the stop() method of CsvWriter");
        w1.stop(); // This will set the running flag to alse in w1 and w2 and the loop will end...
        w2.stop();
        System.out.println("Now we have to notify all Threads to wake up from syncObject.wait(interval)...");
        synchronized (w1.syncObject) {
            w1.syncObject.notify();
        }
        synchronized (w2.syncObject) {
            w2.syncObject.notify();
        }
        System.out.println("....and join the threads to wait for them to exit");
        t1.join();
        t2.join();
    }
}
