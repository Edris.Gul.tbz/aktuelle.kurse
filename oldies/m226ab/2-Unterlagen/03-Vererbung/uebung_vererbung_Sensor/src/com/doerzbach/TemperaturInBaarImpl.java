package com.doerzbach;

public class TemperaturInBaarImpl extends TemperaturSensor{

    @Override
    public String getName() {
        return "Temperatur in Baar";
    }

    @Override
    public void doMeasurement() {
        measurementValue=0+30*Math.random();
    }
}
