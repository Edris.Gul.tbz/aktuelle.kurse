package com.doerzbach;

public abstract class Sensor {
    public abstract double getValue();
    public abstract String getUnit();
    public abstract String getName();
    public abstract void doMeasurement();
}
