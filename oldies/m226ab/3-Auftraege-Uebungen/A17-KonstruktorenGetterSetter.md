# Konstruktorn, Getter und Setter

Kapitel 7 in [JAVA9, Herdt](../2-Unterlagen/00-Buecher/Buch__Java_9_Grundlagen_Programmierung) 
durcharbeiten.
- Übungen auf Seite 88 lösen.
- als IntelliJ- oder Eclipse-Export angeben.

Achtung: In Punkt 1 steht, dass sie die Klassen Circle und SomeMath aus Kapitel 4 benutzen sollen. Es sollte Kapitel 6 heissen.