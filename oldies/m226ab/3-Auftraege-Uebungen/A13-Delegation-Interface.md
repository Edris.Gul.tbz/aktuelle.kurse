# "Delegation" -> Erweitern der InterfaceDemo Applikation

Zeitbedarf: 30-60 min

**Erweitern der InterfaceDemo Applikation**

Erweitern sie die Applikation durch eine zusaetzliche Klasse Rectangle (Rechteck).
Sie wissen ja, dass ein Quadrat ein Spezialfall eines Rechtecks ist (alle 4 Steiten sind dann gleich lang).

Man könnte auf die Idee kommen, eine Vererbung (extends) zu machen. 
Aber genau das soll jetzt grade nicht passieren. Die Idee ist, dass Sie eine "Delegation" machen.
Dies wird heute mehr als die Vererbung gemacht, weil es flexibler und mächtiger ist.

Das heisst, Sie überarbeiten die Klasse Square so, dass Sie trotzdem noch 
die Methode getArea() von Rectangle benutzen können um die Fläche zu berechnen. 

(Tip: im Square.java muessen sie eine Membervariable "private Rectangle square" 
definieren und diese im Konstruktor initialisieren. Dann koennen sie in getArea() 
von Square nur noch square.getArea() aufrufen. Dem sagt man *_Delegation_*)

Bitte geben sie den Code von den beiden Klassen Square.java und Rectangle.java per Upload oder in einem Textfile ab (copy/paste). 
Das erleichtert mir das korrigieren. Danke

Delegate pattern
11:56 min, E, YouTube, 2015
 https://www.youtube.com/watch?v=Xa04E1upEg0

Replace Inheritance With Delegation
2:25 min, E, YouTube, 2014
 https://www.youtube.com/watch?v=QBJigWDBlGE

	
Tutorial - Delegation In Java and Kotlin
12:54 min, E, YouTube, 2017
 https://www.youtube.com/watch?v=zfiohSIZtbo