# 1 - Wissensaneignung
(Buch Kap.1-4, M. Ruggerio, Compendio)

Zeitbedarf: 3-6 Std
 - Installieren Sie eine IDE wie Eclipse oder IntelliJ (wenn Sie das noch nicht schon haben)
 - Lesen Sie Kap. 1 bis Kap. 4 (S. 50) im Buch__M226_Compendio_LehrbuchTutorialAufgabenstellungen.pdf
 - Bauen Sie die vorkommenden Codebeispiele bei sich in Ihrem System nach
 - Zeichnen Sie die Diagramme in https://draw.io nach und speichern Sie sie in Ihrem OneDrive (und/oder ePortfolio). Wenn Sie eigene Erweiterungen machen, führen Sie sie in Ihren Diagrammen nach.

Erzeugen Sie von allen Ihren Code-Stücken und Grafiken ein PDF (via Word-Datei und Speicherung als PDF) und zeigen Sie es der LP und/oder laden Sie dies in diesen Auftrag vor dem Abgabedatum hoch.


Weitere Informationen zu UML kommen noch.
Über die Grundlagen der Objektorientierung und UML-Diagramme wird es eine mündliche und eine schriftliche Prüfung geben

<https://draw.io>

[Ruggerio, Compendio](../2-Unterlagen/00-Buecher/Buch__M226_Ruggerio_Compendio)