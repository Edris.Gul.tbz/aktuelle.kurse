# Fragen zu "Collections

Arbeiten sie das Kapitel 13 im Buch “Java 9 Grundlagen Programmierung” von Herdt-Campus durch und beantworen sie folgende Fragen:
- Kann ein bestehnder Array vergrössert ohne dass der ganze Inhalt des Arrays umkopiert werden muss?
- Kann zu einem bestehenden Array eine Element dazugefügt werden?
- Kann eine LinkedList vergrössert werden ohne dass der ganze Inhalt der LinkedList umkopiert werden muss?
- Kann zu einer ArrayList ein Element dazugfügt werden?
- Wo im Beispiel der letzen Aufgabe mit dem PrinterController sollte man eine Collection einsetzen?
Antworten in einen doc oder txt file abgeben.

[Java 9 Grundlagen Programmierung](../2-Unterlagen/00-Buecher/Buch__Java_9_Grundlagen_Programmierung)