<!DOCTYPE html>
<html lang="de-AT">
<head>
<title>E-Mail Kopfzeilen verstehen</title>
<meta name="description" content="Dieser Artikel beschreibt, wie die Kopfzeilen einer E-Mail aufgebaut sind und wie diese richtig gelesen und interpretiert werden.">
<meta name="robots" content="index,follow">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="/styles/global.css">
<link rel="shortcut icon" href="/favicon.ico">
<link rel="apple-touch-icon" href="/apple-touch-icon.png">
<script>if(top!=self) top.location=self.location;</script>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
function CookiesOk() {
	SetCookie("Cookies", "ok", 3650);
}
function NoCookies() {
	SetCookie("Cookies", "no", 3650);
}
function SetCookie(name, value, days) {
	var d = new Date();
	d.setTime(d.getTime() + (days * 86400000));
	document.cookie = name + "=" + value + ";expires=" + d.toUTCString() + ";path=/";
}
</script>
<style>
a.cookiebanner_link, a.cookiebanner_link:hover {
	color: #0060c0;
	text-decoration: underline;
}
a.cookiebanner_button {
	background: #0060c0;
	color: #ffffff;
	margin-left: 16px;
	padding: 4px 24px;
}
a.cookiebanner_button:hover {
	background-color: #009000;
	text-decoration: none;
}
</style>
</head>
<body>

<div class="div_table gaijin_logo_bar"><div class="div_row">
<div class="div_cell gaijin_logo"><a href="/de/"><div>Gaijin<span class="gaijin_logo_dot">.</span><span class="gaijin_logo_at">at</span></div></a></div>
<div class="div_cell">&nbsp;</div>
</div></div>

<ul class="menubar" id="menubar">

<li><a href="/de/" title="Startseite" style="height:1.5rem; width:2rem;">
<svg viewbox="0 0 100 85" width="100%" height="100%">
  <polygon points="0,50 50,00 100,50" style="fill:rgb(255,255,255)" />
  <rect x="15" y="45" width="25" height="40" style="fill:rgb(255,255,255);" />
  <rect x="60" y="45" width="25" height="40" style="fill:rgb(255,255,255);" />
</svg>
</a></li>

<li><a href="/de/tools/">Online-Tools</a></li>
<li><a href="/de/infos/">Informationen</a></li>
<li><a href="/de/software/">Software</a></li>
<li><a href="/de/wallpapers/">Hintergrundbilder</a></li>

<li class="menuright"><a href="/en/">English Homepage</a></li>
</ul>

<div class="page">

<div class="page_content">
<h1>E-Mail Kopfzeilen verstehen</h1>

<h3>Inhalt</h3>
<blockquote>
	<a href="#smtp">Der Nachrichtenversand mit SMTP</a><br>
	<a href="#pop3">Der Nachrichtenempfang mit POP3</a><br>
	<a href="#received">Die Received-Zeilen</a><br>
	<a href="#header">Kopfzeilen im Detail</a><br>
</blockquote>



<div class="cont_content" style="margin:32px 0 16px 0; overflow:auto;"><div id="container_content_alt" class="container_alt"><p>Gefällt dir meine Webseite, meine Freeware-Programme oder Online-Tools?</p><p style="margin-top:16px;">Dann <a href="https://paypal.me/gaijinat/?locale.x=de_DE" class="link_external" target="_blank" rel="noopener nofollow" title="Spenden per PayPal">spende bitte per PayPal</a> und hilf mit, den Inhalt weiterhin kostenlos anbieten zu können - jeder Betrag ist willkommen!</p><p style="margin-top:16px;">Erlaube Werbung für die Domain <b>Gaijin.at</b> in deinem Werbeblocker und hilf damit beim Erhalt dieser Seite!</p><p style="margin-top:16px;"><a href="/de/spenden">Lese mehr über Unterstützungs&shy;möglichkeiten...</a></p></div><div id="container_content_ext" style="display:none;"><ins class="adsbygoogle" style="display:block;overflow:hidden;" data-ad-client="ca-pub-4549485846911679" data-ad-slot="9662075536" data-ad-format="auto" data-full-width-responsive="true"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script></div></div>



<h2 id="smtp">Der Nachrichtenversand mit SMTP</h2>

<p>Das Versenden und Empfangen von E-Mails wird normalerweise von E-Mail Clients wie etwa Microsoft Outlook Express oder Mozilla Firebird erledigt. Zum besseren Verständnis ist es jedoch von Vorteil, wenn das Prinzip des Nachrichtenversandes bekannt ist.</p>

<p>Zum Versenden von E-Mails bedienen sich Mail-Programme meist dem Mailserver des eigenen Providers, der dann die weitere Zustellung an den Empfänger übernimmt. Jedoch können Nachrichten auch direkt zugestellt werden. Dazu benötigt man eine DNS-Anfrage vom Typ "MX", welche die verfügbaren MX-Server (Mail eXchange Server) einer Domain liefert. Dafür können spezielle Programme oder auch Online-Dienste verwendet werden.</p>

<p>Im folgenden Beispiel möchte der fiktive Absender "test@tester.tld" eine Nachricht an "noname@example.tld" senden. Dazu wird zuerst der MX-Server der Domain "example.tld" benötigt. Eine DNS-Anfrage ergibt, dass der MX-Server "mail.example.tld" Nachrichten für die Domain "example.tld" entgegen nimmt. Mit einem Terminal-Programm kann nun eine Verbindung zum Host "mail.example.tld", Port 25 (SMTP) hergestellt werden.</p>

<p>Nach dem Aufbau der Verbindung meldet sich der Mailserver mit der Zeile:</p>

<pre class="box_example">
220 server11011.net ESMTP
</pre>

<p>Die Zahl "220" am Beginn der Zeile bedeutet, dass es sich bei dem nachfolgenden Text nur um eine gewöhnliche Meldung handelt und dass kein Fehler aufgetreten ist. Jetzt wartet der SMTP-Server auf eine sog. Vorstellung, welche mit dem Kommando "HELO" bzw. "EHLO" eingeleitet wird.</p>

<pre class="box_example">
<span class="blue">helo faked.tester.tld</span>
</pre>

<p>Mit diesem Kommando stellt sich der Sender mit dem Namen "faked.tester.tld" vor. Der Server bestätigt dies mit dem Statuscode 250, welcher für einen gültigen Befehl steht.</p>

<pre class="box_example">
250 server11011.net
</pre>

<p>Nun folgt die Adressierung der Nachricht, beginnend mit der E-Mail Adresse des Senders, welche mit dem Befehl "mail from:" gekennzeichnet wird.</p>

<pre class="box_example">
<span class="blue">mail from: test@tester.tld</span>
</pre>

<p>Nachdem der SMTP-Server die Adresse des Absenders verifiziert hat und diese als gültig befunden hat, meldet er den Statuscode 250, meist gefolgt vom Text "ok", zurück.</p>

<pre class="box_example">
250 ok
</pre>

<p>Anschließend wird die E-Mail-Adresse des Empfängers angegeben. Diese wird mit dem Befehl "rcpt to:" (engl. für "recipient") eingeleitet.</p>

<pre class="box_example">
<span class="blue">rcpt to: noname@example.tld</span>
</pre>

<p>Auch dies wird vom SMPT-Server bestätigt:</p>

<pre class="box_example">
250 ok
</pre>

<p>Die Adressierung ist nun abgeschlossen und die eigentliche Nachricht kann dem Server übermittelt werden. Dazu wird das Kommando "data" verwendet.</p>

<pre class="box_example">
<span class="blue">data</span>
</pre>

<p>Der SMPT-Server bestätigt den data-Befehl mit dem Statuscode 354, der zur Eingabe des Textes auffordert. Im Anschluss an den Statuscode können Texte wie z.B. "go ahead" oder "Enter mail, end with "." on a line by itself" folgen.</p>

<pre class="box_example">
354 go ahead
</pre>

<p>Keine der oben genannten Angaben müssen auch in der Nachricht enthalten sein, die beim Empfänger eintrifft. Darum werden nun die typischen Kopfzeilen wie "From", "To" und "Subject" einer Nachricht eingegeben. Hier wird zu Beginn auch eine gefälschte Received-Zeile angegeben, die über den wahren Absender täuschen soll. Darauf wird jedoch weiter unten genauer eingegangen.</p>

<p>Nach den Kopfzeilen wird der eigentliche Nachrichtentext, durch eine Leerzeile getrennt, eingegeben.</p>

<pre class="box_example">
<span class="blue">Received: from faked.tester.tld (faked.tester.tld [123.45.67.89])
  by server11011.net with SMTP; 1 Sep 2018 13:53:24 +0200
Message-Id: &lt;1234567890@tester.tld&gt;
X-Sender: test@tester.tld
From: "The Faker" &lt;test@tester.tld&gt;
To: "NoName" &lt;noname@example.tld&gt;
Subject: Ein Test

Text, Text, Text
Zeile 2
Zeile 3
.</span>
</pre>

<p>Der Nachrichtentext wird immer mit einem einzelnen Punkt in einer eigenen Zeile abgeschlossen. Der Server bestätigt den Empfang der Nachricht wieder mit dem Code 250:</p>

<pre class="box_example">
250 ok 1157111913 qp 7263
</pre>

<p>Durch das Kommando "quit" wird die Verbindung zum SMTP-Server geschlossen.</p>

<pre class="box_example">
<span class="blue">quit</span>
</pre>

<p>Bevor der Server die Verbindung beendet gibt er den Statuscode 221 (Bestätigung des quit-Befehls) zurück:</p>

<pre class="box_example">
221 server11011.net
</pre>

<p>Hiermit ist die Verbindung geschlossen und die Nachricht wird im Postfach des Empfängers hinterlegt.</p>

<p>Der komplette oben beschriebene Dialog zwischen Server und Client sieht nun so aus:</p>

<pre class="box_example">
220 server11011.net ESMTP
<span class="blue">helo faked.tester.tld</span>
250 server11011.net
<span class="blue">mail from: test@tester.tld</span>
250 ok
<span class="blue">rcpt to: noname@example.tld</span>
250 ok
<span class="blue">data</span>
354 go ahead
<span class="blue">Received: from faked.tester.tld (faked.tester.tld [123.45.67.89])
  by server11011.net with SMTP; 1 Sep 2018 13:53:24 +0200
Message-Id: &lt;1234567890@tester.tld&gt;
X-Sender: test@tester.tld
From: "The Faker" &lt;test@tester.tld&gt;
To: "NoName" &lt;noname@example.tld&gt;
Subject: Ein Test

Text, Text, Text
Zeile 2
Zeile 3
.</span>
250 ok 1157111913 qp 7263
<span class="blue">quit</span>
221 server11011.net
</pre>



<h2 id="pop3">Der Nachrichtenempfang mit POP3</h2>

<p>Das Empfangen von E-Mails funktioniert ähnlich wie der Versand. Jedoch kommt hier ein eigenes Protokoll, das Post Office Protokoll (POP), zum Einsatz. Der POP3-Server ist meist über den Port 110 erreichbar.</p>

<p>Im Folgenden wird die oben gesendete Nachricht abgerufen:</p>

<pre class="box_example">
+OK Hello there.
<span class="blue">user xxxxxxxx</span>
+OK Password required.
<span class="blue">pass xxxxxxxx</span>
+OK logged in.
<span class="blue">list</span>
+OK
1 598
.
<span class="blue">retr 1</span>
+OK 598 octets follow.
<span class="red">Return-Path: &lt;test@tester.tld&gt;
Received: (qmail 7881 invoked from network); 1 Sep 2018 13:58:33 +0200
Received: from m198p010.dipool.highway.telekom.at
  (HELO tester.tld) (62.46.14.170)
  by server11011.net with SMTP; 1 Sep 2018 13:55:38 +0200</span>
Received: from faked.tester.tld (faked.tester.tld [123.45.67.89])
  by server11011.net with SMTP; 1 Sep 2018 13:53:24 +0200
Message-Id: &lt;1234567890@tester.tld&gt;
X-Sender: test@tester.tld
From: "The Faker" &lt;test@tester.tld&gt;
To: "NoName" &lt;noname@example.tld&gt;
Subject: Ein Test

Text, Text, Text
Zeile 2
Zeile 3
.
<span class="blue">quit</span>
+OK Bye-bye.
</pre>

<p>Der rot markierte Text wurde vom Mailserver eingefügt. Abhängig vom Mailserver und der Daten, die beim Versand der Nachricht angegeben wurden, können sich die Headerzeilen unterscheiden. Die erste bzw. in diesem Fall die erste und zweite Received-Zeile von oben werden jedoch immer vom eigenen Mailserver eingefügt, wodurch diese auf jeden Fall authentisch sind.</p>



<h2 id="received">Die Received-Zeilen</h2>

<p>In unserem Beispiel ergeben sich jetzt folgende Received-Zeilen:</p>

<pre class="box_example">
Received: (qmail 7881 invoked from network); 1 Sep 2018 13:58:33 +0200
Received: from m198p010.dipool.highway.telekom.at
  (HELO tester.tld) (62.46.14.170)
  by server11011.net with SMTP; 1 Sep 2018 13:55:38 +0200
Received: from faked.tester.tld (faked.tester.tld [123.45.67.89])
  by server11011.net with SMTP; 1 Sep 2018 13:53:24 +0200
</pre>

<p>Received-Zeilen sind Zustellvermerke der Mailserver, über die die Nachricht gesendet wurde. Ein Mailserver fügt die Received-Zeile immer oben an die Nachricht an. Daraus ergibt sich, dass der Received-Eintrag des ersten Mailservers unten und der Eintrag des letzten Mailservers, über den die Nachricht transportiert wurde, oben steht.</p>

<pre class="box_example">
Received: (qmail 7881 invoked from network); 1 Sep 2018 13:58:33 +0200
</pre>

<p>Diese Received-Zeile kommt recht häufig in Nachrichten vor, es ist ein Vermerk des Mailservers "QMail". Diese Zeile hat nur bedingt Bedeutung für eine Rückverfolgung des Absenders.</p>

<pre class="box_example">
Received: from m198p010.dipool.highway.telekom.at
  (HELO tester.tld) (62.46.14.170)
  by server11011.net with SMTP; 1 Sep 2018 13:55:38 +0200
</pre>

<p>Diese Zeile wurde, so wie die erste auch, vom Mailserver des Empfängers eingefügt und ist somit eine gute Ausgangsbasis für eine Rückverfolgung. Der Aufbau einer Received-Zeile ist jedoch nicht an feste Richtlinien gebunden, weshalb man alle Angaben überprüfen sollte. Sehen wir uns die einzelnen Teile genauer an:</p>

<pre class="box_example">
from m198p010.dipool.highway.telekom.at
</pre>

<p>Unter "from" kann einerseits der richtige Hostname des Absenders stehen, so wie in diesem Fall. Aber auch der Computername oder Hostname, wie dieser unter "HELO" angegeben wurde, ist möglich.</p>
 
<pre class="box_example">
(HELO tester.tld)
</pre>

<p>Nach "HELO" wird jener Text angegeben, der bei der Begrüßung beim Nachrichtenversand festgelegt wurde. Diese Angabe muss jedoch nicht immer enthalten sein.</p>

<pre class="box_example">
(62.46.14.170)
</pre>

<p>Am Ende des Absenders (jedoch vor "by ...") steht in runden Klammern der tatsächliche Absender. Diese IP-Adresse hat die Verbindung zum Mailserver hergestellt und die E-Mail übermittelt. Dies bedeutet, dass die IP-Adresse 62.46.14.170 zum Zeitpunkt des Versandes den Hostnamen "m198p010.dipool.highway.telekom.at" hatte. Da dieser mit dem Hostnamen nach "from ..." identisch ist, wurde er nicht nochmals angeführt. Auch die folgende Angabe ist hier möglich:</p>

<pre class="box_example">
(m198p010.dipool.highway.telekom.at [62.46.14.170])
</pre>

<p>Es gibt keine einheitliche Form der Darstellung, weshalb alle Hostnamen mit der IP-Adresse überprüft werden sollten.</p>

<pre class="box_example">
by server11011.net with SMTP
</pre>

<p>Daraus ist ersichtlich, dass der Server "server11011.net" die Nachricht entgegengenommen hat. Das verwendete Übertragungsprotokoll war SMTP.</p>

<pre class="box_example">
1 Sep 2018 13:55:38 +0200
</pre>

<p>Durch einen Strichpunkt getrennt folgt am Ende immer das Datum und die Zeit des Einganges beim Mailserver. Das Format der Zeitangabe kann, trotzt Standardisierung unterschiedlich sein. In unserem Fall wurde die E-Mail am 1. September 2018, um 13:55:38 vom Mailserver entgegengenommen. Die Zeitangaben in den Received-Zeilen sind immer in der Lokalzeit des Mailservers angegeben. Um die UTC-Zeit bestimmen zu können, wird die Abweichung zur UTC angefügt. In diesem Fall ist es die Angabe "+0200", was bedeutet, dass die angeführte Zeit eine Abweichung von plus 2 Stunden zur UTC hat. Somit wurde die Nachricht am 01.09.2018, um 11:55:38 UTC entgegengenommen.</p>

<p>Soweit die Einträge des Mailservers. Nun folgen die vom Absender angegebenen und teilweise auch gefälschten Headerzeilen:</p>

<pre class="box_example">
Received: from faked.tester.tld (faked.tester.tld [123.45.67.89])
  by server11011.net with SMTP; 1 Sep 2018 13:53:24 +0200
</pre>

<p>Wie bereits angedeutet, kann die Nachricht auch über mehrere Mailserver laufen, wobei die Received-Zeile eines Mailservers immer oben angefügt wird. In jeder Received-Zeile ist sowohl der Absender ("from...") als auch der Empfänger ("by...") enthalten. Daraus ergibt sich, dass der Versender der ersten Zeile und der Empfänger der zweiten Zeile identisch oder zumindest ähnlich sein müssen. In unserem Beispiel ist dies jedoch nicht der Fall:</p>

<pre class="box_example">
Received: <span class="red">from m198p010.dipool.highway.telekom.at</span> (62.46.14.170)
          by server11011.net

Received: from faked.tester.tld (faked.tester.tld [123.45.67.89])
          <span class="red">by server11011.net</span>
</pre>

<p>Ein plausibler Verlauf der Nachricht könnte etwa so aussehen (vereinfachte Darstellung):</p>

<pre class="box_example">
Received: <span class="blue">from mail.target.tld</span> (12.34.56.78)
          by mx.target.tld

Received: <span class="green">from mx3.provider1.tld</span> (123.45.67.3)
          <span class="blue">by target.tld</span>

Received: <span class="red">from mx1.provider1.tld</span> (123.45.67.1)
          <span class="green">by mx3.provider1.tld</span>

Received: from user1.provider1.tld (123.45.67.89)
          <span class="red">by mx1.provider1.tld</span>
</pre>



<h2 id="header">Kopfzeilen im Detail</h2>

<p>Die nachfolgende Liste enthält nur die wesentlichsten Kopfzeilen. Je nach verwendetem Mailprogramm, Mailserver oder auch bei Mailinglisten können noch weitere Zeilen vorkommen.</p>

<h3>Bcc</h3><blockquote><p><code>Bcc: Webmaster &lt;webmaster@domain.tld&gt;</code></p><p>Das Header-Feld "Bcc" (Blind Carbon Copy) enthält zusätzliche Empfänger, die die Nachricht als "unsichtbare Kopie" erhalten sollen. Im BCC-Feld werden jene zusätzlichen Empfänger eingetragen, die die anderen Empfänger nicht sehen sollen. Die Empfänger in der To- und CC-Zeile sind für alle Empfänger sichtbar.</p></blockquote>
<h3>Cc</h3><blockquote><p><code>Cc: Name &lt;name@domain.tld&gt;</code></p><p>Das Header-Feld "Cc" (Carbon Copy) enthält zusätzliche Empfänger, die die Nachricht in "Kopie" erhalten sollen. Diese Angabe ist nur für administrative Zwecke vorgesehen. Die E-Mail wird jedoch so zugestellt, als ob die Adressen im To-Feld enthalten wären.</p></blockquote>
<h3>Content-Language</h3><blockquote><p><code>Content-Language: de-AT, en-US</code></p><p>Das Header-Feld "Content-Language" enthält einen Code für die im MIME Body-Part verwendete Sprache, z.B. "de" für Deutsch. Es können auch mehrere Sprachen, mit einem Komma getrennt, angegeben werden. Ein MIME Body-Part kann beispielsweise eine Anlage wie ein Bild oder ein Dokument enthalten, oder auch die Text- bzw. HTML-Version der Nachricht.</p></blockquote>
<h3>Content-Transfer-Encoding</h3><blockquote><p><code>Content-Transfer-Encoding: quoted-printable</code></p><p>Das Header-Feld "Content-Transfer-Encoding" gibt an, wie der Nachrichtentext oder der MIME Body-Part kodiert ist. Übliche Angaben sind "quoted-printable", "base64", "7bit" und "8bit". Ein MIME Body-Part kann beispielsweise eine Anlage wie ein Bild oder ein Dokument enthalten, oder auch die Text- bzw. HTML-Version der Nachricht.</p></blockquote>
<h3>Content-Type</h3><blockquote><p><code>Content-Type: text/plain; charset=ISO-8859-1</code></p><p>Das Header-Feld "Content-Type" gibt den Typ der Nachricht oder des MIME Body-Parts an. Gebräuchlich sind beispielsweise "text/plain" für Textnachrichten oder "text/html" für HTML-Nachrichten. Hinter "charset" folgt der verwendete Zeichensatz. Ein MIME Body-Part kann beispielsweise eine Anlage wie ein Bild oder ein Dokument enthalten, oder auch die Text- bzw. HTML-Version der Nachricht.</p></blockquote>
<h3>Date</h3><blockquote><p><code>Date: Wed, 6 Sept 2018 10:02:47 +0000</code></p><p>Das Header-Feld "Date" enthält die Erstellungszeit bzw. die Zeit des Versendens. Diese Zeitangabe wird vom Mail-Programm des Versenders eingefügt. Die Uhrzeit ist in der Lokalzeit des versendenden Rechners angegeben. Nach der Zeit folgt entweder die Abweichung zur UTC (z.B. "+0200" für +2 Stunden), oder in manchen Fällen auch die Zeitzone.</p></blockquote>
<h3>From</h3><blockquote><p><code>From: Name &lt;name@domain.tld&gt;</code></p><p>Das Header-Feld "From" enthält die Mail-Adresse und eventuell auch den Namen des Absenders. Diese Angabe wird vom Mail-Programm des Versenders eingefügt.</p></blockquote>
<h3>In-Reply-To</h3><blockquote><p><code>In-Reply-To: webmaster@domain.tld</code></p><p>Das Header-Feld "In-Reply-To" enthält bei einer Antwort die Message-ID der beantworteten Nachricht.</p></blockquote>
<h3>Message-ID</h3><blockquote><p><code>Message-ID: &lt;DCEA2913.6003005@domain.tld&gt;</code></p><p>Das Header-Feld "Message-ID" enthält die eindeutige Kennung der Nachricht. Diese Kennung wird vom Mailprogramm, oft aus dem Datum, generiert. Sollte die Message-ID fehlen, wird sie meist von einem Mailserver erstellt und eingefügt.</p></blockquote>
<h3>Organization</h3><blockquote><p><code>Organization: Example Company Ltd.</code></p><p>Das Header-Feld "Organization" gibt die Organisation oder den Firmennamen des Absenders an. Die Organisation kann im Mail-Programm eingestellt werden.</p></blockquote>
<h3>Received</h3><blockquote><p><code>Received: from sender.tld (sender.tld [10.1.2.3]) by server.tld with SMTP; 4 Oct 2019 15:40:53 +0200</code></p><p>Das Header-Feld "Received" enthält Informationen über den Absender, den Empfänger und die Empfangszeit einer Nachricht. Dieses Header-Feld wird vom empfangenden Mailserver als oberste Zeile hinzugefügt. Je nach Anzahl der beteiligten Mailserver, können mehrere "Received"-Felder enthalten sein.</p></blockquote>
<h3>References</h3><blockquote><p><code>References: &lt;DCEA2913.6003005@domain.tld&gt;</code></p><p>Das Header-Feld "References" enthält die Message-IDs von zusammengehörigen Nachrichten. Bei einer Antwort wäre dies die Message-ID der beantworteten Nachricht. Diese Angabe wird von Mailprogrammen verwendet, um die Nachrichten zu sortieren oder um sie hierarchisch darzustellen.</p></blockquote>
<h3>Reply-To</h3><blockquote><p><code>Reply-To: mail@domain.tld</code></p><p>Das Header-Feld "Reply-To" kann eine alternative Antwortadresse enthalten, falls die Antwort nicht an die Absenderadresse (From) gesendet werden soll.</p></blockquote>
<h3>Return-Path</h3><blockquote><p><code>Return-Path: name@domain.tld</code></p><p>Das Header-Feld "Return-Path" enthält die Rücksendeadresse, falls eine Zustellung der Nachricht nicht möglich war. Zu der ursprünglichen E-Mail wird vom Mailserver, der die Nachricht nicht zustellen konnte, eine Fehlermeldung angefügt. Der Return-Path kann auch von einem Mailserver auf Grund der Angabe unter "MAIL FROM" nachgetragen werden.</p></blockquote>
<h3>Subject</h3><blockquote><p><code>Subject: Example subject</code></p><p>Das Header-Feld "Subject" enthält den Betreff der Nachricht.</p></blockquote>
<h3>To</h3><blockquote><p><code>To: &quot;Webmaster&quot; &lt;webmaster@domain.tld&gt;, name@domain.tld, support@domain.tld (Support Center)</code></p><p>Das Header-Feld "To" enthält den oder die Empfänger. Bei mehreren Adressen müssen diese durch einen Beistrich getrennt sein. Die Reihenfolge der Adressen und Namen kann variieren.</p></blockquote>
<h3>X-Mailer</h3><blockquote><p><code>X-Mailer: ExampleMailer 1.23</code></p><p>Das Header-Feld "X-Mailer" enthält Informationen zur Software, die zum Versenden der Nachricht benutzt wurde. Übliche Angaben sind der Name und die Versionsnummer der Software, aber auch der URL oder sonstige Angaben sind möglich.</p></blockquote>
<h3>X-Priority</h3><blockquote><p><code>X-Priority: 3 (Normal)</code></p><p>Das Header-Feld "X-Priority" gibt die Priorität der Nachricht an. Der Wert hat keinen Einfluss auf die Geschwindigkeit des Versands. Mögliche Werte sind: 1 (Sehr hoch), 2 (Hoch), 3 (Normal), 4 (Niedrig), 5 (Sehr niedrig). Manchmal ist nur die Zahl angegeben. Dieses Header-Feld wird in Spam mit einem hohen Prioritätswert verwendet.</p></blockquote>



<div class="cont_bottom" style="margin:32px 0 16px 0; overflow:auto;"><div id="container_bottom_alt" class="container_alt"><p>Gefällt dir meine Webseite, meine Freeware-Programme oder Online-Tools?</p><p style="margin-top:16px;">Dann <a href="https://paypal.me/gaijinat/?locale.x=de_DE" class="link_external" target="_blank" rel="noopener nofollow" title="Spenden per PayPal">spende bitte per PayPal</a> und hilf mit, den Inhalt weiterhin kostenlos anbieten zu können - jeder Betrag ist willkommen!</p><p style="margin-top:16px;">Erlaube Werbung für die Domain <b>Gaijin.at</b> in deinem Werbeblocker und hilf damit beim Erhalt dieser Seite!</p><p style="margin-top:16px;"><a href="/de/spenden">Lese mehr über Unterstützungs&shy;möglichkeiten...</a></p></div><div id="container_bottom_ext" style="display:none;"><ins class="adsbygoogle" style="display:block;overflow:hidden;" data-ad-client="ca-pub-4549485846911679" data-ad-slot="0383745348" data-ad-format="auto" data-full-width-responsive="true"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script></div></div>
</div>

<div class="sidebar">

<div class="sidebar_title"><a href="/de/infos/">Informationen</a></div>
<div class="sidebar_content">

<div class="sidebar_subtitle sidebar_subtitlefirst">Artikel und Anleitungen</div>
<p><a href="/de/infos/alternative-datenstroeme">Alternative Datenströme (ADS)</a></p>
<p><a href="/de/infos/bios-setup">BIOS-Setup aufrufen</a></p>
<p><a href="/de/infos/daten-und-dateien-verbergen">Daten und Dateien verbergen</a></p>
<p><a href="/de/infos/digitale-fussspuren">Digitale Fußspuren</a></p>
<p><a href="/de/infos/e-mail-kopfzeilen-ermitteln">E-Mail Kopfzeilen ermitteln</a></p>
<p><a href="/de/infos/e-mail-kopfzeilen-verstehen">E-Mail Kopfzeilen verstehen</a></p>
<p><a href="/de/infos/google-suchausdruecke">Google Suchausdrücke</a></p>
<p><a href="/de/infos/passwortsicherheit">Passwortsicherheit</a></p>
<p><a href="/de/infos/spam-mails">Spam-Mails</a></p>
<p><a href="/de/infos/whois-ripe">WHOIS (RIPE)</a></p>
<p><a href="/de/infos/windows-batchprogramme">Windows Batchprogramme</a></p>

<div class="sidebar_subtitle">Listen und Tabellen</div>
<p><a href="/de/infos/ascii-ansi-zeichentabelle">ASCII und ANSI Zeichentabelle</a></p>
<p><a href="/de/infos/dateinamenerweiterungen-buchstabe-a">Dateinamenerweiterungen</a></p>
<p><a href="/de/infos/e-mail-kopfzeilen">E-Mail Kopfzeilen</a></p>
<p><a href="/de/infos/farbtabellen">Farbtabellen</a></p>
<p><a href="/de/infos/http-statuscodes">HTTP-Statuscodes</a></p>
<p><a href="/de/infos/linux-befehle">Linux-Befehle</a></p>
<p><a href="/de/infos/portnummern">Portnummern</a></p>
<p><a href="/de/infos/top-level-domains">Top-Level-Domains (TLDs)</a></p>
<p class="sidebar_subsubtitle"><a href="/de/infos/unicode-zeichentabelle">Unicode Zeichentabellen (Inhalt)</a></p>
<blockquote class="sidebar_indent">
	<p><a href="/de/infos/unicode-zeichentabelle-latin">Lateinische Schriftzeichen</a></p>
	<p><a href="/de/infos/unicode-zeichentabelle-geometrie">Geometrie und Mathematik</a></p>
	<p><a href="/de/infos/unicode-zeichentabelle-dingbats">Dingbats, Symbole und Pfeile</a></p>
	<p><a href="/de/infos/unicode-zeichentabelle-piktogramme-1">Piktogramme</a></p>
</blockquote>
<p><a href="/de/infos/windows-versionsnummern">Windows-Versionsnummern</a></p>
<p><a href="/de/infos/zeitzonen">Zeitzonen</a></p>

</div>

<div id="container_sidebar_ext" style="display:none; margin-top:32px;"><ins class="adsbygoogle" style="display:block;overflow:hidden;" data-ad-client="ca-pub-4549485846911679" data-ad-slot="6569008336" data-ad-format="auto" data-full-width-responsive="true"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script></div><div class="sidebar_title" style="margin-top:32px;"><a href="/de/spenden">Spenden und Unterstützung</a></div>
<div class="sidebar_content"><p>Gefällt dir meine Webseite, meine Freeware-Programme oder Online-Tools?</p><p style="margin-top:16px;">Dann <a href="https://paypal.me/gaijinat/?locale.x=de_DE" class="link_external" target="_blank" rel="noopener nofollow" title="Spenden per PayPal">spende bitte per PayPal</a> und hilf mit, den Inhalt weiterhin kostenlos anbieten zu können - jeder Betrag ist willkommen!</p><p style="margin-top:16px;" id="container_sidebar_alt_ext">Erlaube Werbung für die Domain <b>Gaijin.at</b> in deinem Werbeblocker und hilf damit beim Erhalt dieser Seite!</p><p style="margin-top:16px;"><a href="/de/spenden">Lese mehr über Unterstützungs&shy;möglichkeiten...</a></p></div>

</div>

</div>

<div class="bottombar">

<div class="bottombar_links">

<a href="/de/impressum">Impressum</a>
<a href="/de/datenschutz">Datenschutzerklärung</a>
<a href="/de/nutzungsbedingungen">Nutzungsbedingungen</a>
<a href="/de/spenden">Spenden und Unterstützen</a>
<a href="mailto:web@gaijin.at?subject=Feedback%20(%2Fde%2Finfos%2Fe-mail-kopfzeilen-verstehen)">Feedback senden</a>
<a href="/de/dateien">Datei-Verzeichnis</a>

<a href="/en/"><b>English Homepage</b></a>
</div>

<div class="bottombar_copyright">
Copyright &copy; 2003-2022 Gaijin.at. Alle Rechte vorbehalten.
</div>

<br><br><br><br>

</div>

<div id="cookiebanner" style="position:fixed; bottom:0; left:0; width:100%; background-color:#d0d0d0;">
<p style="line-height:1.5rem; text-align:center; margin:0; padding:8px; font-size:.8rem;">Diese Webseite verwendet Cookies und verarbeitet Daten. Informationen zur Datenverarbeitung sowie zur Möglichkeit, diese abzulehnen, finden Sie in der <a href="/de/datenschutz" class="cookiebanner_link">Datenschutzerklärung</a>. <a href="" onclick="CookiesOk()" class="cookiebanner_button">Ok</a></p>
</div>

<script>ABD_F=true;</script>
<script src="/advertisings.js"></script>
<script>
if(!ABD_F){
e=document.getElementById("container_content_alt");if(e!=null){e.style.display="none";}
e=document.getElementById("container_content_ext");if(e!=null){e.style.display="block";}
e=document.getElementById("container_bottom_alt");if(e!=null){e.style.display="none";}
e=document.getElementById("container_bottom_ext");if(e!=null){e.style.display="block";}
e=document.getElementById("container_sidebar_alt_ext");if(e!=null){e.style.display="none";}
e=document.getElementById("container_sidebar_ext");if(e!=null){e.style.display="block";}
}
</script>

</body>
</html>