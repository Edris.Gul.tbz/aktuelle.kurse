## UML-Diagramme


[OOP_UML-Diagrammtypen-mit-Uebungen_V2.0.pdf](./OOP_UML-Diagrammtypen-mit-Uebungen_V2.0.pdf)


**Videos**

- [09:55 min, e, YouTube: 00 UML Tutorial - UseCase, Activity, Class and Sequence Diagrams - Essential Software Modeling](https://www.youtube.com/watch?v=RMuMz5hQMf4)
- [13:23 min, e, YouTube: 01 UML UseCase Diagram Tutorial, "Lucidchart"](https://www.youtube.com/watch?v=zid-MVo7M-E&t=292s)
- [15:06 min, d, YouTube: 02 UML UseCase- Anwendungsfall-Diagramme, "nerdwest"](https://www.youtube.com/watch?v=YY6ozKlp0B4)
- [15:06 min, d, YouTube: 03 UML Klassendiagramm, "nerdwest"](https://www.youtube.com/watch?v=YY6ozKlp0B4)
- [07:31 min, d, YouTube: 03 UML Klassendiagramm, "TheSimpleInformatics"](https://www.youtube.com/watch?v=zzwUH3vbNkc)
- [04:08 min, d, YouTube: 04 UML Objektdiagramm, "TheSimpleInformatics"](https://www.youtube.com/watch?v=oaykwg2_TG8)
- [06:32 min, d, YouTube: 05 UML Assoziationen 1, "TheSimpleInformatics"](https://www.youtube.com/watch?v=sTM0EjAit7c)
- [05:19 min, d, YouTube: 05 UML Assoziationen 2, "TheSimpleInformatics](https://www.youtube.com/watch?v=Z6CrqPukPkY)
- [09:25 min, e, YouTube: 06 UML Sequence Diagram](https://www.youtube.com/watch?v=4WDbte6cPa8)

