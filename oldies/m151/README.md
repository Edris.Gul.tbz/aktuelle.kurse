# M151 - Datenbanken in Web-Applikation einbinden

[> **Modulidentifikation** ](https://www.modulbaukasten.ch/module/151/3/de-DE?title=Datenbanken-in-Web-Applikation-einbinden)


[TOC]


<br>
<br>

## Ablaufplan <mark>AP20b</mark> (Dienstag morgens)
| Tag  | Datum | Thema                         |
| ---- | ----- | -----                         |
|  -1- | 15.11.| Einführung und Start LB1      |
|  -2- | 22.11.| Arbeit an LB1                 |
|  -3- | 29.11.| Arbeit an LB1                 |
|  -4- | 06.12.| Arbeit an LB1                 |
|  -5- | 13.12.| Arbeit an LB1                 |
|  -6- | 20.12.| Abgabe LB1 (Erklärdokument) <br><br> LB2 (schriftlich, 90 min)  <br><br> Start LB3  |
|  --- |  ---  | Weihnachtsferien              |
|  -7- | 10.01.| Arbeit an LB3                 |
|  -8- | 17.01.| Arbeit an LB3 (4 Prüfungen / Debatte hybrides MVC)                |
|  -9- | 24.01.| Arbeit an LB3 (Abschluss)     |
| -10- | 31.01.| Abgabe LB3 (einzeln)          |

<br>
<br>

<mark>
**Grundsätzlich sind in diesem Modul Einzelarbeiten zu machen. Das heisst, dass jede Person (seine/ihre) eigene Abgabe macht und auch selber/einzeln abgibt. Gemeinsames Recherchieren und der Austausch von Techniken und Erkenntnisse sind, ausser bei der LB2 natürlich, explizit erwünscht.**
</mark>
<br>
<br>


## LB1 (30%, Erklär-Dokument)
Als ersten Kompetenznachweis ist ein Erklärdokument, ähnlich eines Lehrbuchs 
oder gar eine kleine wissenschaftliche Arbeit, zu erstellen. Sie erarbeiten selbstständig Themen, die folgend vorgegeben sind. Die Themen werden in einem Dokument behandelt, in dem sie zeigen, dass die die Inhalte vertieft verstanden haben. 

### Formale Kriterien
- Titelblatt mit ihrem Namen, Auftrag und Datum
- Header und Footer
- Inhaltsverzeichnis
- Referenzen und Quellenangaben. Sie müssen die Quellen und Zitat jeweils im Dokument ausweisen
- Rechtschreibung und ganze Sätze
- Pünktliche Abgabe und richtiges Dateiformat & richtiger Dateiname
- Abgabe als PDF mit Dateiname: **M151-LB1-Erklaerdokument-NACHNAME.pdf**

### Inhalt
Sie zeigen in ihrem Text, dass sie die Themen verstanden haben, indem Sie 
- den/die Grundgedanken klar und mit eigenen Worten beschreiben.
- die Themen korrekt und richtig beschreiben
- zeigen, dass Sie weitergehen können, als nur die nachfolgenden Leitfragen zu beantworten (z.B. Alternativen aufzeigen, etc.)

Folgende Themen werden erwartet:

- 1 "DESIGN"
    - wie sieht eine Multi-Tier-Architektur aus (Konzept)
    - welche Varianten gibt es (2-Tier, 3-Tier, 4-Tier, ...) und wie sehen die aus
    - was ist und wie sieht eine MVC-Architektur aus (Konzept)
    - was sind Layers und welche Verbindungen existieren
    - warum macht man das und was sind die Vor- und Nachteile
    - Gibt es eine hybride Form?
    - Wie sieht eine Architektur aus, in der mehr als ein Typ, mehr als ein Datenbankenhersteller parallel verwendet werden kann

- 2 "SICHERHEIT"
    - Verweis auf Modul 183, bzw. Sie erstellen eine Zusammenfassung davon. 
    <br>Speziell interessiert, wie Sie das System
    <br> - save gegen SQL-Injektion
    <br> - save gegen HTML- und andere Manipulationen
    <br> - save gegen Cross-Site-Scripting
	<br> - save gegen "schlechte"/"bösartige" Benutzereingaben
	<br> - save gegen ...
    <br>machen.
	<br>Erklären Sie auch, was man machen muss, damit keine "unsauberen" Eingaben in die Datenbank gelangen.

- 3 "REALISIERUNG"
    - Sie erklären was der konzeptionelle, der logische und der physische Datenbank-Design ist,
    - deren Nutzen, Hauptaufgabe und die Resultate aus diesen Phasen.   
    - Sie erklären, wie man vom konzeptionellen zum logischen und zum physisches DB-Design kommt, und zwar letzteres ohne Generator-Tools zu benutzen.
    - Sie zeigen (u.a. mit Screen-Shots), wie sie auf der Datenbank den entsprechenden (Hilfs-)Client benutzen und wie Sie dort 
    <br> - Queries absetzen können 
    <br> - DB-Transaktionen anwenden können (und in welchen Fällen ist dies sinnvoll) 
    <br> - Stored Procedures, Functions oder ähnliches umsetzen können (und in welchen Fällen ist dies sinnvoll) 
    <br> - - -> Geben sie konkrete Beispiele und belegen Sie ihre Beispielabfragen mit **Screenshots**.
    - Sie beschreiben die wichtigsten Eigenschaften der SQL, also der DDL, der DML und der DCL, allenfalls auch TCL
    - Sie beschreiben, wie man Views macht (auch materialisierte Views) und wie man die verwenden kann und warum man das so macht / machen kann
	- Wie muss eine DB im/mittels Code verbunden werden?  (Code-Schnipsel)
	- Wie kann man SQL-Statements zur DB "bringen"?  (Code-Schnipsel)
	- Wie können die zurückkommenden Resultate oder Daten ausgewertet und/oder dargestellt werden?  (Code-Schnipsel)

- 4 "CHANGE"(-management)
    - Wie machen Sie in der laufenden Entwicklung die Erweiterungen des DB-Modells
    - Wie macht man die Einführung von Relesases im laufenden (produktiven) Betrieb
    - Wie organisieren Sie Versionen und die zusammenarbeit im Team
    <br> d.h. die Strategie der Branches in/mit Git, wie können Fixes und Weiterentwicklungen durchgeführt werden, ohne dass Sie den aktuellen Live-Code unterbrechen/stören 

- 5 "TESTING"
    - Wie testet man während der Entwicklung und wie bei der Entwicklung im laufenden (produktiven) Betrieb
    - Welche Testebenen gibt es?
	  <br>	(Dev-T, UserAcceptance-T, ProdInstall-T)
	- Einsatz von automatischer Tests. Fürs Frontend, fürs Backend.
	- Was ist, bzw. wie macht man CI/CD (continous integration / deployment)
    - Erklären Sie welche Tests für die Datenbankanbindung relevant sind (Load-Tests, Performace-Tests, ...). 
	<br>Sie sollen dabei eine kurze Beschreibung der beiden Tests liefern und dann auf die Relevanz eingehen. Für beide Tests führen sie zusätzlich aus, was für Massnahmen möglich sind, wenn die Tests nicht erfolgreich verlaufen.



### Abgabetermin 
ist der **Tag 5 oder 6** 08:15 (vor der LB2)

### Bewertungskriterien:
 - 33% Form (Titelblatt, Autor, Datum, Header u/o Footer, Inhaltsverzeichnis (bei > 3 Seiten), Nummerierte Kapitel, Referenzen / Links zur Herkunft						
   Aufbau, Gliederung und Ausgeglichenheit der Kapitel		
 - 34% Inhalt (pro Kapitel), Kerngehalt, Richtigkeit, Vollständigkeit, Auswahl und Relevanz (+/-)							
 - 33% Erklärlichkeit (wie erklärt wird), Sprache (ganze Sätze, Orth. & Gram.), Bilder-Zumischung		


### Empfehlung
Es empfiehlt sich, die Themen im Erklärdokument so zu beschreiben, dass zugleich auch eine gute Vorbereitung für die schriftliche Prüfung **LB2** gemacht wird. Es empfiehlt sich ebenso, die thematisierten Techniken auch selber auszuprobieren und zu dokumentieren, damit sie dabei auch gleichzeitig diese Teile in der praktischen Übung **LB3** verwendet und angewendet werden können.

### Tipps zum Vorgehen:
- forschen Sie in ihren früheren Schul-Unterlagen und im Internet (z.B. ITIL, Modul 133)
- schliessen Sie sich mit anderen Lernenden zusammen und tauschen Sie sich darüber aus, 
  wie sie es im Betrieb jetzt schon machen
- "hören" Sie sich  z.B. beim zuständigen Profi in Ihrem Betrieb oder in anderen Betrieben um
- organisieren Sie z.B. eine Telefonkonferenz mit einem Profi (insbesondere für -4- und -5- )
- machen Sie Ihre eigenen Überlegungen
- diskutieren Sie mit Ihrer Lehrperson


<br>
<br>

## LB2 (25%, schriftliche Prüfung, 90 min) 
Am **Tag 5** findet zu Beginn des Halbtages eine schriftliche Prüfung statt. 

Gut für die Vorbereitung ist, wenn im Erklärdokument die Themen schon eingehend behandelt worden sind.

Folgende Themen weden geprüft:
- Multi-Tier-Architektur(en)
- Design-Phasen von Applikation und Datenbanken
- DB-Entwicklungsprozess ERM/ERD, SQL, DDL, DML, DCL, Tabellen, Views
- Ausgewählte PHP-Code-Stücke (z.B. zur DB-Anbindung)

[Weitere Details](./weitere_details_zur_schriftlichen_pruefung.txt)
<br>
<br>

## LB3 (45%, praktisches Projekt) 
Für die praktische Übung ist eine Verwaltungs-WebApp in PHP oder ähnlichem mit 2 verschiedenen Datenbanken zu erstellen. Es kann dafür ein eigenes Thema (Sneaker- oder TShirt-Shop, Essenslieferdienst, Sporteventplanung, Ticket-Shop usw.) gewählt/gemacht werden.


Es wird verlangt, dass eine datenbasierte Software, mit mind. 12 Elementen/Artikeln in der DB mit Bestellmöglichkeit bis Rechnungstellung (ohne Bezahlmechanismus) gebaut wird. Zentral dabei ist, dass mindestens 2 unterschiedliche Daten-Haltungstechniken (Datenbank & Filedatenhaltung) angewendet werden.
 
 
#### Hosting mit Datenbanken
Mögliche / empfohlene Gratishosts mit Datenbanken:

<https://www.bplaced.net/> --> Angebot "freeStyle"
<br><https://infinityfree.net/>
<br><https://www.heroku.com/>
<br>Wer selber einen eigenen Host(-Anbieter) hat, oder ein Account vom Betrieb benutzen kann, darf das natürlich darauf machen.

Eine Abgabe auf "localhost", also nicht über das Internet auf einem Webserver, gibt Abzüge bei der Bewertung.

### Grundanforderung (Note bis 4.7)
- Dynamisch generierte Website (z.B. in PHP) mit 2 Datenbankanbindungen (z.B. für Artikel) und 1 Filedatenhaltung (z.B. für DB-Einstellungen oder für User) programmiert.
- Statischer DB-Switch (hart codiert, Applikation muss neu gestartet werden um auf die 2. DB zu kommen)
- Git wird benutzt
- Abgabe auf einem online Web-System 
	-- eigene Domain, 
	-- oder Subdomain bei einem Provider 
	-- oder als Unterordner in einer Domain
	> Abgabe "nur" auf dem eigenen Rechner (localhost) gibt Abzüge
	
- Einzelabgabe (Es kann zusammen gearbeitet werden, aber jede:r gibt den eigenen Code ab)
- Teile aus Modul M133 können wiederverwendet werden. Es muss aber eine sichtliche Weiterentwicklung davon sein.
- Datenbank angebunden
- MVC konsequent umgesetzt
- Sicherheitsaspekte berücksichtigt
	-- "save" gegen SQL-Injection, Benutzer-Fehleingaben, ...)
	-- "save" gegen URL-Klau (Sessionhandling)
	
- Mehrere Artikel können ausgewählt und/oder abgewählt und danach "bestellt" werden.
 


#### Adminbereich
 -	Verschlüsseltes Login mit Sessionüberprüfung
 -	Benutzerverwaltung mit verschiedenen Rechten
 -	Online-Administration der Websiteinhalte
 -	Formularüberprüfungen

#### Userbereich
 -	Navigation funktionsgerecht
 -	Sortieren möglich
 -	Zeitgerechte Usability (responsive, Steuer- und Anzeigeelemente). Dabei können Sie gerne HTML5/CSS3-Templates verwenden.

### Erweiterte Anforderungen (Note über 4.75)
- Mehr als eine Datenbank gleichzeitig implementiert. Dabei müssen die beiden Datenbanken so umgeschaltet werden können, ohne dass **ein Restart der Applikation** durchgeführt wird. Sie müssen also einen "Toggle-Button" oder ähnliches auf der Oberfläche einbauen. 
    - Use case 1: Sie haben eine Produktion-Umgebung und eine User-Acceptance-Test-Umgebung parallel im Betrieb und Sie dürfen im Test keine produktiven Daten sehen und müssen das dem Gesetzgeber und dem/der Kunden beweisen indem Sie zwei Datenbanken "fahren", die sogar herstellerunabhängig sind.
    - Use case 2: Sie migrieren Ihre Applikation auf eine neue Umgebung, in der nur noch das Datenbank-"Konkurrenzprodukt" verwendet werden darf. Z.B. wenn "Ihre" Applikation an eine andere Unternehmung verkauft wurde oder das Management bestimmt hat, dass nur noch die Datenbanken des "Einen" Herstellers verwendet werden darf. 

 1. ) **Oracle** Express: Datenbankanbindung programmieren
 2. ) **PostgreSQL**: Datenbankanbindung mit transaktionssicheren Abfragen programmieren
 3. ) **MSSQL Express**: Datenbankanbindung programmieren
 4. ) **MySQL/MariaDB**: Innodb Tabellen mit transaktionssicheren Abfragen und in MVC programmiert
 5. ) **Andere** Datenbankanbindung programmieren (NoSQL, MongoDB, ..)

- Bonus 1: Rechnungsstellung per Mail oder PDF für den Kunden zum Ausdrucken und zum Bezahlen via Einzahlungsschein oder persönlichem TWINT (-> QR-Code)

- Bonus 1: Externe Schnittstelle implementiert (Zahlungssystem einbinden, z.B. TWINT)

<br>
<br>
<br>
<br>

### Bewertungsraster
| Thema/Aufgabe                                         | (-0.3)                                                           | Note 4.0                                                     | (+0.3)                                                       |
| ----------------------------------------------------- | ---------------------------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **Aufgabe 1** <br/>Adminbereich samt Login und Userbereich (mit und ohne Login) realisieren | Aufgaben unvollständig. Daten wurden unvollständig oder gar nicht übernommen. Keine Benutzeridentifikation realisiert. Resultate sind für eine Anwendung unbrauchbar | 1. Verschlüsseltes Login mit Sessionüberprüfung <br/>2. Benutzerverwaltung mit verschiedenen Rechten <br/>3. Online-Administration der Websiteinhalte <br/>4. Formularüberprüfung | Datenstruktur und Datentypen wurden angelegt. Umfangreiches Benutzerkonzept erstellt und zumindest teilweise realisiert (z. B. Benutzerverwaltung über DB-Tabellen). |
| **Aufgabe 2** <br/>Datenbankserver mit transaktionssicheren Abfragen. (Oracle-Express, MSSQL-Express, MYSQL, Postgresql) | Applikation ist nicht lauffähig, Daten werden nicht vollständig angezeigt, Navigation ist nicht anwendergerecht implementiert. | Anbindung Applikation und Datenbank ist funktionsfähig. Daten der DB können administriert werden. Transaktionssichere Abfragen wurden realisiert, Navigation ist möglich. | SQL der Datenbank angepasst. Sinnvolle, grafisch ansprechende Darstellung der Daten. Daten können sortiert werden. Navigation ist übersichtlich und funktionsfähig. |
| **Aufgabe 3** <br/>MVC, 2-Tier, 3-Tier, 4-Tier        | MVC nicht oder nur teilweise angewendet                          | MVC realisiert und funktionsfähig -> "läuft"                 | MVC realisiert und vorbildlich implementiert (bis ins Detail)|
| **Aufgabe 4** <br/>Eigene Funktionen - Eigene Klassen | Keine eigenen Funktionen entwickelt                              | Mehrere eigene Funktionen entwickelt und sinnvoll eingesetzt | Umfangreiches Funktionskonzept umgesetzt                     |
| **Aufgabe 5** <br/>Anwendung testen                   | Tests nicht oder nur wenig durchgeführt, kein Testkonzept, keine Testanleitung vorhanden | Tests wurden durchgeführt und dokumentiert. Testanleitung. Gefundene Fehler sind beschrieben. | Testkonzept vorhanden und durchgeführt. Testergebnisse sind dokumentiert und nachvollziehbar |
| **Bonus 1** <br/>Läuft im Internet auf (Sub-)Domain | nur lokal | Internet mit 1 DB | Internet mit 2 DBs |
| **Bonus 2** <br/>DB-Switch | - | statisch | dynamisch | 
| **Bonus 3** <br/>Einbindung externen Schnittstelle | - | - | z.B. Zahlungssystem wie TWINT oder Datatrans eingebunden und funktioniert | 
|  |  |  |  |
