[TOC]


# M322 Einleitung

## Richtiges Verstehen, Interpretation
<br>
![witz-vom-olli_flussziege.jpg](material/witz-vom-olli_flussziege.jpg)
<br>
[https://www.youtube.com/watch?v=dW3FGs8oaKg&t=2s](https://www.youtube.com/watch?v=dW3FGs8oaKg&t=2s)

<br>
<br>
<br>

## UX in der Praxis

### ZKB-App 2022

User sind mit dem neuen UX (und den neuen, bzw. nicht mehr vohandenen Funktionen) nicht zufrieden.
<br>
<br><https://insideparadeplatz.ch/2022/05/05/neue-zkb-app-kunden-wuetend-bank-beschwichtigt/>
<br><https://insideparadeplatz.ch/2022/05/11/wut-auf-neue-zkb-mobile-app-explodiert/>
<br><https://insideparadeplatz.ch/2022/05/25/zkb-apparatschiks/>
<br>

![2022-05-25-Neue-ZKB-App-txt.jpg](material/2022-05-25-Neue-ZKB-App-txt.jpg)
<br>
<br>
<br>

### Mein-Uster.ch 2022

Bevölkerung wird aufgerufen, am neuen Auftritt mitzuwirken.
<br>
<br>[2022-07-21_mein-uster.ch.pdf<br>
![2022-07-21_mein-uster.ch.jpg](material/2022-07-21_mein-uster.ch.jpg)
](material/2022-07-21_mein-uster.ch.pdf)



