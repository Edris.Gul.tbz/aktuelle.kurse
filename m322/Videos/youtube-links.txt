Finger weg von UI/UX Jobs!   ---------------> hat ein gutes Hirn-Hälften-Bild
3 min, D, 2022
https://www.youtube.com/watch?v=zeyxamckbtk


UI vs. UX (Erklärung auf Deutsch)   --------------------> think/pair/share zu Beginn
12 min, D, 2018 https://www.youtube.com/watch?v=qTU5XelOIWo


Become a UX Designer in 2022 - 4 reasons
9 min, E, 2022 https://www.youtube.com/watch?v=NVCUfYW62IA


The UX Design Process explained step by step with a mobile app project
7 min, E, 2022 https://www.youtube.com/watch?v=rYH7AErVd7w


UX Design Trends 2022 (And The Future Of UX…)
20 min, E, 2022 https://www.youtube.com/watch?v=aFJpdHEvR64


UX/UI Design Trends 2022
12 min, E, 2022, Produktwerbung envato, https://www.youtube.com/watch?v=YRIHdCJqQOg


Get Started In UX Design - A Full 7-Step Guide (2022)
30 min, E, 2022, https://www.youtube.com/watch?v=1PHnzrhAnfw


https://www.youtube.com/user/careerfoundry


UX Design Course Tutorial for Beginners: User Experience Design Fundamentals
3 h 50 min, E, 2021
https://www.youtube.com/watch?v=uL2ZB7XXIgg