# M122 Abläufe mit einer Scriptsprache automatisieren

[TOC]


# Tools & Technics

## Scheduler "Cron"

Wenn Sie unter Windows Subsystem for Linux (WSL) arbeiten, ist es hoch wahrscheinlich, dass `cron` nicht eingeschaltet ist. Prüfen Sie:

	sudo service cron status
	sudo service cron start

Schauen, was in cron eingestellt ist (mit `sudo` für alle user des Systems oder ohne für den aktuellen User):

	sudo crontab -l
	sudo crontab -e 

wobei  (l = list) und (e = edit). Die Funktionsweise und Einstellungen siehe [https://de.wikipedia.org/wiki/Cron](https://de.wikipedia.org/wiki/Cron)

## FTP

Auomatisierte Skripts verschieben oft Dateien auf andere Systeme. Dabei wird oft "file transfer" FTP, bzw. sFTP 

- [https://www.howtoforge.com/tutorial/how-to-use-ftp-on-the-linux-shell/](https://www.howtoforge.com/tutorial/how-to-use-ftp-on-the-linux-shell/)

Einen FTP-Zugang zu meinem Server können Sie nutzen, um eigene HTML-Inhalte zu publizieren. Zum testen benutzen Sie den Copy-Befehl auf Ihr Desktop Ihres Windows (wenn Sie Ihr Linux unter dem WSL installiert haben)

	cp index.html /mnt/c/Users/nnnnnnn/Desktop/index.html

![x_ressources/ftp-zugangsdaten](./x_ressources/ftp-zugangsdaten.png)
<br>
<br>
		
**bash-Beispiel**

<br>
![./x_ressources/ftpanweisungen.jpg](./x_ressources/ftpanweisungen.jpg)

<br>
<br>
<br>
<br>

Oder über eine **Zusatzinstallation** von 'lftp'
<br>

	sudo apt install lftp

Danach sollte über folgenden Befehl die FTP-Anweisungen auch auszuführen sein, wobei die Variablen `$user`, `$pass` und `$host` sowie die anderen Variablen `$remote_path` und `$archive_filename` vorher natürlich entsprechend gesetzt werden müssen.

	lftp -e "set ftp:ssl-allow no; put $archive_filename -o $remote_path/$archive_filename; quit" -u "$user","$pass" "$host"

Man beachte, dass mit `ssl-allow no` die Security abgeschaltet wurde. In diesem Beispiel wird `put` gemacht. Bei `get` dann entsprechendes.	

<br>
<br>
<br>
<br>
<br>

**cmd-Beispiel** (Tasten 'WINDOWS' 'R' und dann "cmd" eintippen)
<br>
Legen Sie folgende 2 Dateien (`.bat` und `.ftp`) an und danach machen Sie Doppelklick auf den `.bat`-Programmaufruf
![./cmd-ftp-start.bat.jpg](./x_ressources/cmd-ftp-start.bat.jpg)
![./cmdcommands.ftp.jpg](./x_ressources/cmdcommands.ftp.jpg)

<br>
<br>
<hr>
<br>
<br>


## cURL 

Mittels curl (cURL) können auch ganz einfach Daten, 
<br>die über https zugänglich sind, abholen. 
<br>Benützen Sie z.B.

	curl https://haraldmueller.ch/schueler/m122_projektunterlagen/b/MOCK_DATA.csv > mock_data.csv

Weitere Anleitungen zu cURL

https://everything.curl.dev/ftp

<br>
<br>
<br>
<br>
<br>
<br>

## EMail


### E-Mail in Linux / Bash

Hier eine Anleitung, wie in Bash ein Mail-System eingerichtet 
werden kann und wie man ein Mail machen kann.
Für unsere Anwendungen hier im Modol M122 recht es, 
dass man Mails senden kann. Das Empfangen von Mails 
hier in Bash ist für uns jetzt nicht nötig.

Dazu muss man 'mailutils' installieren, damit man mit 
dem Befehl 'mail' ein Mail absenden kann.
Es geht auch mit 'mutt' und 'sendmails' und mit 'ssmtp':

Hier eine kleine Anleitung für ssmpt via einen 
"fremden" Postausgangsserver:


- **1.)** Lösen Sie einen Account bei einem Mailprovider 
wie z.B. [smart-mail.de](https://www.smart-mail.de/index.php?action=signup).
<br><br>Es muss ein Provider sein, der es zulässt, dass man seinen Server 
"von aussen" her adressieren darf. Über die Provider  bluewin.ch, 
gmail.com und tbz.ch habe ich das (MUH, 2023) nicht (mehr) hinbekommen. 
Aber vielleicht schafft es jemand von Euch.


- **2.)** Dort drin, bei den "Einstellungen" finde ich die Angaben zum Mailserver
<br>![smart-mail_pop3-smtp-daten.png](x_ressources/smart-mail_pop3-smtp-daten.png)
<br><br>


- **3.)** Installieren Sie dann diese beiden Programme in Ubuntu
.<br>`sudo apt install ssmtp`
<br>`sudo apt install mailutils`


- **4.)** Machen Sie die ssmpt-Konfiguration. Rufen Sie dabei mit folgendem Befehl die `ssmpt.conf` auf

      
     
	`sudo nano /etc/ssmtp/ssmtp.conf`


und dort ergänzen Sie dann entsprechendes:

![ssmtp.conf.png](x_ressources/ssmtp.conf.png)


<br>
<br>
<br>
<br>

- **5.)** Zu den Mail-Aufrufen:


Ein Mail-Aufruf direkt über die Command-line geht zum Beispiel so:
<br>Dabei bedeutet das `<<<`, dass der folgende Text als Body der Nachricht 'importiert' wird.
    
    
	mail -s 'subject' -a From:HM\<harald.mueller@smart-mail.de\> harald.mueller@tbz.ch <<< 'bodytext'

Wenn man den Mailtext als separate Datei mitgeben will, macht man das so:

	mail -s 'subject' -a From:HM\<harald.mueller@smart-mail.de\> harald.mueller@tbz.ch < msg.txt

Wenn man den Mailtext als separate Datei mitgeben will, macht man das so:

	mail -s 'subject' -a From:HM\<harald.mueller@smart-mail.de\> harald.mueller@tbz.ch < msg.txt -A att.pdf


Natürlich kann man diese Zeile auch in ein Skript einbauen. 


<br>

Weitere Beschreibungen sind hier:

- https://linuxhint.com/bash_script_send_email
- https://www.linuxfordevices.com/tutorials/linux/mail-command-in-linux
- https://www.geeksforgeeks.org/send-mails-using-a-bash-script

Als **Ubuntu**-Installation über `msmtp-mta` und `heirloom-mailx`.
Beim Passwort muss das GMail-App-Passwort was man in den GMail-Einstellungen erstellt eingeben.
Man muss ein weiteres spezielles Zugangs-Passwort für externe User setzen. Schauen Sie in diesem 
Video wie das geht und machen Sie alles nach.

- https://youtu.be/J1DHQG5qFdU dasselbe findet sich auch als Text (um die commands zu kopieren) 
- https://www.alanbonnici.com/2020/11/howto-send-email-from-google-from.html	



### Mailing mit Python 

(Dank an Hr. Noel L. Hug, AP21a)
<br><br>
import win32com.client as win32
<br><br>
![./mailing-mit-python.jpg](x_ressources/mailing-mit-python.jpg)



### Mailing mit PowerShell

![./mailing-mit-powershell.jpg](./x_ressources/mailing-mit-powershell.jpg)



### Mailing mit PHP (nur auf Webserver!)

Diese Art funktioniert nur auf einem Webserver, auf dem ein Mailserver installiert ist, was bei allen teuren und billigen Internetprovidern normal ist. Wenn es auf dem lokalen Rechner funltionieren soll, muss zuerst ein Mailserver lokal installiert werden.

<br>
![./mailing-mit-phpwebserver.jpg](./x_ressources/mailing-mit-phpwebserver.jpg)

<br>
<br>
<br>
<br>
<br>
<br>


## Textformattierung

Am gebräuchlichsten Formattierungs- und Ausgabe-Programme in Linux- und Unix-Systemen sind `echo` und `printf`. 
Die beiden Programme können einzeln und auch in Kombination miteinander verwendet werden.

<br>
<br>

### echo

Die einfach Textausgabe kennen Sie. Es ist der Befehl `echo` mit dem Sie Texte und Variablen ausgeben können.

[![echo.png](x_ressources/echo.png)](https://openbook.rheinwerk-verlag.de/shell_programmierung/shell_007_001.htm#RxxKap00700104004E721F034174)


Der normale Gebrauch ist:

	echo "Geben Sie die Anzahl ein:"
	
oder 

	res="Das Resultat ist:"
	echo $res
	
Die Ausgabe von Variablen geht eitwas eigenartig. Das verwirrt Leute, die schon eine 
Programmiersprache kennen, denn dort geht sowas in der Regel nicht. Nämlich man 
kann/muss die Variable in den Text(-String) hinein!! nehmen.

	summe=32
	echo "Die Summe ist: $summe"
	
Es ist sogar möglich, über die Echo-Funktion zu rechnen. Wenn man nicht nur mit 
Ganzzahlen (Integer) gerechnet werden soll, wird die Mithilfe des 
Programmes `bc` (Basic Calculator) angefordert.

	anz=4.5
	preis=2.13
	summe=$(echo "$anz * $preis" | bc)
	echo "Die Summe ist: $summe"

	

<br>
<br>
<br>
<br>


### printf

Die weit mächtigere Ausgabe von Zahlen und Texten ist der Befehl `printf`. Der kombiniert die Text- und die Zahlenausgabe. 


Der Gebrauch geht über Formatanweisungen `format` und über die zu importierenden `argument`en.

[![printf-argumente.png](x_ressources/printf-argumente.png)](https://openbook.rheinwerk-verlag.de/shell_programmierung/shell_007_001.htm#t2t34)


[![printf.png](x_ressources/printf.png)](https://openbook.rheinwerk-verlag.de/shell_programmierung/shell_007_001.htm#t2t34)


Wobei die "%s" (string), die "%f" (float), und die "%i" oder "%d" (integer=Ganzzahl) am Häufigsten vorkommen. 

[![printf-formate.png](x_ressources/printf-formate.png)](https://openbook.rheinwerk-verlag.de/shell_programmierung/shell_007_001.htm#t2t34)


<br>
<br>
<br>

<br>
<br>
<br>



#### Beispiel 'printf' und 'echo'

Eine Solche Ausgabe können Sie am Besten über die `printf` machen:

![rechnungspos1.png](x_ressources/rechnungspos1.png)

Beachten Sie die Ausrichtung des Texts nach der linken und der rechten Kolonne. Es geht über "%-25s" und "%8s".

![rechnungspos2.png](x_ressources/rechnungspos2.png)

*Natürlich muss am Schluss die* **Liste** *der Artikel-Positionen mit einer `for`-Schleife abgearbeitet werden.*

<br>
<br>
<br>

In diesem Code-Stück kommt auch noch die schweizerische **5-Rappen-Rundung** vor. 
Da wird ein Aufruf einer Unterfunktion, die in einer separaten
Skript-Datei untergebracht ist, gemacht `./round05.sh $summe`. Der Einfachkeit halber 
wurde die Datei und die Funktion gleich benannt, was aber nicht zwingend ist. 

(Nebenbei bemerkt: Anders als in modernen Programmiersprachen, 
kann eine Funktion in Bash **keine** Rückgabewerte liefern. Die 
Werte-Rückgabe erfolgt über `echo`, das dann vom aufrufenden Programm 
entsprechend abgefangen werden muss.)

![rechnungspos3.png](x_ressources/rechnungspos3.png)

Funktionsweise:

Diese Funktion geht über die Mithilfe des Programmes 
`bc` (Basic Calculator) mit der 
Multiplikation mit 20 gemacht wird. 

Dann erfolgt eine Addition von 0.5 und dann sofort die 
"normale" Rundung mit einer Überführung in einen "Integer" 
(=Ganzzahlwert), was die Kommastellen abschneidet.

Am Schluss wird dann wieder durch 20 geteilt und auf 
2 Stellen einen "Float" (reelle Zahl, mit Kommastellen) zugelassen.  


Der unterste Befehl `round05 $1` ist eigentlich er Erste. der ausgeführt wird, 
wenn die Datei aufgerufen wird. Die die Funktions-Definition ist eigentlich 
von "aussen" her gekapselt und muss zuerst aufgerufen werden. Mit `$1` wird 
das erste Element nach dem Datei-Aufruf als Parameter aufgegriffen und so verarbeitet.