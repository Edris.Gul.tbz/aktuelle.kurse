# M122 - Aufgabe

2023-07 MUH

## Automatisierte Installation

Erstellen Sie eine automatische Installation mit verschiedenen 
Parametern, die Sie vom Benutzer in irgend einer Form abfragen 
(Konfig-Datei oder Dialog, wobei die Datei Vorteile bringt)

Zum guten Skript gehört dazu, dass ein Log geschrieben wird
und dass das Skript am Schluss selber gewisse Tests macht
und das dem Anwender am Schluss in einem Display und! im
Log zeigt.

Mögliche Installationen:

- eine ganze suite von Tools
- eine VM
- Drucker, Scanner, Treiber usw.
- *lassen Sie sich was einfallen* ...


<hr>

## Bewertung

| Punkte | Beschreibung | 
|--------|--------------|
|     1  | Eine Ablaufskizze (activity diagram) wird der Lehrperson vorgelegt |
|     1  | Ein Log existiert |
|   2-4  | Für die Komplexität und den Algo |
|   1-2  | Tests gemacht und sind protokolliert  |
| **5-8**  | **Total** | 
|        | Bonuspunkte für aussergewöhnliches |
|        |    |
| **1/2**| Halbierung der Punkte, wenn der gleiche Code schon mal (bei einem Kollegen) gesehen wurde |

<hr>
