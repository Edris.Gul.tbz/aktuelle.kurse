# M122 - Aufgabe

2024-06 MUH

## F APIs-Abfragen mit Datendarstellung

| Punkte | Beschreibung | 
|--------|--------------|
|     1  | Eine Ablaufskizze (activity diagram) wird der Lehrperson vorgelegt |
|     1  | Für die Anbindung der ersten API mit einer Informationsklasse  (Wetter, Kurse, ...)  |
|     1  | Für die Verarbeitung von JSON-Files usw. (einfache Auflistung aller Daten)  |
|     1  | Einbindung in crontab für die regelmässige Verarbeitung --> Log-File |
|     1  | "Schöne" (tabellarische) Darstellung der Informationsklasse(n)  |
| **5**  | **Total** | 
|     1  | Eingechecked in GitLab, GitHub, BitBucket  |
|     1  | Bonuspunkt für jede Anbindung einer weiteren API, bzw. Informationsklasse | 
|     2  | Bonuspunkte für eine applikatorische Bearbeitung (nur ausgewählte Daten mit Steuerung über eine andere Config-Datei) |
|     1  | Bonuspunkt für die Onlinebereitstellung (FTP, HTML) mindestens einer Informationsklasse, halber Punkt für html-File auf mnt/c/Users/NAME/Desktop  |
|     1  | Bonuspunkt für die regelmässige Zusendung per Mail (an sich selber)  |
|        |   |
| **Plagiat**  | Reduktion der Punkte nach Einschätzung des Lehrers, wenn der gleiche Code schon mal gesehen wurde  |
|        |   |


### Aufgabenstellung

Für eine automatische Abholung von frei verfügbaren
Informationen kann man APIs (application programming interfaces) 
benützen. Erstellen Sie "regelmässig" (muss in cron eingebunden sein)
eine Serie von für Sie wertvolle und tagesaktuelle Informationen.

Verwenden Sie dafür Bash-Shell-Script (ausnahmsweise auch PowerShell oder Python)

Die Informationen sollen (lokal oder auf einer Domain) als HTML-source "schön" 
und so dargestellt werden, dass sie einfach mit dem Browser angesehen werden können.

Besser ist es, wenn die Informationen im Internet abrufbar sind 
[**FTP-Zugangsdaten**](../../tools-technics/ftp-zugangsdaten.md)

Lassen Sie sich die Informationen auch mailen.

Binden Sie Ihr Skript in die `crontab` ein (sudo crontab -e)
und wählen Sie einen geeigneten und vernünftigen Ausführungs-Takt.

Mögliche Informationen könnten sein:
- Hauptwährung-Umrechnungskurse (EUR zu CHF und 2 weitere Währungen)
- Kurse von Crypto-Währungen (wählen Sie 3 eigene aus)
- Aktienkurse oder Index-Entwicklungen
- Wetterdaten (von mehr als 1 Ort und über mehrere Zeitpunkte)
- Wassertemperaturen
- Sportresultate
- Kultur- und Event-Termine
- Aktuelle Preise von Gebrauchs- oder Konsumgüter

Sie können auch APIs von Einmaldaten abrufen wie
- Distanzen von Orten
- Bahnverbindungen
- Flugverbindungen
- Himmels-/Astronomiedaten


Hier einige mögliche APIs zum anbinden:

https://mixedanalytics.com/blog/list-actually-free-open-no-auth-needed-apis/

https://polygon.io/pricing

https://dev.to/sajankc/top-free-apis-without-an-api-key-120n

https://apipheny.io/free-api/


<hr>

https://api.coindesk.com/v1/bpi/currentprice.json

https://coinmap.org/api/v1/coins/

https://api.gemini.com/v2/ticker/btcusd, /ethusd, ... (nimm "ask"-Preis)

https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=1&sparkline=false



https://github.com/15Dkatz/official_joke_api

https://v6.exchangerate-api.com/v6/bb9ddd1855e84ded5fca04e0/latest/USD

```
curl https://v6.exchangerate-api.com/v6/bb9ddd1855e84ded5fca04e0/latest/CHF | grep USD 
curl https://v6.exchangerate-api.com/v6/bb9ddd1855e84ded5fca04e0/latest/CHF | grep EUR
``` 
