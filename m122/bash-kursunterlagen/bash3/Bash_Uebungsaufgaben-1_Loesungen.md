# Bash\Übung 1 - Beispiellösungen

**Aufgabe 1 - Repetition: Navigieren in Verzeichnissen**
```
1.  [root@localhost: ] cd ~
2.  [root@localhost: ~ ] cd /var/log
3.  [root@localhost: log ] cd /etc/udev
4.  [root@localhost: udev ] cd ..
5.  [root@localhost: udev ] cd network
6.  [root@localhost: network ] cd ../../dev
```

**Aufgabe 2 - stdout, stdin, stderr:**

**a)**
`ls -z 2> /root/errorsLs.log`

**b)**
```
echo "sdfonsdodsf" > datei.txt
cat datei.txt > datei2.txt
cat datei.txt > datei2.txt
cat datei2.txt
cat datei.txt >> datei2.txt
cat datei.txt >> datei2.txt
cat datei2.txt
      #Unterschied: 
	  >> hängt Inhalt an, 
	  > überschreibt Inhalt
cat datei.txt >> datei.txt
      #Erzeugt Fehler, Quell- ist Zieldatei
```

**c)**
`whoami > info.txt`

**d)**
`id >> info.txt`

**e)**
`cat info-txt | wc -w 
`

<br>
**Aufgabe 3 - grep, cut:**

**a)**
```
cat file.txt | grep obelix
cat file.txt | grep 2
cat file.txt | grep e
cat file.txt | grep -v gamma
cat file.txt | grep -E "1|2|3"
```
**b)** 
```		
cat file.txt | cut -d ':' -f 1
cat file.txt | cut -d ':' -f 2
cat file.txt | cut -d ':' -f 3
```

<br>
**Aufgabe 4 - Wildcards:**

1.  `mkdir ~/Docs`
2.  `touch ~/Docs/file{1..10}`
3.  `rm -f ~/Docs/file1*`
4.  `rm -f ~/Docs/file[247]`
5.  `rm -f ~/Docs/* oder rm -f ~/Docs/file*`
6.  `mkdir Files`, `cd Files`
7.  `touch file{1..10}` (Erzeugt fileX von 1 bis 10)
8.  `cd ..` , `cp -R Files Files2`
9.  `cp -R Files Files2/Files3`
10.  `mv Files Files1`
11.  `rm -f *`

<br>
**Aufgabe 5 - Tilde expansions:**
> -   Siehe Präsentationsfolien

<br>
**Aufgabe 6 - Für Fortgeschrittene:**

-   Findet alle Zeilen, welche eine PCI-Adresse beinhalten
-   Findet IP-Adressen
-   Findet alle Dateien, welche dem Benutzer root gehören ( -user root ), 
        im Dateinamen das Wort bash vorkommt ( -iname `"*bash*"`, Gross- und 
        Kleinschreibung wird ignoriert) und kopiert diese nach 
         `/root ( -exec cp {} /root/` ).
