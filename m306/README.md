# M306 - Kleinprojekte im eigenen Berufsumfeld abwickeln

**Modulidentifikation**
- Bivo14 (Lehrbeginn bis 2020) [.pdf](https://cf.ict-berufsbildung.ch/modules.php?name=Mbk&a=20106&nmodulid=496&cdateiformat=pdf)
- Bivo21 (Lehrbeginn ab 2021) [.html](https://www.modulbaukasten.ch/module/a875c9da-716c-eb11-b0b1-000d3a830b2b/de-DE?title=Kleinprojekte-im-eigenen-Berufsumfeld-abwickeln)
[.pdf](https://cf.ict-berufsbildung.ch/modules.php?name=Mbk&a=20106&nmodulid=660&cdateiformat=pdf)

<br>
![projektmanagement.jpg](projektmanagement.jpg)
<br>
<br>
<br>


[TOC]

<br>
<br>

## Tages- und Themenplan



### PE21a (Mo morgens)

| ~Tag | Datum     | Thema                                                                          | Bemerkungen               |
| ---- | -----     | ----                                                                           | ----                      |
|   1  | Mo 13.11. | [Projekt-Ziele                        ](./1_Projekt-Ziele)                     |                           |
|   2  | Mo 20.11. | [Projekt-Planung                      ](./2_Projekt-Planung)                   | Ecolm-Test P-Zie 7% MNote |
|   3  | Mo 27.11. | [Projekt-Organisation                 ](./3_Projekt-Organisation)              | Ecolm-Test P-Pla 7% MNote |
|   4  | Mo 04.12. | [Projekt-Führung                      ](./4_Projekt-Fuehrung)                  | Ecolm-Test P-Org 7% MNote |
|   5  | Mo 11.12. | [Projekt-Kontrolle und Vorgangsmodelle](./5_Projekt-Kontrolle-Vorgangsmodelle) | Ecolm-Test P-Füh 7% MNote |
|   6  | Mo 18.12. | [Projekt-Risiken und Risikoanalyse    ](./6_Projekt-Risiken-Risikoanalyse)     | Ecolm-Test P-Kon 7% MNote |
|   -  | -Ferien-  |                                                                                |                           |
|   7  | Mo 08.01. | [Qualitätsicherung                    ](./7_Qualitaetsicherung)                | Gruppen-Projekt 32% MNote |
|   8  | Mo 15.01. | [Abschlussprojekt                     ](./Abschlussprojekt)                    |                           |
|   9  | Mo 22.01. | [Abschlussprojekt                     ](./Abschlussprojekt)                    | Gruppen-Projekt 33% MNote |
|  10  | Mo 29.01. | [Warum Projekte scheitern](./add-on2_Warum-Projekte-scheitern)                 |                           |
|      |           |                                                                                |                           |
|  add on |  1     | [Lastenheft-Pflichtenheft](./add-on1_Lastenheft-Pflichtenheft)                 |                           |



### AP21a (Di morgens)

| ~Tag | Datum     | Thema                                                                          | Bemerkungen               |
| ---- | -----     | ----                                                                           | ----                      |
|   1  | Di 14.11. | [Projekt-Ziele                        ](./1_Projekt-Ziele)                     |                           |
|   2  | Di 21.11. | [Projekt-Planung                      ](./2_Projekt-Planung)                   | Ecolm-Test P-Zie 7% MNote |
|   3  | Di 28.11. | [Projekt-Organisation                 ](./3_Projekt-Organisation)              | Ecolm-Test P-Pla 7% MNote |
|   4  | Di 05.12. | [Projekt-Führung                      ](./4_Projekt-Fuehrung)                  | Ecolm-Test P-Org 7% MNote |
|   5  | Di 12.12. | [Projekt-Kontrolle und Vorgangsmodelle](./5_Projekt-Kontrolle-Vorgangsmodelle) | Ecolm-Test P-Füh 7% MNote |
|   6  | Di 19.12. | [Projekt-Risiken und Risikoanalyse    ](./6_Projekt-Risiken-Risikoanalyse)     | Ecolm-Test P-Kon 7% MNote |
|   -  | -Ferien-  |                                                                                |                           |
|   7  | Di 09.01. | [Qualitätsicherung                    ](./7_Qualitaetsicherung)                | Gruppen-Projekt 32% MNote |
|   8  | Di 16.01. | [Abschlussprojekt                     ](./Abschlussprojekt)                    |                           |
|   9  | Di 23.01. | [Abschlussprojekt                     ](./Abschlussprojekt)                    | Gruppen-Projekt 33% MNote |
|  10  | Di 30.01. | [Warum Projekte scheitern](./add-on2_Warum-Projekte-scheitern)                 |                           |
|      |           |                                                                                |                           |
|  add on |  1     | [Lastenheft-Pflichtenheft](./add-on1_Lastenheft-Pflichtenheft)                 |                           |





### PE21d (Fr morgens)

| ~Tag | Datum     | Thema                                                                          | Bemerkungen               |
| ---- | -----     | ----                                                                           | ----                      |
|   1  | Fr 17.11. | [Projekt-Ziele                        ](./1_Projekt-Ziele)                     |                           |
|   2  | Fr 24.11. | [Projekt-Planung                      ](./2_Projekt-Planung)                   | Ecolm-Test P-Zie 7% MNote |
|   -  |-fällt aus-| - Klausurtagung -                                                              |                           |
|   3  | Fr 08.12. | [Projekt-Organisation                 ](./3_Projekt-Organisation)              | Ecolm-Test P-Pla 7% MNote |
|   4  | Fr 15.12. | [Projekt-Führung                      ](./4_Projekt-Fuehrung)                  | Ecolm-Test P-Org 7% MNote |
|   5  | Fr 22.12. | [Projekt-Kontrolle und Vorgangsmodelle](./5_Projekt-Kontrolle-Vorgangsmodelle) | Ecolm-Test P-Füh 7% MNote |
|   -  | -Ferien-  |                                                                                |                           |
|   6  | Fr 12.01. | [Projekt-Risiken und Risikoanalyse    ](./6_Projekt-Risiken-Risikoanalyse)     | Ecolm-Test P-Kon 7% MNote |
|   7  | Fr 19.01. | [Qualitätsicherung                    ](./7_Qualitaetsicherung)                | Gruppen-Projekt 32% MNote |
|   8  | Fr 26.01. | [Abschlussprojekt                     ](./Abschlussprojekt)                    |                           |
|   9  | Fr 02.02. | [Abschlussprojekt                     ](./Abschlussprojekt)                    | Gruppen-Projekt 33% MNote |
|      |           |                                                                                |                           |
|  add on |  1     | [Lastenheft-Pflichtenheft](./add-on1_Lastenheft-Pflichtenheft)                 |                           |






<br>
<br>
<br>[TIME](https://www.youtube.com/watch?v=JwYX52BP2Sk)
<br>[MONEY](https://www.youtube.com/watch?v=cpbbuaIA3Ds)



![IT-Projectmanagement.jpg](./it-projectmanagement.jpg)

![Projekt-Zeitbedarf.jpg](./M306_Cartoon_Projekt-Zeitbedarf.jpg)

