# M306/5 Projekt-Kontrolle und Vorgangsmodelle

- [Projektkontrolle_Praeinstruktion](./M306_5_Projektkontrolle_Praeinstruktion.txt)


## Theorie
- [Skript (Seiten 34-40)](../docs) 
- Theorie/Skript [Projektkontrolle (alternativ)](./M306_5_Projektkontrolle.pdf)
- Übersicht [Vorgehensmodelle](./01_Vorgehensmodelle-Sarre2015.pdf) von F.Sarre
- [HERMES_2013.pdf](./02_HERMES_2013.pdf) als Vorgehensmodell 
<br>der Bundesverwaltung [hermes.admin.ch](https://www.hermes.admin.ch),
<br>des Kanton Zürich [hermes.zh.ch](http://hermes.zh.ch), mit [Methoden-Übersicht](https://hermes.zh.ch/de/pjm-2022/verstehen/methodenueberblick.html) , 
<br>[Dokumenten-Vorlagen](https://hermes.zh.ch/de/pjm-2022/anwenden/vorlagen.html) für z.B. [Hermes Kleinprojekte Kt. ZH](http://ec2-34-242-70-192.eu-west-1.compute.amazonaws.com/anwenderloesung/szenarien-overview.xhtml)
<br>und auch [Aufgaben und Anleitungen](https://hermes.zh.ch/de/pjm-2022/verstehen/aufgaben.html) um HERMES anzuwenden.
<br>Dabei ist HERMES nicht stehengeblieben. So gibt es auch
<br>ein [agiles Vorgehen mit Hermes](https://www.hermes.admin.ch/de/projektmanagement/verstehen/hinweise-zur-anwendung/agiles-projektmanagement.html)

- [What is SCRUM (scrum.org)](https://www.scrum.org/resources/what-is-scrum)
- (7:51 min, E) [Introduction to SCRUM in 7 Minutes](https://www.youtube.com/watch?v=9TycLR0TqFA)

## Aufgabe
- **Einzelarbeit** oder in **Teams zu 2 Personen**: [Ausarbeitung/Vergleich von Vorgehensmodellen (in Selbstinstruktion)](./M306_5_Vorgehensmodelle_AusarbeitungSelbstinstruktion.pdf)

