# M306/Abschlussprojekt

_(33 % Modulnote)_


### Aufgabenstellung als Prüfungssituation für 4 oder 7 Lektionen 

**Themen/Lernziele:** 
 - Projektmanagement, 
 - Zusammenarbeiten im (Projekt-)Team, 
 - Rückblick auf alle Themen des Moduls

**Teamgrösse**
- 4 Personen (+/- 1)

**Aufgabenstellung**
- Variante A: <br>Abschlussprojekt als Erarbeitung eines Übersichtsdokuments bzw. ein **erklärendes Projekthandbuch** <br>[M306_9_Abschlussprojekt_A.pdf](./M306_9_Abschlussprojekt_A.pdf) / [(DOCX)](./M306_9_Abschlussprojekt_A.docx)<br>
- Variante B: <br>[Organisation eines Triathlons](./M306_9_AbschlussprojektB_OrganisationEinesTriathlons.pdf) oder etwas in dieser Art (selbstbestimmt)<br><br>

- Bewertung: <br>[M306_9_Abschlussprojekt_klasse_Gx.xlsx](./M306_9_Abschlussprojekt_klasse_Gx.xlsx)
